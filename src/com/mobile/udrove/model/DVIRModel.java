package com.mobile.udrove.model;

import java.util.HashMap;

public class DVIRModel {

	private  long ID = 1;

	private  String odometer = null;//ODOMETER
	private  String powerUnit = null;//POWERUNIT
	private  String powerUnitName = null;
	private  String trailerId = null;//TRAILERID
	private  String dvirDate = null; //date
	private  String dvirId = null; //Id
	private  String mechanic_name = null;
	private  String driver_name = null;
	private  String mechanic_inspection_date = null;
	private  String approved = null;
	private  String defects_corrected = null; 
	private  String dvir_receipt_image_name = null; 
	private  String remarks_main = null; 

	private String imageUri;
	private String remarks;
	
	private HashMap<Integer,DVIRItemModel> hashmapdviritem = new HashMap<Integer, DVIRItemModel>();

	public DVIRModel() {

	}

	
	public  void setID(long iD) {
		ID = iD;
	}

	public  long getID() {
		return ID;
	}

	/**
	 * @param key0
	 *            the key0 to set
	 */
	
	/**
	 * @return the imageUri
	 */
	
	
	
	public String getImageUri() {
		return imageUri;
	}

	/**
	 * @param imageUri the imageUri to set
	 */
	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	
	public String getOdometer() {
		return odometer;
	}


	public void setOdometer(String odometer) {
		this.odometer = odometer;
	}


	public String getPowerUnit() {
		return powerUnit;
	}


	public void setPowerUnit(String powerUnit) {
		this.powerUnit = powerUnit;
	}


	public String getTrailerId() {
		return trailerId;
	}


	public void setTrailerId(String trailerId) {
		this.trailerId = trailerId;
	}


	public String getDvirDate() {
		return dvirDate;
	}


	public void setDvirDate(String dvirDate) {
		this.dvirDate = dvirDate;
	}


	public String getDvirId() {
		return dvirId;
	}


	public void setDvirId(String dvirId) {
		this.dvirId = dvirId;
	}
	
	public String getMechanic_name() {
		return mechanic_name;
	}

	public void setMechanic_name(String mechanic_name) {
		this.mechanic_name = mechanic_name;
	}

	public String getDriver_name() {
		return driver_name;
	}

	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}

	public String getMechanic_inspection_date() {
		return mechanic_inspection_date;
	}

	public void setMechanic_inspection_date(String mechanic_inspection_date) {
		this.mechanic_inspection_date = mechanic_inspection_date;
	}

	public String getApproved() {
		return approved;
	}

	public void setApproved(String approved) {
		this.approved = approved;
	}

	public String getDefects_corrected() {
		return defects_corrected;
	}

	public void setDefects_corrected(String defects_corrected) {
		this.defects_corrected = defects_corrected;
	}

	public String getDvir_receipt_image_name() {
		return dvir_receipt_image_name;
	}

	public void setDvir_receipt_image_name(String dvir_receipt_image_name) {
		this.dvir_receipt_image_name = dvir_receipt_image_name;
	}

	
	public String getRemarks_main() {
		return remarks_main;
	}

	public void setRemarks_main(String remarks_main) {
		this.remarks_main = remarks_main;
	}


	/**
	 * @return the hashmapdviritem
	 */
	public HashMap<Integer,DVIRItemModel> getHashmapdviritem() {
		return hashmapdviritem;
	}


	/**
	 * @param hashmapdviritem the hashmapdviritem to set
	 */
	public void setHashmapdviritem(HashMap<Integer,DVIRItemModel> hashmapdviritem) {
		this.hashmapdviritem = hashmapdviritem;
	}


	/**
	 * @return the powerUnitName
	 */
	public String getPowerUnitName() {
		return powerUnitName;
	}


	/**
	 * @param powerUnitName the powerUnitName to set
	 */
	public void setPowerUnitName(String powerUnitName) {
		this.powerUnitName = powerUnitName;
	}


}
