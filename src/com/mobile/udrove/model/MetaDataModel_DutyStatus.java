package com.mobile.udrove.model;

public class MetaDataModel_DutyStatus {
		
	    public MetaDataModel_DutyStatus(int[] ids,String[] names, String[] codes){
			this.nameList = names;
			this.idList = ids;
			this.codeList = codes;
		}
		
		private int[] idList;
		private String[] nameList;
		private String[] codeList;
		
		public String[] getNameList() {
			return nameList;
		}
		
		public void setNameList(String[] nameList) {
			this.nameList = nameList;
		}

		public String[] getCodeList() {
			return codeList;
		}
		
		public void setCodeList(String[] codeList) {
			this.codeList = codeList;
		}

		public int[] getIdList() {
			return idList;
		}

		public void setIdList(int[] idList) {
			this.idList = idList;
		}

	}
