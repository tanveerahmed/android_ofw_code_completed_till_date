package com.mobile.udrove.model;

import com.mobile.udrove.db.uDroveDbAdapter;

import android.content.Context;

public class LoadManagementTable {

	private static long ID = 1;

	private static int loadstatus = 0; // load status could be current,create, 0
										// = create and 1 is current

	public static int intorignstate = 0;
	public static int intdeststate = 0;

	public static String startdate = null;
	public static String originstate = null;
	public static String origincity = null;
	public static String loadodometer = null;
	public static String shipper = null;
	public static String commodity = null;
	public static String proship = null;
	public static String deststate = null;
	public static String destcity = null;
	public static String enddate = null;
	public static String loadstatustype = null;
	public static String loadid = null;
	// 1 means current
	// 2 means completed

	private Context context;
	private static LoadManagementTable loadmanagementable = null;

	public LoadManagementTable() {

	}

	public synchronized static LoadManagementTable getDriverInstance() {

		return loadmanagementable;
	}

	private synchronized void setDefaults() {

		LoadManagementTable.startdate = "";
		LoadManagementTable.originstate = "";
		LoadManagementTable.origincity = "";
		LoadManagementTable.loadodometer = "";
		LoadManagementTable.shipper = "";
		LoadManagementTable.commodity = "";
		LoadManagementTable.proship = "";
		LoadManagementTable.deststate = "";
		LoadManagementTable.destcity = "";
		LoadManagementTable.enddate = "";
		LoadManagementTable.loadstatustype = "";
		LoadManagementTable.loadid = "";
	}

	public synchronized String getLoadId() {

		return loadid;
	}

	public synchronized void setLoadId(String _loadid) {
		LoadManagementTable.loadid = _loadid;
	}

	public synchronized String getLoadStatusType() {

		return loadstatustype;
	}

	public synchronized void setLoadStatusType(String _loadstatustype) {
		LoadManagementTable.loadstatustype = _loadstatustype;
	}

	public synchronized String getStartDate() {
		return startdate;
	}

	public synchronized void setStartDate(String _startdate) {
		LoadManagementTable.startdate = _startdate;
	}

	public synchronized String getOriginState() {
		return originstate;
	}

	public synchronized void setOriginState(String _originstate) {
		LoadManagementTable.originstate = _originstate;
	}

	public synchronized String getOriginCity() {
		return origincity;
	}

	public synchronized void setOriginCity(String _origincity) {
		LoadManagementTable.origincity = _origincity;
	}

	public synchronized String getLoadOdometer() {
		return loadodometer;
	}

	public synchronized void setLoadOdometer(String _loadodometer) {
		LoadManagementTable.loadodometer = _loadodometer;
	}

	public synchronized String getShipper() {
		return shipper;
	}

	public synchronized void setShipper(String _shipper) {
		LoadManagementTable.shipper = _shipper;
	}

	public synchronized String getCommodity() {
		return commodity;
	}

	public synchronized void setCommodity(String _commodity) {
		LoadManagementTable.commodity = _commodity;
	}

	public synchronized String getProship() {
		return proship;
	}

	public synchronized void setProship(String _proship) {
		LoadManagementTable.proship = _proship;
	}

	public synchronized String getDestState() {
		return deststate;
	}

	public synchronized void setDestState(String _deststate) {
		LoadManagementTable.deststate = _deststate;
	}

	public synchronized String getDestCity() {
		return destcity;
	}

	public synchronized void setDestCity(String _destcity) {
		LoadManagementTable.destcity = _destcity;
	}

	public synchronized String getEndDate() {
		return enddate;
	}

	public synchronized void setEndDate(String _enddate) {
		LoadManagementTable.enddate = _enddate;
	}

	// Make sure You have set all the parameters using setters before u call
	// this
	public synchronized void save(boolean initial) {
		uDroveDbAdapter db = new uDroveDbAdapter(context);
		db.deleteAll();
		db.open();
		if (initial == true) {

			setID(db.insertLoadManagement(this));

		}
		db.close();

	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public static void setID(long iD) {
		ID = iD;
	}

	/**
	 * @return the iD
	 */
	public static long getID() {
		return ID;
	}

	/**
	 * @param loadstatus
	 *            the loadstatus to set
	 */
	public static void setLoadstatus(int loadstatus) {
		LoadManagementTable.loadstatus = loadstatus;
	}

	/**
	 * @return the loadstatus
	 */
	public static int getLoadstatus() {
		return loadstatus;
	}

}
