package com.mobile.udrove.model;

public class PODModel {

	private String imageuri;
	private String podtype;
	private int podTypeId;
	private String podString;
	
	/**
	 * @return the imageuri
	 */
	public String getImageuri() {
		return imageuri;
	}
	/**
	 * @param imageuri the imageuri to set
	 */
	public void setImageuri(String imageuri) {
		this.imageuri = imageuri;
	}
	/**
	 * @return the podTypeId
	 */
	public int getPodTypeId() {
		return podTypeId;
	}
	/**
	 * @param podTypeId the podTypeId to set
	 */
	public void setPodTypeId(int podTypeId) {
		this.podTypeId = podTypeId;
	}
	/**
	 * @param podtype the podtype to set
	 */
	public void setPodtype(String podtype) {
		this.podtype = podtype;
	}
	/**
	 * @return the podtype
	 */
	public String getPodtype() {
		return podtype;
	}
	/**
	 * @return the podString
	 */
	public String getPodString() {
		return podString;
	}
	/**
	 * @param podString the podString to set
	 */
	public void setPodString(String podString) {
		this.podString = podString;
	}
}
