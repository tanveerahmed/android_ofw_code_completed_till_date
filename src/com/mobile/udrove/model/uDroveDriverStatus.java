package com.mobile.udrove.model;

import com.mobile.udrove.db.uDroveDbAdapter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class uDroveDriverStatus {

	private static long ID = 1;
	public static int status = 0;
	public static String city = null;
	public static String state = null;
	public static String odotmeter = null;
	public static String remarks = null;
	public static String startdatetime = null;
	public static String enddatetime = null;

	private Context context;
	private static uDroveDriverStatus udrovedriverstatus = null;

	private uDroveDriverStatus(Context context) {

		this.context = context;
		System.out.println("In constuctor ");
		boolean result = getDriverStatusFromDb();
		if (result == false) {
			setDefaults();
		}

	}
	
	

	public synchronized static uDroveDriverStatus getDriverInstance(Context ctx) {
		return getDriverInstance(ctx, false);
	}

	public synchronized static uDroveDriverStatus getDriverInstance(Context ctx,
			boolean getFromdb) {

		if (udrovedriverstatus == null) {
			Log.v("udrovedriverstatus", "Creating udrovedriverstatus");
			udrovedriverstatus = new uDroveDriverStatus(ctx);
		} else {
			if (getFromdb == true) {
				udrovedriverstatus.getDriverStatusFromDb();
			}

		}
		return udrovedriverstatus;
	}

	public synchronized static uDroveDriverStatus getDriverInstance() {

		return udrovedriverstatus;
	}

	private synchronized void setDefaults() {
		uDroveDriverStatus.status = 0; //off duty
		uDroveDriverStatus.city = "";
		uDroveDriverStatus.state = "";
		uDroveDriverStatus.odotmeter = "";
		uDroveDriverStatus.remarks = "";
	}

	public synchronized int getStatus() {
		return status;
	}

	public synchronized void setStatus(int status) {
		uDroveDriverStatus.status = status;
	}

	public synchronized String getCity() {
		return city;
	}

	public synchronized void setCity(String city) {
		uDroveDriverStatus.city = city;
	}

	public synchronized String getState() {
		return state;
	}

	public synchronized void setState(String state) {
		uDroveDriverStatus.state = state;
	}

	public synchronized String getOdometer() {
		return odotmeter;
	}

	public synchronized void setOdometer(String odometer) {
		uDroveDriverStatus.odotmeter = odometer;
	}

	public synchronized String getRemarks() {
		return remarks;
	}

	public synchronized void setRemarks(String remarks) {
		uDroveDriverStatus.remarks = remarks;
	}

	public synchronized String getstartDateTime() {
		return startdatetime;
	}

	public synchronized void setstartDateTime(String startdatetime) {
		uDroveDriverStatus.startdatetime = startdatetime;
	}

	public synchronized String getendDateTime() {
		return enddatetime;
	}

	public synchronized void setendDateTime(String enddatetime) {
		uDroveDriverStatus.enddatetime = enddatetime;
	}

	private synchronized boolean getDriverStatusFromDb() {
		boolean val = false;
		uDroveDbAdapter db = new uDroveDbAdapter(context);
		db.open();
		System.out.println("Before GetAllDroveDuty Status Method.....");
		Cursor c = db.getAlluDroveDutyStatus();
		
		System.out.println("After " + c.getCount());
		if (c.getCount()==0) {
			System.out.println("No Records in the DB getDriverStatusfrom DB ");
			setStatus(3);
			db.close();
			return false;
		}
		val = c.moveToFirst();
		if (val == false) {
			c.close();
			db.close();
			return false;
		}
		System.out.println("Before Getting Columns");
		uDroveDriverStatus.status = c.getInt(c.getColumnIndex(uDroveDbAdapter.KEY_STATUS));
		uDroveDriverStatus.city = c.getString(c.getColumnIndex(uDroveDbAdapter.KEY_CITY));

		uDroveDriverStatus.state = c.getString(c.getColumnIndex(uDroveDbAdapter.KEY_STATE));

		System.out.println("Before Getting Odometer Columbs");
		uDroveDriverStatus.odotmeter = c.getString(c
				.getColumnIndex(uDroveDbAdapter.KEY_ODOMETER));
		System.out.println("Before Getting After Odometer Columns");
		uDroveDriverStatus.remarks = c.getString(c
				.getColumnIndex(uDroveDbAdapter.KEY_REMARKS));

		uDroveDriverStatus.startdatetime = c.getString(c
				.getColumnIndex(uDroveDbAdapter.KEY_STARTDATETIME));

		uDroveDriverStatus.enddatetime = c.getString(c
				.getColumnIndex(uDroveDbAdapter.KEY_ENDDATETIME));
		c.close();

		db.close();
		return true;
	}

	// Make sure You have set all the parameters using setters before u call
	// this
	public synchronized void save(boolean initial) {
		uDroveDbAdapter db = new uDroveDbAdapter(context);
		db.deleteAll();
		db.open();
		if (initial == true) {
			 
			ID = db.insertDriverStatus(this);
		} else {
			boolean updated = db.updateuDroveDutyStatus(ID, this);
			if (updated == false) {
				Log.i("DutyStatus", "Update failed inserting");
				ID = db.insertDriverStatus(this);
			}
		}
		db.close();

	}
	
	

}
