package com.mobile.udrove.model;

public class LogsdataModel {
	
	private  String status_id = null;
	private  String hours = null;
	/**
	 * @param status_id the status_id to set
	 */
	public  void setStatus_id(String status_id) {
		this.status_id = status_id;
	}
	/**
	 * @return the status_id
	 */
	public String getStatus_id() {
		return status_id;
	}
	/**
	 * @param hours the hours to set
	 */
	public  void setHours(String hours) {
		this.hours = hours;
	}
	/**
	 * @return the hours
	 */
	public String getHours() {
		return hours;
	}

	
}
