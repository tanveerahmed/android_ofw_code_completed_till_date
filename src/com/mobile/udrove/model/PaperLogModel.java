package com.mobile.udrove.model;

public class PaperLogModel {

	private String paperLogId;
	private String paperLogDate;
	private int approved;
	private String paperLogUrl;

	public String getPaperLogId() {
		return paperLogId;
	}
	public void setPaperLogId(String paperLogId) {
		this.paperLogId = paperLogId;
	}
	public String getPaperLogDate() {
		return paperLogDate;
	}
	public void setPaperLogDate(String paperLogDate) {
		this.paperLogDate = paperLogDate;
	}
	public int getApproved() {
		return approved;
	}
	public void setApproved(int approved) {
		this.approved = approved;
	}
	public String getPaperLogUrl() {
		return paperLogUrl;
	}
	public void setPaperLogUrl(String paperLogUrl) {
		this.paperLogUrl = paperLogUrl;
	}
	
}
