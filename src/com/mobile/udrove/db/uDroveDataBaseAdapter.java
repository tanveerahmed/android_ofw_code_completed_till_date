package com.mobile.udrove.db;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.model.MetaDataModel_DutyStatus;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class uDroveDataBaseAdapter {

	public static final String DATABASE_NAME = "uDroveDataBase";
	public static final int DATABASE_VERSION = 10;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;
	private final Context context;

	public uDroveDataBaseAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);

	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);

		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			
			// TODO Auto-generated method stub
			try {

				db.execSQL("CREATE TABLE IF NOT EXISTS DRIVER_DETAILS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ "USER_ID TEXT,PASSWORD TEXT,SIGN_URL TEXT,SERVICE_IDS TEXT,LOG_DATE TEXT,FUEL_RECEIPT_REQUIRED TEXT,EXPENSE_RECEIPT_REQUIRED TEXT"
						+ ")");

				db.execSQL("CREATE TABLE IF NOT EXISTS HOS_DETAILS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ "DUTY_ROLE TEXT,AVAILABLE_DRIVE_TIME TEXT,AVAILABLE_ONDUTY_TIME TEXT,TOTAL_AVAILABLE_DRIVE_TIME TEXT,UPDATED_AT TEXT"
						+ ")");

				db.execSQL("CREATE TABLE IF NOT EXISTS LOGS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ "PAPER_LOG_ID TEXT,LOG_DATE TEXT,APPROVED TEXT,PAPER_LOG_URL TEXT"
						+ ")");

				db.execSQL("CREATE TABLE IF NOT EXISTS LOG_INFO"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,"
						+ "LOG_INFO_ID TEXT,LOG_DATE TEXT,COMMODITY TEXT,APPROVED TEXT,CO_DRIVER TEXT"
						+ ",PRO_SHIPPER TEXT,POWER_UNIT TEXT,TRAILER TEXT,OIL_FIELD_EXEMPTION_LINE TEXT,IN_DRIVING_FLAG TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS LOGS_DUTY_STATUSES"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,LOG_DATE TEXT,"
						+ "DUTY_STATUS_TYPE_ID TEXT,DUTY_STATUS_START_TIME TEXT,"
						+ "DUTY_STATUS_HOURS TEXT,REMARKS TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS DRIVER_lOADS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,LOAD_ID TEXT,"
						+ "LOAD_STATUS_TYPE_ID TEXT,TRACKING_NUMBER TEXT,"
						+ "LOAD_START_DATE TEXT,LOAD_END_DATE TEXT,ORIGIN_CITY TEXT,ORIGIN_STATE_ID TEXT,DEST_CITY TEXT,"
						+ "DEST_STATE_ID TEXT,START_ODOMETER_READING TEXT,END_ODOMETER_READING TEXT,SHIPPER TEXT,COMMODITY TEXT,"
						+ "PRO_SHIP TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS DUTY_STATUSES"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,DUTY_STATUS_ID INTEGER,DUTY_STATUS_NAME TEXT,DUTY_STATUS_CODE TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS STATES"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,STATE_ID INTEGER,STATE_NAME TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS POWER_UNITS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,POWER_UNIT_ID INTEGER,POWER_UNIT_NAME TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS EXPENSE_TYPES"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,EXPENSE_TYPE_ID INTEGER,EXPENSE_TYPE_NAME TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS POD_TYPES"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,POD_TYPE_ID INTEGER,POD_TYPE_NAME TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS EXEMPTION_LIST"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,EXEMPTION_ID INTEGER,EXEMPTION_NAME TEXT)");				
				
				db.execSQL("CREATE TABLE IF NOT EXISTS TRUCK_DEFECTS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,TRUCK_DEFECT_ID INTEGER,TRUCK_DEFECT_NAME TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS TRAILER_DEFECTS"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,TRAILER_DEFECT_ID INTEGER,TRAILER_DEFECT_NAME TEXT)");

				db.execSQL("CREATE TABLE IF NOT EXISTS GALLERY"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,IMG_NAME TEXT,IMG_DATA BLOB,IMG_THUMB_DATA BLOB)");

				db.execSQL("CREATE TABLE IF NOT EXISTS PERSONAL_CONVEYANCE"
						+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT,PC_FLAG TEXT)");

				System.out.println("Created all the tables in db");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

			db.execSQL("DROP TABLE IF EXISTS DRIVER_DETAILS");
			db.execSQL("DROP TABLE IF EXISTS DRIVER_lOADS");
			db.execSQL("DROP TABLE IF EXISTS LOGS_DUTY_STATUSES");
			db.execSQL("DROP TABLE IF EXISTS LOGS");
			db.execSQL("DROP TABLE IF EXISTS HOS_DETAILS");
			db.execSQL("DROP TABLE IF EXISTS LOG_INFO");
			db.execSQL("DROP TABLE IF EXISTS GALLERY");
			db.execSQL("DROP TABLE IF EXISTS PERSONAL_CONVEYANCE");

			db.execSQL("DROP TABLE IF EXISTS DUTY_STATUSES");
			db.execSQL("DROP TABLE IF EXISTS EXPENSE_TYPES");
			db.execSQL("DROP TABLE IF EXISTS STATES");
			db.execSQL("DROP TABLE IF EXISTS POD_TYPES");
			db.execSQL("DROP TABLE IF EXISTS POWER_UNITS");
			db.execSQL("DROP TABLE IF EXISTS TRUCK_DEFECTS");
			db.execSQL("DROP TABLE IF EXISTS EXEMPTION_LIST");
			db.execSQL("DROP TABLE IF EXISTS TRAILER_DEFECTS");

			onCreate(db);
		}

	}

	// ---opens the database---
	public uDroveDataBaseAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}

	// Driver Details

	public void insertDriverDetails(String userId, String password,
			String signUrl, String serviceIds, String fuelRequired,
			String expenseRequired) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("USER_ID", userId);
		initialValues.put("PASSWORD", password);
		initialValues.put("SIGN_URL", signUrl);
		initialValues.put("SERVICE_IDS", serviceIds);
		initialValues.put("FUEL_RECEIPT_REQUIRED", fuelRequired);
		initialValues.put("EXPENSE_RECEIPT_REQUIRED", expenseRequired);
		db.insert("DRIVER_DETAILS", null, initialValues);

	}

	public void insertIntoGallery(String filename, byte[] image)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("IMG_NAME", filename);
		initialValues.put("IMG_DATA", image);
		// initialValues.put("IMG_THUMB_DATA", thumbimage);
		db.insert("GALLERY", null, initialValues);

	}

	public byte[] getImageFromGallery(String filename) throws SQLException {

		Cursor mCursor = db.rawQuery(
				"SELECT IMG_DATA FROM GALLERY WHERE IMG_NAME=\"" + filename
						+ "\"", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			byte[] image = mCursor.getBlob(0);
			mCursor.close();
			return image;

		}
		return null;
	}

	public byte[] getThumbImageFromGallery(String filename) throws SQLException {

		Cursor mCursor = db.rawQuery(
				"SELECT IMG_THUMB_DATA FROM GALLERY WHERE IMG_NAME=\""
						+ filename + "\"", null);
		if (mCursor != null) {
			mCursor.moveToFirst();

			byte[] image = mCursor.getBlob(0);
			mCursor.close();
			return image;
		}
		return null;
	}

	public void deleteImageFromGallery(String filename) {

		db.execSQL("DELETE FROM GALLERY WHERE IMG_NAME=\"" + filename + "\"");

	}

	public void deleteGallery() {

		db.delete("GALLERY", null, null);

	}

	public void updateDriverSignImage(String imageurl) {

		db.execSQL("UPDATE DRIVER_DETAILS SET SIGN_URL=\"" + imageurl + "\"");

	}

	public boolean didAlreadyLoggedIn() {

		boolean result = false;
		Cursor mCursor = db.rawQuery("SELECT COUNT(*) FROM DRIVER_DETAILS",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			System.out
					.println("LOGINTAG: didAlreadyLoggedIn() cursor not null ");
			System.out.println("LOGINTAG:  cursor.getInt(0)= "
					+ mCursor.getInt(0));

			if (mCursor.getInt(0) == 1) {
				System.out.println("LOGINTAG: didAlreadyLoggedInresult true ");
				result = true;
				mCursor.close();
				return result;
			}
			mCursor.close();
		}
		return result;
	}

	public boolean isFuelReceiptRequired() {

		boolean result = false;
		Cursor mCursor = db.rawQuery(
				"SELECT FUEL_RECEIPT_REQUIRED FROM DRIVER_DETAILS", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			System.out
					.println("FUELTAG: isFuelReceiptRequired() cursor not null ");
			System.out.println("FUELTAG:  cursor.getString(0)= "
					+ mCursor.getString(0));

			if (mCursor.getString(0).equalsIgnoreCase("true")) {
				System.out.println("FUELTAG: isFuelReceiptRequired true ");
				result = true;
				mCursor.close();
				return result;
			}
			mCursor.close();
		}
		return result;
	}

	public boolean isExpenseReceiptRequired() {

		boolean result = false;
		Cursor mCursor = db.rawQuery(
				"SELECT EXPENSE_RECEIPT_REQUIRED FROM DRIVER_DETAILS", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			System.out
					.println("EXPENSETAG: isExpenseReceiptRequired() cursor not null ");
			System.out.println("EXPENSETAG:  cursor.getString(0)= "
					+ mCursor.getString(0));

			if (mCursor.getString(0).equalsIgnoreCase("true")) {
				System.out.println("EXPENSETAG: isExpenseReceiptRequired true");
				result = true;
				mCursor.close();
				return result;
			}
			mCursor.close();
		}
		return result;
	}

	public Cursor getDriverDetails() throws JSONException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT USER_ID,PASSWORD,SIGN_URL,SERVICE_IDS FROM DRIVER_DETAILS",
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}

	public void upDateLogDateForPromt() {
		db.execSQL("UPDATE DRIVER_DETAILS SET LOG_DATE=\""
				+ HTTPUtil.TodayDateOnly() + "\"");
	}

	public String getpromptedLogDate() {

		Cursor mCursor = db.rawQuery("SELECT LOG_DATE FROM DRIVER_DETAILS",
				null);
		if (mCursor != null && mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			String temp = mCursor.getString(0);
			mCursor.close();
			return temp;
		}
		return "noLogdate";
	}

	// HOS RELATED METHODS

	public void insertHosInfo(JSONObject json_hos, String updatedAt)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("DUTY_ROLE", json_hos.getString("driver_work_rule"));
		initialValues.put("AVAILABLE_DRIVE_TIME",
				json_hos.getString("available_drive_time"));
		initialValues.put("AVAILABLE_ONDUTY_TIME",
				json_hos.getString("available_on_duty_time"));
		initialValues.put("TOTAL_AVAILABLE_DRIVE_TIME",
				json_hos.getString("total_available_drive_time"));
		initialValues.put("UPDATED_AT", updatedAt);
		db.insert("HOS_DETAILS", null, initialValues);
	}

	public Cursor getHosDetails() throws JSONException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT DUTY_ROLE,AVAILABLE_DRIVE_TIME,AVAILABLE_ONDUTY_TIME,"
								+ "TOTAL_AVAILABLE_DRIVE_TIME,UPDATED_AT FROM HOS_DETAILS",
						null);
		if (mCursor != null) {
			if (mCursor.getCount() > 0) {
				mCursor.moveToFirst();
				return mCursor;

			}
		}
		mCursor.close();
		return null;

	}

	public void insertLogsForCurrentDay(String paperlogid, String logdate)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("PAPER_LOG_ID", paperlogid);
		initialValues.put("LOG_DATE", logdate);
		initialValues.put("APPROVED", "0");
		initialValues.put("PAPER_LOG_URL", "");
		db.insert("LOGS", null, initialValues);

	}

	public void upDateLogsPaperLogUrl(String paperLogId, String paperLogURL) {

		db.execSQL("UPDATE LOGS SET PAPER_LOG_URL=\"" + paperLogURL
				+ " \" WHERE PAPER_LOG_ID=\"" + paperLogId + "\"");
	}

	// ---insert a Logs into the database---
	public void insertLogs(JSONObject json_log) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("PAPER_LOG_ID", json_log.getString("paper_log_id"));
		initialValues.put("LOG_DATE", json_log.getString("log_date"));
		initialValues.put("APPROVED", json_log.getString("approved"));
		initialValues.put("PAPER_LOG_URL", "");
		db.insert("LOGS", null, initialValues);
		JSONArray dutyStatusArray = json_log.getJSONArray("duty_statuses");
		if (dutyStatusArray.length() > 0) {

			for (int i = 0; i < dutyStatusArray.length(); i++) {
				insertDutyStatuses(dutyStatusArray.getJSONObject(i),
						json_log.getString("log_date"));
			}
		}

	}

	/*
	 * public void upDatePaperLogUrl(String paperLogId, String paperLogURL) {
	 * 
	 * db.execSQL("UPDATE LOGS SET PAPER_LOG_URL=\"" + paperLogURL+
	 * "\",APPROVED=1 WHERE PAPER_LOG_ID=\"" + paperLogId +"\"");
	 * 
	 * }
	 */

	public void upDatePaperLogUrl(String paperLogId) {

		db.execSQL("UPDATE LOGS SET APPROVED=1 WHERE PAPER_LOG_ID=\""
				+ paperLogId + "\"");

	}

	public void insertLogInfo(JSONObject json_log_info) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues
				.put("LOG_INFO_ID", json_log_info.getString("log_info_id"));
		// initialValues.put("LOG_DATE",json_log_info.getString("log_date"));
		initialValues.put("LOG_DATE", HTTPUtil.TodayDateOnly());
		initialValues.put("COMMODITY", json_log_info.getString("commodity"));
		// initialValues.put("APPROVED", json_log_info.getString("approved"));
		initialValues.put("CO_DRIVER", json_log_info.getString("co_driver"));
		initialValues.put("PRO_SHIPPER",
				json_log_info.getString("pro_shipping_no"));
		initialValues.put("POWER_UNIT", json_log_info.getString("power_unit"));
		// initialValues.put("POWER_UNIT",
		// json_log_info.getString("account_vehicle_id"));
		initialValues.put("OIL_FIELD_EXEMPTION_LINE", json_log_info.getString("oilfield_exemption_line"));
		initialValues.put("TRAILER", json_log_info.getString("trailer"));
		initialValues.put("IN_DRIVING_FLAG",
				json_log_info.getString("in_driving"));

		db.insert("LOG_INFO", null, initialValues);

	}

	public Cursor getCurrentDayOrLastLogInfo() throws SQLException {
		System.out.println("Inside get currentday or last log info");
		Cursor mCursor = db.rawQuery(
				"SELECT * FROM LOG_INFO ORDER BY ID DESC LIMIT 1", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}
	
	public Cursor getTodaysExemptionLineStatus(String logDate) throws SQLException {
		System.out.println("Inside get getTodaysExemptionLineStatus or last log info");
		Cursor mCursor = db.rawQuery(
				"SELECT OIL_FIELD_EXEMPTION_LINE FROM LOG_INFO WHERE LOG_DATE=\""
						+ logDate + "\"", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}
	
	public Cursor getTodaysExemptionLineStatus() throws SQLException {
		System.out.println("Inside get getTodaysExemptionLineStatus or last log info");
		Cursor mCursor = db.rawQuery(
				"SELECT OIL_FIELD_EXEMPTION_LINE FROM LOG_INFO ORDER BY ID DESC LIMIT 1", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}

	// INSERT DUTY STATUS FOR A LOG DATE
	public void insertDutyStatuses(JSONObject json_duty_status, String logdate)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("LOG_DATE", logdate);
		initialValues.put("DUTY_STATUS_TYPE_ID",
				json_duty_status.getString("duty_status_type_id"));
		initialValues.put("DUTY_STATUS_START_TIME",
				json_duty_status.getString("duty_date_time"));
		initialValues.put("DUTY_STATUS_HOURS",
				json_duty_status.getString("duty_hours"));
		initialValues.put("REMARKS", json_duty_status.getString("remarks"));
		db.insert("LOGS_DUTY_STATUSES", null, initialValues);

	}

	// GET CURRENT DUTY STATUS OR LAST DUTY STATUS IN THE DATA
	public int getCurrentDutyStatus() throws SQLException {

		int result = 0;
		Cursor mCursor = db
				.rawQuery(
						"SELECT DUTY_STATUS_TYPE_ID FROM LOGS_DUTY_STATUSES ORDER BY ID DESC LIMIT 1",
						null);
		if (mCursor != null && mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			if (!mCursor.getString(0).equalsIgnoreCase("null")) {
				result = Integer.parseInt(mCursor.getString(0));
			}
		}
		mCursor.close();
		return result;
	}

	/*
	 * public String getStartTimeOfCurrentDutyStatus() {
	 * 
	 * String startDate = null; Cursor mCursor = db .rawQuery(
	 * "SELECT DUTY_STATUS_START_TIME FROM LOGS_DUTY_STATUSES ORDER BY ID DESC LIMIT 1"
	 * , null); if (mCursor != null && mCursor.getCount() > 0) {
	 * mCursor.moveToFirst(); startDate = mCursor.getString(0); }
	 * mCursor.close();
	 * 
	 * return startDate; }
	 */
	public String[] getLastSevenLogDates() throws SQLException {

		Cursor mCursor = db.rawQuery(
				"SELECT LOG_DATE FROM LOGS ORDER BY ID DESC LIMIT 8", null);

		if (mCursor != null && mCursor.getCount() >= 0) {
			mCursor.moveToFirst();
			String[] datesList = new String[mCursor.getCount()];
			int i = 0;
			if (mCursor.getCount() > 0) {
				do {
					datesList[i] = mCursor.getString(0);
					i++;
				} while (mCursor.moveToNext());
			}
			mCursor.close();
			return datesList;
		}
		return null;
	}

	public Cursor getLastSevenLogDetails() throws SQLException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT PAPER_LOG_ID,LOG_DATE,APPROVED,PAPER_LOG_URL FROM LOGS ORDER BY ID DESC LIMIT 8",
						null);

		if (mCursor != null && mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			return mCursor;
		}

		return null;
	}

	public Cursor getDutyStatusesForDate(String logDate) throws SQLException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT DUTY_STATUS_TYPE_ID,DUTY_STATUS_HOURS FROM LOGS_DUTY_STATUSES WHERE LOG_DATE=\""
								+ logDate + "\"", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}

	public double getTotalOnDutyHoursForDate(String logDate)
			throws SQLException {
		double totHours = 00.00;

		Cursor mCursor = db.rawQuery(
				"SELECT DUTY_STATUS_HOURS FROM LOGS_DUTY_STATUSES WHERE LOG_DATE=\""
						+ logDate
						+ "\"AND DUTY_STATUS_TYPE_ID NOT IN(\"2\",\"3\",\"11\") ",
				null);
		if (mCursor != null && mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			do {
				String doubleStr = mCursor.getString(0);
				if (doubleStr.equalsIgnoreCase("null")
						|| doubleStr.length() == 0) {
					doubleStr = "0";
				}
				totHours += Double.parseDouble(doubleStr);
			} while (mCursor.moveToNext());
		}
		mCursor.close();

		return totHours;

	}

	/* DRIVER LOADS RELATED METHODS */

	public void insertLoads(JSONObject json_load_info) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("LOAD_ID", json_load_info.getString("id"));
		initialValues.put("LOAD_STATUS_TYPE_ID",
				json_load_info.getString("load_status_type_id"));
		initialValues.put("TRACKING_NUMBER",
				json_load_info.getString("tracking_number"));
		initialValues.put("LOAD_START_DATE",
				json_load_info.getString("load_start_date"));
		initialValues.put("LOAD_END_DATE",
				json_load_info.getString("load_end_date"));
		initialValues.put("ORIGIN_CITY",
				json_load_info.getString("origin_city"));
		initialValues.put("ORIGIN_STATE_ID",
				json_load_info.getString("origin_state_id"));
		initialValues.put("DEST_CITY", json_load_info.getString("dest_city"));
		initialValues.put("DEST_STATE_ID",
				json_load_info.getString("dest_state_id"));
		initialValues.put("START_ODOMETER_READING",
				json_load_info.getString("start_odometer_reading"));
		initialValues.put("END_ODOMETER_READING",
				json_load_info.getString("end_odometer_reading"));
		initialValues.put("SHIPPER", json_load_info.getString("shipper"));
		initialValues.put("COMMODITY", json_load_info.getString("commodity"));
		initialValues.put("PRO_SHIP", json_load_info.getString("pro_ship"));

		db.insert("DRIVER_lOADS", null, initialValues);

	}

	public boolean isCurrentLoadCompleted() {

		boolean result = false;

		Cursor mCursor = db
				.rawQuery(
						"SELECT COUNT(*) FROM DRIVER_lOADS WHERE LOAD_STATUS_TYPE_ID=\"1\"",
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();

			if (mCursor.getInt(0) == 0) {
				result = true;
				mCursor.close();
				return result;
			}
			mCursor.close();
		}
		return result;
	}

	public Cursor getLastSevenLoadDates() throws SQLException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT LOAD_END_DATE FROM DRIVER_lOADS WHERE LOAD_STATUS_TYPE_ID=\"2\" ORDER BY LOAD_ID DESC",
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}

	public Cursor getCompletedLoadDetails(String load_end_date)
			throws SQLException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT LOAD_START_DATE,ORIGIN_STATE_ID,ORIGIN_CITY,END_ODOMETER_READING,SHIPPER,COMMODITY,"
								+ "PRO_SHIP,DEST_STATE_ID,DEST_CITY,LOAD_END_DATE FROM DRIVER_lOADS WHERE LOAD_END_DATE=\""
								+ load_end_date + "\"", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}

	public void insertCreatedLoads(JSONObject json_load_info, String loadId,
			String trackNum) throws JSONException {

		ContentValues initialValues = new ContentValues();
		int zonelength = AppUtil.getTimeZoneString().length();
		initialValues.put("LOAD_ID", loadId);
		initialValues.put("LOAD_STATUS_TYPE_ID", "1");
		initialValues.put("TRACKING_NUMBER", trackNum);
		initialValues.put(
				"LOAD_START_DATE",
				json_load_info.getString("load_start_date").substring(
						0,
						json_load_info.getString("load_start_date").length()
								- zonelength));
		initialValues.put(
				"LOAD_END_DATE",
				json_load_info.getString("load_end_date").substring(
						0,
						json_load_info.getString("load_end_date").length()
								- zonelength));
		initialValues.put("ORIGIN_CITY",
				json_load_info.getString("origin_city"));
		initialValues.put("ORIGIN_STATE_ID",
				json_load_info.getString("origin_state_id"));
		initialValues.put("DEST_CITY", json_load_info.getString("dest_city"));
		initialValues.put("DEST_STATE_ID",
				json_load_info.getString("dest_state_id"));
		initialValues.put("START_ODOMETER_READING",
				json_load_info.getString("start_odometer_reading"));
		initialValues.put("END_ODOMETER_READING", "");
		initialValues.put("SHIPPER", json_load_info.getString("shipper"));
		initialValues.put("COMMODITY", json_load_info.getString("commodity"));
		initialValues.put("PRO_SHIP", json_load_info.getString("pro_ship"));

		db.insert("DRIVER_lOADS", null, initialValues);
	}

	public void updateCurrentLoadDetails(JSONObject json_load_info,
			String loadId) throws JSONException {

		ContentValues initialValues = new ContentValues();
		String loadStatus = json_load_info.getString("load_status_type_id");
		initialValues.put("LOAD_STATUS_TYPE_ID",
				json_load_info.getString("load_status_type_id"));
		initialValues.put("LOAD_START_DATE",
				json_load_info.getString("load_start_date"));
		initialValues.put("LOAD_END_DATE",
				json_load_info.getString("load_end_date"));
		initialValues.put("ORIGIN_CITY",
				json_load_info.getString("origin_city"));
		initialValues.put("ORIGIN_STATE_ID",
				json_load_info.getString("origin_state_id"));
		initialValues.put("DEST_CITY", json_load_info.getString("dest_city"));

		if (json_load_info.getString("dest_state_id").trim().length() != 0) {
			initialValues.put("DEST_STATE_ID",
					json_load_info.getString("dest_state_id"));
		} else {
			initialValues.put("DEST_STATE_ID", "0");
		}
		initialValues.put("SHIPPER", json_load_info.getString("shipper"));
		initialValues.put("COMMODITY", json_load_info.getString("commodity"));
		initialValues.put("PRO_SHIP", json_load_info.getString("pro_ship"));

		if (loadStatus.equalsIgnoreCase("1")) {
			initialValues.put("START_ODOMETER_READING",
					json_load_info.getString("start_odometer_reading"));
		} else {
			initialValues.put("END_ODOMETER_READING",
					json_load_info.getString("end_odometer_reading"));
		}

		db.update("DRIVER_lOADS", initialValues, "LOAD_ID=\"" + loadId + "\"",
				null);
	}

	public Cursor getCurrentLoadDetails() throws SQLException {

		Cursor mCursor = db
				.rawQuery(
						"SELECT LOAD_START_DATE,ORIGIN_STATE_ID,ORIGIN_CITY,START_ODOMETER_READING,SHIPPER,COMMODITY,"
								+ "PRO_SHIP,DEST_STATE_ID,DEST_CITY,LOAD_END_DATE,LOAD_ID FROM DRIVER_lOADS WHERE LOAD_STATUS_TYPE_ID=\"1\"",
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			return mCursor;
		}
		return null;
	}

	public void dropAllTables() throws SQLException {

		db.delete("DRIVER_DETAILS", null, null);
		db.delete("DRIVER_lOADS", null, null);
		db.delete("LOGS_DUTY_STATUSES", null, null);
		db.delete("LOGS", null, null);
		db.delete("HOS_DETAILS", null, null);
		db.delete("LOG_INFO", null, null);

	}

	public void clearAllMetaDataTabelsRecords() throws SQLException {
		System.out.println("LOGINTAG:logout clearAllMetaDataTabelsRecords()  ");

		db.delete("DUTY_STATUSES", null, null);
		db.delete("EXPENSE_TYPES", null, null);
		db.delete("STATES", null, null);
		db.delete("POD_TYPES", null, null);
		db.delete("POWER_UNITS", null, null);
		db.delete("TRUCK_DEFECTS", null, null);
		db.delete("EXEMPTION_LIST", null, null);
		db.delete("TRAILER_DEFECTS", null, null);

	}

	public void clearAllTabelsRecords() throws SQLException {
		System.out.println("LOGINTAG:logout clearAllTabelsRecords()  ");
		db.delete("DRIVER_DETAILS", null, null);
		db.delete("DRIVER_lOADS", null, null);
		db.delete("LOGS_DUTY_STATUSES", null, null);
		db.delete("LOGS", null, null);
		db.delete("HOS_DETAILS", null, null);
		db.delete("LOG_INFO", null, null);
		db.delete("PERSONAL_CONVEYANCE", null, null);

		clearAllMetaDataTabelsRecords();
	}

	public void deleteCurrentDayDutyStatuses(String curDate) {

		db.execSQL("DELETE FROM LOGS_DUTY_STATUSES WHERE LOG_DATE=\"" + curDate
				+ "\"");

	}

	public void deleteCurrentDayLog(String curDate) {

		db.execSQL("DELETE FROM LOGS WHERE LOG_DATE=\"" + curDate + "\"");
	}

	public void deleteAllRecords(String tableName) throws SQLException {

		db.delete(tableName, null, null);
	}

	public boolean didAlreadyMetaDataLoaded() {

		boolean result = false;

		Cursor mCursor = db.rawQuery("SELECT COUNT(*) FROM STATES", null);
		if (mCursor != null) {
			mCursor.moveToFirst();

			System.out
					.println("METADATA: didAlreadyMetaDataLoaded() cursor not null ");
			System.out.println("METADATA:  cursor.getInt(0)= "
					+ mCursor.getInt(0));

			if (mCursor.getInt(0) > 0) {
				System.out.println("METADATA: didAlreadyMetaDataLoaded true ");
				result = true;
				mCursor.close();
				return result;
			}
			mCursor.close();
		}
		return result;
	}

	public void insertMetaData(JSONObject json_log) throws JSONException {

		System.out.println("Metadata Body String " + json_log);
		// Expense types
		JSONArray json_array = json_log.getJSONArray("expense_types");

		System.out.println("Metadata expense_types String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertExpenseTypes(json_array.getJSONObject(i));
			}

		}

		// Duty Statuses
		json_array = json_log.getJSONArray("duty_status_types");

		System.out.println("Metadata duty_status_types String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertDutyStatusTypes(json_array.getJSONObject(i));
			}

		}

		// States
		json_array = json_log.getJSONArray("states");
		System.out.println("Metadata states String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertStatesTypes(json_array.getJSONObject(i));
			}

		}

		// PODs
		json_array = json_log.getJSONArray("pod_types");
		System.out.println("Metadata pod_types String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertPodTypes(json_array.getJSONObject(i));
			}

		}

		// PowerUnits
		json_array = json_log.getJSONArray("power_units");
		System.out.println("Metadata power_units String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertPowerUnits(json_array.getJSONObject(i));
			}

		}

		// TruckDefects
		json_array = json_log.getJSONArray("truck_defects");
		System.out.println("Metadata truck_defects String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertTruckDefects(json_array.getJSONObject(i));
			}

		}
		
		// Exemptions
		json_array = json_log.getJSONArray("exemptions");
		System.out.println("Metadata exemptions String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertExemptions(json_array.getJSONObject(i));
			}

		}		

		// TrailerDefects
		json_array = json_log.getJSONArray("trailer_defects");
		System.out.println("Metadata trailer_defects String " + json_array);

		if (json_array.length() > 0) {

			for (int i = 0; i < json_array.length(); i++) {
				insertTrailerDefects(json_array.getJSONObject(i));
			}

		}

	}

	public void updatePersonalConveyance(String pc_flag) {

		ContentValues initialValues = new ContentValues();
	
		initialValues.put("PC_FLAG", pc_flag);
		
		db.delete("PERSONAL_CONVEYANCE", null, null);
	
		db.insert("PERSONAL_CONVEYANCE", null, initialValues);

	}
	
	public Boolean getPersonalConveyanceStatus() throws SQLException {

		
		boolean pc_flag = false;

		Cursor mCursor = db.rawQuery("SELECT PC_FLAG FROM PERSONAL_CONVEYANCE",
				null);
		
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				
				String flag = mCursor.getString(0);
				
				if(flag.equalsIgnoreCase(AppConstants.PC_FLAG_TRUE)){
					
					pc_flag= true;
				}else{
					
					pc_flag = false;
				}
			}
			mCursor.close();

		}
		return pc_flag;
	}


	private void insertDutyStatusTypes(JSONObject jsonObject)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("DUTY_STATUS_ID", 
				jsonObject.getInt("duty_status_id"));
		initialValues.put("DUTY_STATUS_NAME",
				jsonObject.getString("duty_status_name"));
		initialValues.put("DUTY_STATUS_CODE",
				jsonObject.getString("duty_status_code"));
		db.insert("DUTY_STATUSES", null, initialValues);

	}

	private void insertExpenseTypes(JSONObject expenseTypeObj)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("EXPENSE_TYPE_ID",
				expenseTypeObj.getInt("expense_type_id"));
		initialValues.put("EXPENSE_TYPE_NAME",
				expenseTypeObj.getString("expense_type_name"));
		db.insert("EXPENSE_TYPES", null, initialValues);

	}

	private void insertStatesTypes(JSONObject jsonObject) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("STATE_ID", jsonObject.getInt("state_id"));
		initialValues.put("STATE_NAME", jsonObject.getString("state_name"));
		db.insert("STATES", null, initialValues);

	}

	private void insertPodTypes(JSONObject jsonObject) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("POD_TYPE_ID", jsonObject.getInt("pod_type_id"));
		initialValues.put("POD_TYPE_NAME",
				jsonObject.getString("pod_type_name"));
		db.insert("POD_TYPES", null, initialValues);

	}

	private void insertPowerUnits(JSONObject jsonObject) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("POWER_UNIT_ID", jsonObject.getInt("power_unit_id"));
		initialValues.put("POWER_UNIT_NAME",
				jsonObject.getString("power_unit_name"));
		db.insert("POWER_UNITS", null, initialValues);

	}

	private void insertTruckDefects(JSONObject jsonObject) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("TRUCK_DEFECT_ID", jsonObject.getInt("defect_id"));
		initialValues.put("TRUCK_DEFECT_NAME",
				jsonObject.getString("defect_name"));
		db.insert("TRUCK_DEFECTS", null, initialValues);

	}

	private void insertExemptions(JSONObject jsonObject) throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("EXEMPTION_ID", jsonObject.getInt("exemption_id"));
		initialValues.put("EXEMPTION_NAME",
				jsonObject.getString("exemption_name"));
		db.insert("EXEMPTION_LIST", null, initialValues);

	}
	
	
	private void insertTrailerDefects(JSONObject jsonObject)
			throws JSONException {

		ContentValues initialValues = new ContentValues();
		initialValues.put("TRAILER_DEFECT_ID", jsonObject.getInt("defect_id"));
		initialValues.put("TRAILER_DEFECT_NAME",
				jsonObject.getString("defect_name"));
		db.insert("TRAILER_DEFECTS", null, initialValues);

	}

	public MetaDataModel getStatesList() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db.rawQuery("SELECT STATE_ID,STATE_NAME FROM STATES",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount() + 1];
				names = new String[mCursor.getCount() + 1];
				ids[0] = 0;
				names[0] = "Select State";
				int i = 1;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();
				return new MetaDataModel(ids, names);
			}
			mCursor.close();

		}
		return null;
	}

	public MetaDataModel getPowerUnitsTypes() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db.rawQuery(
				"SELECT POWER_UNIT_ID,POWER_UNIT_NAME FROM POWER_UNITS", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount() + 1];
				names = new String[mCursor.getCount() + 1];
				ids[0] = 0;
				names[0] = "Select Powerunit";
				int i = 1;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();

				return new MetaDataModel(ids, names);
			} else {
				ids = new int[mCursor.getCount() + 1];
				names = new String[mCursor.getCount() + 1];
				ids[0] = 0;
				names[0] = "Select Powerunit";
				mCursor.close();
				return new MetaDataModel(ids, names);

			}

		}
		return null;
	}

	public MetaDataModel getExpenseTypes() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db.rawQuery(
				"SELECT EXPENSE_TYPE_ID,EXPENSE_TYPE_NAME FROM EXPENSE_TYPES",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount() + 1];
				names = new String[mCursor.getCount() + 1];
				ids[0] = 0;
				names[0] = "Select Category";
				int i = 1;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();

				return new MetaDataModel(ids, names);
			}
			mCursor.close();

		}
		return null;
	}

	public MetaDataModel getPodTypes() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db.rawQuery(
				"SELECT POD_TYPE_ID,POD_TYPE_NAME FROM POD_TYPES", null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount() + 1];
				names = new String[mCursor.getCount() + 1];
				ids[0] = 0;
				names[0] = "Select Document Type";
				int i = 1;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();

				return new MetaDataModel(ids, names);
			}
			mCursor.close();

		}
		return null;
	}

	public MetaDataModel_DutyStatus getDutyStatusTypes() throws SQLException {

		int[] ids;
		String[] names;
		String[] codes;

		Cursor mCursor = db
				.rawQuery(
						"SELECT DUTY_STATUS_ID,DUTY_STATUS_NAME,DUTY_STATUS_CODE FROM DUTY_STATUSES WHERE DUTY_STATUS_ID>3",
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount() + 1];
				names = new String[mCursor.getCount() + 1];
				codes = new String[mCursor.getCount() + 1];
				ids[0] = 0;
				names[0] = "Select Duty Role";
				codes[0] = "Select Duty Code";
				int i = 1;
				do {
					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					codes[i] = mCursor.getString(2);
					i++;
				} while (mCursor.moveToNext());
				mCursor.close();
				return new MetaDataModel_DutyStatus(ids, names, codes);
			}
			mCursor.close();
		}
		return null;
	}
	
	public String getCurrentDutyStatusCode() throws SQLException {

		String result="OFF";
		Cursor mCursor = db
				.rawQuery(
						"SELECT DUTY_STATUS_CODE FROM DUTY_STATUSES ORDER BY ID DESC LIMIT 1",
						null);
		if (mCursor != null && mCursor.getCount() > 0) {
			mCursor.moveToFirst();
			if (!mCursor.getString(0).equalsIgnoreCase("null")) {
				result = mCursor.getString(0);
			}
		}
		mCursor.close();
		return result;
	}

	public MetaDataModel getTrailerDefectsTypes() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db
				.rawQuery(
						"SELECT TRAILER_DEFECT_ID,TRAILER_DEFECT_NAME FROM TRAILER_DEFECTS",
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount()];
				names = new String[mCursor.getCount()];
				int i = 0;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();

				return new MetaDataModel(ids, names);
			}
			mCursor.close();

		}
		return null;
	}

	public MetaDataModel getTruckDefectsTypes() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db.rawQuery(
				"SELECT TRUCK_DEFECT_ID,TRUCK_DEFECT_NAME FROM TRUCK_DEFECTS",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount()];
				names = new String[mCursor.getCount()];
				int i = 0;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();

				return new MetaDataModel(ids, names);
			}
			mCursor.close();

		}
		return null;
	}
	
	public MetaDataModel getExemptions() throws SQLException {

		int[] ids;
		String[] names;

		Cursor mCursor = db.rawQuery(
				"SELECT EXEMPTION_ID,EXEMPTION_NAME FROM EXEMPTION_LIST",
				null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
				ids = new int[mCursor.getCount()];
				names = new String[mCursor.getCount()];
				int i = 0;
				do {

					ids[i] = mCursor.getInt(0);
					names[i] = mCursor.getString(1);
					i++;

				} while (mCursor.moveToNext());
				mCursor.close();

				return new MetaDataModel(ids, names);
			}
			mCursor.close();

		}
		return null;
	}

	public String getDutyStatusCodeFromID(int driverStatus) throws SQLException {
		// TODO Auto-generated method stub
		String code;

		Cursor mCursor = db
				.rawQuery(
						"SELECT DUTY_STATUS_CODE FROM DUTY_STATUSES WHERE DUTY_STATUS_ID="+driverStatus,
						null);
		if (mCursor != null) {
			mCursor.moveToFirst();
			if (mCursor.getCount() > 0) {
					code = mCursor.getString(0);
					mCursor.close();
				return code;
			}
			mCursor.close();
		}
		return null;
	}

}
