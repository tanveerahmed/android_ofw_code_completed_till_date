package com.mobile.udrove.db;

import java.util.ArrayList;
import java.util.List;

import com.mobile.udrove.model.DVIRTable;
import com.mobile.udrove.model.LoadManagementTable;
import com.mobile.udrove.model.uDroveDriverStatus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class uDroveDbAdapter {

	/* Driver Status Table */
	/*
	 * Driver Status combination 0 for offduty -- default record 1 for onduty 2
	 * for sleeperberth 3 for driving
	 */
	public static final String KEY_ROWID = "_id";
	public static final String KEY_STATUS = "status";
	public static final String KEY_CITY = "city";
	public static final String KEY_STATE = "state";
	public static final String KEY_ODOMETER = "odometer";
	public static final String KEY_REMARKS = "remarks";
	public static final String KEY_STARTDATETIME = "startdatetime";
	public static final String KEY_ENDDATETIME = "enddatetime";

	/* Load Management DB */

	public static final String KEY_START_DATE = "startdate";
	public static final String KEY_ORIGIN_STATE = "originstate";
	public static final String KEY_ORIGIN_CITY = "origincity";
	public static final String KEY_LOAD_ODOMETER = "loadodometer";
	public static final String KEY_SHIPPER = "shipper";
	public static final String KEY_COMMODITY = "commodity";
	public static final String KEY_PRO_SHIP = "proship";
	public static final String KEY_DEST_STATE = "deststate";
	public static final String KEY_DEST_CITY = "destcity";
	public static final String KEY_END_DATE = "enddate";
	public static final String KEY_LOAD_STATUS_TYPE="loadstatustype";
	public static final String KEY_LOAD_ID="loadid";

	/*								*/

	public static final String KEY_zero = "zero";
	public static final String KEY_one = "one";
	public static final String KEY_two = "two";
	public static final String KEY_three = "three";
	public static final String KEY_four = "four";
	public static final String KEY_five = "five";
	public static final String KEY_six = "six";
	public static final String KEY_seven = "seven";

	public static final String KEY_eight = "eight";
	public static final String KEY_nine = "nine";
	public static final String KEY_ten = "ten";
	public static final String KEY_eleven = "eleven";
	public static final String KEY_twelve = "twelve";
	public static final String KEY_thirt = "thirt";
	public static final String KEY_fourt = "fourt";

	public static final String KEY_fivt = "fivt";
	public static final String KEY_sixt = "sixt";
	public static final String KEY_sevt = "sevt";
	public static final String KEY_eightt = "eightt";
	public static final String KEY_ninet = "ninet";
	public static final String KEY_twenty = "twenty";
	public static final String KEY_twentyone = "twentyone";

	public static final String KEY_twentytwo = "twentytwo";
	public static final String KEY_twentythree = "twentythree";
	public static final String KEY_twentyfour = "twentyfour";
	

	private static final String TAG = "udroveDBAdapter";

	private static final String DATABASE_NAME = "myudrove.db";
	private static final String DATABASE_TABLE_1 = "driverstatus";
	private static final String DATABASE_TABLE_2 = "loadmanagment";
	private static final String DATABASE_TABLE_3 = "dvirmanagment";

	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_TABLE_1_CREATE = "create table driverstatus(_id integer primary key autoincrement, "
			+ "status integer,"
			+ "city text not null,"
			+ "state text not null, "
			+ "odometer text not null,"
			+ "remarks text not null,"
			+ "startdatetime text not null,"
			+ "enddatetime text not null);";

	private static final String DATABASE_TABLE_2_CREATE = "create table loadmanagment(_id integer primary key autoincrement, "
			+ "startdate text not null,"
			+ "originstate text not null,"
			+ "origincity text not null, "
			+ "loadodometer text not null,"
			+ "shipper text not null,"
			+ "commodity text not null,"
			+ "proship text not null,"
			+ "deststate text not null,"
			+ "destcity text not null," + "enddate text not null," + "loadstatustype text not null," + "loadid text not null);";

	private static final String DATABASE_TABLE_3_CREATE = "create table dvirmanagment(_id integer primary key autoincrement, "
		+ "zero text not null," //date
		+ "one text not null,"
		+ "two text not null, "
		+ "three text not null,"
		+ "four text not null,"
		+ "five text not null,"
		+ "six text not null,"
		+ "seven  text not null,"
		+ "eight text not null,"
		+ "nine text not null,"
		+ "ten text not null, "
		+ "eleven text not null,"
		+ "twelve text not null,"
		+ "thirt text not null,"
		+ "fourt text not null,"
		+ "fivt  text not null,"
		+ "sixt text not null,"
		+ "sevt text not null,"
		+ "eightt text not null, "
		+ "ninet text not null,"
		+ "twenty text not null,"
		+ "twentyone text not null," 
		+ "twentytwo text not null," //ododmetetr,POWERUNIT,Trailer
		+ "twentythree text not null," + "twentyfour text not null);";// status

	private Context context;

	private DatabaseHelper uDroveDBHelper;
	private SQLiteDatabase uDroveDb;

	public uDroveDbAdapter(Context ctx) {
		this.context = ctx;
		uDroveDBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		private DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public synchronized void onCreate(SQLiteDatabase db) {
			Log.d("DATABASE", "Calling Table create");
			db.execSQL(DATABASE_TABLE_1_CREATE);
			db.execSQL(DATABASE_TABLE_2_CREATE);
			db.execSQL(DATABASE_TABLE_3_CREATE); // dvir

		}

		@Override
		public synchronized void onUpgrade(SQLiteDatabase db, int oldVersion,
				int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS driverstatus");
			db.execSQL("DROP TABLE IF EXISTS loadmanagment");
			db.execSQL("DROP TABLE IF EXISTS dvirmanagment");
			onCreate(db);
		}
	}

	// ---opens the database---
	public synchronized uDroveDbAdapter open() throws SQLException {
		Log.d("DATABASE", "Calling open");
		uDroveDb = uDroveDBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public synchronized void close() {
		try {
			Log.d("DATABASE", "Calling close");
			uDroveDb.close();
		} catch (Throwable error) {

		}
		try {
			Log.d("DATABASE", "Calling close");
			uDroveDBHelper.close();
		} catch (Throwable error) {

		}
	}

	// ---insert a settings into the database---
	// public long insertAccountSettings(accountSettings accountInfo, String
	// deviceid)
	public synchronized long insertDriverStatus(uDroveDriverStatus driverstatus) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_STATUS, driverstatus.getStatus());
		initialValues.put(KEY_CITY, driverstatus.getCity());
		initialValues.put(KEY_STATE, driverstatus.getState());
		initialValues.put(KEY_ODOMETER, driverstatus.getOdometer());
		initialValues.put(KEY_REMARKS, driverstatus.getRemarks());
		initialValues.put(KEY_STARTDATETIME, driverstatus.getstartDateTime());
		initialValues.put(KEY_ENDDATETIME, driverstatus.getendDateTime());
		return uDroveDb.insert(DATABASE_TABLE_1, null, initialValues);
	}

	public synchronized long insertLoadManagement(
			LoadManagementTable loadmanagmenttable) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(KEY_START_DATE, loadmanagmenttable.getStartDate());
		initialValues
				.put(KEY_ORIGIN_STATE, loadmanagmenttable.getOriginState());
		initialValues.put(KEY_ORIGIN_CITY, loadmanagmenttable.getOriginCity());
		initialValues.put(KEY_LOAD_ODOMETER,
				loadmanagmenttable.getLoadOdometer());
		initialValues.put(KEY_SHIPPER, loadmanagmenttable.getShipper());
		initialValues.put(KEY_COMMODITY, loadmanagmenttable.getCommodity());
		initialValues.put(KEY_PRO_SHIP, loadmanagmenttable.getProship());
		initialValues.put(KEY_DEST_STATE, loadmanagmenttable.getDestState());
		initialValues.put(KEY_DEST_CITY, loadmanagmenttable.getDestCity());
		initialValues.put(KEY_END_DATE, loadmanagmenttable.getEndDate());
		initialValues.put(KEY_LOAD_STATUS_TYPE, loadmanagmenttable.getLoadStatusType());
		initialValues.put(KEY_LOAD_ID, loadmanagmenttable.getLoadId());
		return uDroveDb.insert(DATABASE_TABLE_2, null, initialValues);
	}

	public synchronized long insertDVIR() {
		ContentValues initialValues = new ContentValues();
		
		System.out.println("The Date is " + DVIRTable.getKey0() + "Odometer is " + DVIRTable.getKey22() + "Power unit is " + DVIRTable.getKey23() 
			+ "Trailer Id " +	 DVIRTable.getKey24());
		initialValues.put(KEY_zero, DVIRTable.getKey0()); //Current date
		initialValues.put(KEY_one, DVIRTable.getKey1());

		initialValues.put(KEY_two, DVIRTable.getKey2());
		initialValues.put(KEY_three, DVIRTable.getKey3());
		initialValues.put(KEY_four, DVIRTable.getKey4());
		initialValues.put(KEY_five, DVIRTable.getKey5());
		initialValues.put(KEY_six, DVIRTable.getKey6());
		initialValues.put(KEY_seven, DVIRTable.getKey7());
		initialValues.put(KEY_eight, DVIRTable.getKey8());
		initialValues.put(KEY_nine, DVIRTable.getKey9());
		initialValues.put(KEY_ten, DVIRTable.getKey10());

		
		initialValues.put(KEY_eleven, DVIRTable.getKey11());
		initialValues.put(KEY_twelve, DVIRTable.getKey12());
		initialValues.put(KEY_thirt, DVIRTable.getKey13());
		initialValues.put(KEY_fourt, DVIRTable.getKey14());
		initialValues.put(KEY_fivt, DVIRTable.getKey15());
		initialValues.put(KEY_sixt, DVIRTable.getKey16());
		initialValues.put(KEY_sevt, DVIRTable.getKey17());
		initialValues.put(KEY_eightt, DVIRTable.getKey18());
		initialValues.put(KEY_ninet, DVIRTable.getKey19());

		initialValues.put(KEY_twenty, DVIRTable.getKey20());

		initialValues.put(KEY_twentyone, DVIRTable.getKey21());
		initialValues.put(KEY_twentytwo, DVIRTable.getKey22());
		initialValues.put(KEY_twentythree, DVIRTable.getKey23());
		initialValues.put(KEY_twentyfour, DVIRTable.getKey24());
		

		return uDroveDb.insert(DATABASE_TABLE_3, null, initialValues);
	}

	// ---retrieves all the calls---
	public synchronized Cursor getDVIRRecord(long rowId) {

		Cursor mCursor = uDroveDb.query(true, DATABASE_TABLE_3, new String[] {
				KEY_ROWID, KEY_zero, KEY_one, KEY_two, KEY_three, KEY_four, KEY_five, KEY_six,
				KEY_seven, KEY_eight, KEY_nine, KEY_ten, KEY_eleven, KEY_twelve, KEY_thirt, KEY_fourt,
				KEY_fivt, KEY_sixt, KEY_sevt, KEY_eightt, KEY_ninet, KEY_twenty, KEY_twentyone, KEY_twentytwo,
				KEY_twentythree, KEY_twentyfour }, KEY_ROWID + "=" + rowId, null, null,
				null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// ---retrieves all the calls---
	public synchronized List<LoadManagementTable> getAllLoadManagementRecords() {

		List<LoadManagementTable> loadmanagmentList = new ArrayList<LoadManagementTable>();

		Cursor mCursor = uDroveDb.query(true, DATABASE_TABLE_2, new String[] {
				KEY_ROWID, KEY_START_DATE, KEY_ORIGIN_STATE, KEY_ORIGIN_CITY,
				KEY_LOAD_ODOMETER, KEY_SHIPPER, KEY_COMMODITY, KEY_PRO_SHIP,
				KEY_DEST_STATE, KEY_DEST_CITY, KEY_END_DATE,KEY_LOAD_STATUS_TYPE,KEY_LOAD_ID }, null, null,
				null, null, null, null);
		// looping through all rows and adding to list
		if (mCursor.moveToFirst()) {
			do {
				LoadManagementTable loadmanagment = new LoadManagementTable();
				loadmanagment.setStartDate(mCursor.getString(1));
				loadmanagment.setOriginState(mCursor.getString(2));
				loadmanagment.setOriginCity(mCursor.getString(3));
				loadmanagment.setLoadOdometer(mCursor.getString(4));
				loadmanagment.setShipper(mCursor.getString(5));
				loadmanagment.setCommodity(mCursor.getString(6));
				loadmanagment.setProship(mCursor.getString(7));
				loadmanagment.setDestState(mCursor.getString(8));
				loadmanagment.setDestCity(mCursor.getString(9));
				loadmanagment.setEndDate(mCursor.getString(10));
				loadmanagment.setLoadStatusType(mCursor.getString(11));
				loadmanagment.setLoadId(mCursor.getString(12));

				// Adding contact to list
				loadmanagmentList.add(loadmanagment);
			} while (mCursor.moveToNext());
		}
		return loadmanagmentList;

	}

	// ---retrieves all the calls---
	public synchronized Cursor getAllLoadManagementCursor() {

		return uDroveDb.query(true, DATABASE_TABLE_2, new String[] { KEY_ROWID,
				KEY_START_DATE, KEY_ORIGIN_STATE, KEY_ORIGIN_CITY,
				KEY_LOAD_ODOMETER, KEY_SHIPPER, KEY_COMMODITY, KEY_PRO_SHIP,
				KEY_DEST_STATE, KEY_DEST_CITY, KEY_END_DATE,KEY_LOAD_STATUS_TYPE,KEY_LOAD_ID }, null, null,
				null, null, KEY_ROWID + " DESC", null);

	}
	
	
	// ---retrieves all the calls---
	public synchronized Cursor getAllDVIRCursor() {

		return uDroveDb.query(true, DATABASE_TABLE_3, new String[] {
				KEY_ROWID, KEY_zero, KEY_one, KEY_two, KEY_three, KEY_four, KEY_five, KEY_six,
				KEY_seven, KEY_eight, KEY_nine, KEY_ten, KEY_eleven, KEY_twelve, KEY_thirt, KEY_fourt,
				KEY_fivt, KEY_sixt, KEY_sevt, KEY_eightt, KEY_ninet, KEY_twenty, KEY_twentyone, KEY_twentytwo,
				KEY_twentythree, KEY_twentyfour }, null, null,
				null, null, null, null);

	}
	


	// ---deletes a particular ---
	public synchronized boolean deleteuDroveDutyStatus(long rowId) {
		return uDroveDb.delete(DATABASE_TABLE_1, KEY_ROWID + "=" + rowId, null) > 0;
	}

	// ---retrieves all the settings---
	public synchronized Cursor getAlluDroveDutyStatus() {
		return uDroveDb.query(DATABASE_TABLE_1, new String[] { KEY_ROWID,
				KEY_STATUS, KEY_CITY, KEY_STATE, KEY_ODOMETER, KEY_REMARKS,
				KEY_STARTDATETIME, KEY_ENDDATETIME,
		// KEY_DEVICEID,
				}, null, null, null, null, null);
	}

	// ---retrieves all the calls---
	public synchronized Cursor getLoadManagementRecord(long rowId) {

		Cursor mCursor = uDroveDb.query(true, DATABASE_TABLE_2, new String[] {
				KEY_ROWID, KEY_START_DATE, KEY_ORIGIN_STATE, KEY_ORIGIN_CITY,
				KEY_LOAD_ODOMETER, KEY_SHIPPER, KEY_COMMODITY, KEY_PRO_SHIP,
				KEY_DEST_STATE, KEY_DEST_CITY, KEY_END_DATE,KEY_LOAD_STATUS_TYPE,KEY_LOAD_ID }, KEY_ROWID + "="
				+ rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// ---retrieves a particular setting---
	public synchronized Cursor getuDroveDutyStatus(long rowId)
			throws SQLException {
		Cursor mCursor = uDroveDb.query(true, DATABASE_TABLE_1, new String[] {
				KEY_ROWID, KEY_STATUS, KEY_CITY, KEY_STATE, KEY_ODOMETER,
				KEY_REMARKS, KEY_STARTDATETIME, KEY_ENDDATETIME,
		// KEY_DEVICEID,
				}, KEY_ROWID + "=" + rowId, null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}

	// ---updates a setting---
	// public boolean updateAccountSettings(long rowId, accountSettings
	// accountInfo, String deviceid)
	public synchronized boolean updateuDroveDutyStatus(long rowId,
			uDroveDriverStatus driverstatus) {
		ContentValues args = new ContentValues();

		args.put(KEY_STATUS, driverstatus.getStatus());
		args.put(KEY_CITY, driverstatus.getCity());
		args.put(KEY_STATE, driverstatus.getState());
		args.put(KEY_ODOMETER, driverstatus.getOdometer());
		args.put(KEY_REMARKS, driverstatus.getRemarks());
		args.put(KEY_STARTDATETIME, driverstatus.getstartDateTime());
		args.put(KEY_ENDDATETIME, driverstatus.getendDateTime());
		// args.put(KEY_DEVICEID,"deviceId" );
		return uDroveDb.update(DATABASE_TABLE_1, args, KEY_ROWID + "=" + rowId,
				null) > 0;
	}

	// ---deletes a particular ---
	public synchronized boolean deleteLoadManagmentTableLastRecord(long rowId) {
		return uDroveDb.delete(DATABASE_TABLE_2, KEY_ROWID + "=" + rowId, null) > 0;

	}

	public synchronized void deleteLastRow() {
		int id = 0;
		// final String MY_QUERY = "SELECT MIN(_id) AS _id FROM loadmanagment";
		final String MY_QUERY = "DELETE from loadmanagment WHERE _id = (SELECT MIN(_id) FROM loadmanagment)";
		uDroveDb.execSQL(MY_QUERY);

	}

	public synchronized void deleteLoadRecords() {
		uDroveDb = uDroveDBHelper.getWritableDatabase();
		uDroveDb.delete(DATABASE_TABLE_2, null, null);
	}
	
	public synchronized void deleteDVIRRecords() {
		uDroveDb = uDroveDBHelper.getWritableDatabase();
		uDroveDb.delete(DATABASE_TABLE_3, null, null);
	}
	public synchronized void deleteAll() {
		uDroveDb = uDroveDBHelper.getWritableDatabase();
		uDroveDb.delete(DATABASE_TABLE_1, null, null);
	}
}
