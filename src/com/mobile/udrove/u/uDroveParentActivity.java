package com.mobile.udrove.u;

import java.util.ArrayList;
import java.util.HashMap;

import com.mobile.udrove.u.LocationHelperService;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class uDroveParentActivity extends Activity {

	public static final HashMap activities = new HashMap();
	private static boolean bLoginStatus = false;
	private static ArrayList<Activity> act = new ArrayList<Activity>(); 

 

	/**
	 * Initializes an instance to PennnyTalkActivity
	 */
	public uDroveParentActivity() {
		super();

	}
	
	

	@Override
	public void setContentView(int id) {
		try {
			super.setContentView(id);
			;
		} catch (Exception ex) {

		}
	}

	/**
	 * returns the instance of PennyTankActivity from custom UI stack managed by
	 * PennyTalk
	 * 
	 * @param c
	 * @return
	 */
	public static Activity getInstance(Class c) {
		return (Activity) activities.get(c);
	}

	/**
	 * cleans the activity from UI stack
	 */
	@Override
	public void finish() {

		try {
			// removes the activity from the UI stack.
			/*activities.remove(this.getClass());
			System.err.println(activities);
			Log.v("uDrove", "Activities in Stack :"
					+ activities.size());
			Log.v("uDrove", "Activities in Stack :" + activities);
			*/
		   act.remove(this); 
		   stopService(new Intent(getApplicationContext(),LocationHelperService.class));
		   super.finish();


		} catch (Throwable ex) {
			Log.v("uDrove", ex.getMessage(), ex);
		}

	}
	
	@Override 
    public void onDestroy() 
    { 
        super.onDestroy(); 
        activities.remove(this); 
    } 



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			act.add(this);
		} catch (Exception ex) {

		}

	}

	/**
	 * pushes the activity to UI stack that we intended to show
	 * 
	 * @param activity
	 * @param activityName
	 */
	public static final void showActivity(Activity activity, String activityName) {
		Intent myIntent = new Intent();
		myIntent
				.setClassName(activity.getClass().getPackage().getName(),
						activity.getClass().getPackage().getName() + "."
								+ activityName);
		activity.startActivity(myIntent);

	}

	/**
	 * pushes the activity to UI stack that we intended to show
	 * 
	 * @param activity
	 * @param pkgName
	 * @param activityName
	 */
	public static final void showActivity(Activity activity, String pkgName,
			String activityName) {
		Intent myIntent = new Intent();
		myIntent.setClassName(pkgName, activityName);
		activity.startActivity(myIntent);

	}

	/**
	 * Opens the page in the browser window.
	 * 
	 * @param url
	 */
	public void openPage(String url) {

		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}

	public static void setLoginStatus(boolean b) {
		bLoginStatus = b;
	}

	public static boolean getLoginStatus() {
		return bLoginStatus;
	}

	public void updateUI(Object messageToSend) {
		// TODO Auto-generated method stub

	}
	
/*
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
	          //preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
			System.out.println("Pressed Back Key");
	          return true;
	      }
		return super.onKeyDown(keyCode, event);
	}   
	*/
	
	public static void finishAll() 
    { 
	     for(Activity activity:act) 
        	if(activity!=null)
        		activity.finish(); 
    } 

}
