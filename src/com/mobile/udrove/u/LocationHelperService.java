package com.mobile.udrove.u;


import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveServiceListener;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class LocationHelperService extends Service implements UDroveServiceListener {

	  LocationManager locationManager ;
	  MyLocationListener listener;
		uDroveApplication udroveapp;
		JSONObject json;
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
	      
		System.out.println("In service on create method");
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		listener = new MyLocationListener();
		udroveapp = ((uDroveApplication) getApplication());

	}

	@Override
	public void onDestroy() {
		
		System.out.println("In service on destroy method");

		locationManager.removeUpdates(listener);

	}

	@Override
	public void onStart(Intent intent, int startId) {
		System.out.println("In service on start method");

		
		  locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,60000,100,listener);
		
	}
	
	  private class MyLocationListener implements LocationListener{

			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				
				System.out.println("Location changed Latitute::"+location.getLatitude()+"   Longitude::"+location.getLongitude());
				
		        json=new JSONObject();
				
			
				try {
				
					json.put("time_stamp", HTTPUtil.TodaysDate());
					json.put("userid", udroveapp.getUserid());
					json.put("password", udroveapp.getPassword());
					json.put("ping_date_time", HTTPUtil.TodaysDate());
					json.put("latitude",location.getLatitude());
					json.put("longitude",location.getLongitude());

					AsyncUpdate cls = new AsyncUpdate();
					cls.execute();
				
			    } catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}

			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				
			}

			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub
				
			}
			  
		  }

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub
		
		System.out.println("GPS UPDATE SERVICE CALLED " + posts);
			
		
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}
	
	
   public class AsyncUpdate extends AsyncTask<String, String, String> {
		
		private HashMap<String, String> mData = null;// post data
		byte[] result = null;
		String str = "";

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			HttpPost httput = new HttpPost(HTTPConstants.SEND_GPS_UPDATES);

			httput.setHeader("Accept", "application/json");
			httput.setHeader("Content-type", "application/json");

			StringEntity se = null;
			try {
				se = new StringEntity(json.toString());

				System.out.println("The SE is  request " + se.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httput.setEntity(se);

				HttpResponse response = httpclient.execute(httput);

				result = EntityUtils.toByteArray(response.getEntity());
				str = new String(result, "UTF-8");
				Log.v(">>>>>>>", "str:" + str);
			
				return str;
			
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {

			}
			return str;
		}

		@Override
		protected void onPostExecute(String posts) {

			System.out.println("GPS UPDATE SSERVICE CALLED " + posts);
			
		}
	
	}

	
}
