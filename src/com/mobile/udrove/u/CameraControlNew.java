package com.mobile.udrove.u;

import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;


public class CameraControlNew extends Activity {
	private MyPreview mPreview;
	private boolean gFocussed = false;
	private boolean gCameraPressed = false;
	private ProgressDialog myProgressDialog = null;
	private Handler handler = new Handler();
	

	String filename;
	uDroveDataBaseAdapter dbAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.gc();
		// Hide the window title.
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		android.provider.Settings.System.putInt(getContentResolver(),
				SCREEN_OFF_TIMEOUT, -1);
		
		dbAdapter = new uDroveDataBaseAdapter(this);


		mPreview = new MyPreview(this);
		setContentView(mPreview);
	}

	
	
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		System.gc();
		super.finish();
	}



	@Override
	protected void onPause() {

		super.onPause();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mPreview = new MyPreview(this);
		setContentView(mPreview);
		Button btn=new Button(this);
	        btn.setBackgroundResource(R.drawable.cam_icon);
	        btn.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL);
	        btn.setOnClickListener(new View.OnClickListener() {
	     		public void onClick(View v) {
	     			  try {
	     		            if(gFocussed){  
	     		                    mPreview.mCamera.takePicture(mShutterCallback,mPictureCallbackRaw,mPictureCallbackJpeg);
	     		            
	     		            }else if(!gFocussed){
	     		                    gCameraPressed = true;
	     		                    mPreview.mCamera.autoFocus(cb);
	     		            }
	     		            }catch(Exception e) {
	     		                    Log.i("Exc",e.toString());
	     		            }
				}
			});
	        
	        this.addContentView(btn,new LayoutParams(LayoutParams.WRAP_CONTENT,
	                LayoutParams.WRAP_CONTENT));
		
	}



	public boolean onTouch(View v, MotionEvent event) {

		return false;

	}
	
	  public void finishWithResult(){
		   	 Bundle conData = new Bundle();
		     conData.putString("filename",filename);

		     Intent intent = new Intent();
		   	 intent.putExtras(conData);
		   	 
		   	 setResult(RESULT_OK, intent);
		   	 finish();
			 
		    }

	private void returnRes() {

		myProgressDialog.dismiss();
		finishWithResult();
		
		
	}

	private Runnable doReturnRes = new Runnable() {
		public void run() {

			returnRes();
		}
	};

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			new AlertDialog.Builder(this).setTitle("uDrove")
					.setMessage("Application Disables the Back Button")
					.setPositiveButton(android.R.string.ok, null).show();
			return true;
		}

		if (keyCode == KeyEvent.KEYCODE_CAMERA
				|| keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			try {
				if (gFocussed) {
				
						mPreview.mCamera.takePicture(mShutterCallback,
								mPictureCallbackRaw, mPictureCallbackJpeg);
				} else if (!gFocussed) {
					gCameraPressed = true;
					mPreview.mCamera.autoFocus(cb);
				}
			} catch (Exception e) {
			}
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_FOCUS) {
			if (event.getRepeatCount() == 0) {
				if (!gFocussed)
					mPreview.mCamera.autoFocus(cb);
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_FOCUS:
			gFocussed = false;
			return true;
		}
		return false;
	}
	
	

	Camera.PictureCallback mPictureCallbackRaw = new Camera.PictureCallback() {

		public void onPictureTaken(byte[] data, Camera c) {
		
		}
	};


	Camera.PictureCallback mPictureCallbackJpeg = new Camera.PictureCallback() {

		public void onPictureTaken(final byte[] data, Camera c) {

			Runnable doBackgroundProc1 = new Runnable() {
				public void run() {
					try {

					
						filename = String.valueOf(System.currentTimeMillis()) ;
					   try {

							Bitmap    sourceBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
							ByteArrayOutputStream outStreamthumb =new  ByteArrayOutputStream();
							sourceBitmap.compress(Bitmap.CompressFormat.JPEG,75, outStreamthumb);
							sourceBitmap=null;
   					
	/*						
	              			sourceBitmap=	Bitmap.createScaledBitmap(sourceBitmap, 100, 100, false);

							int width = sourceBitmap.getWidth();
							int height = sourceBitmap.getHeight();

							int newWidth = 100;
							int newHeight =100;

							float scaleWidth = ((float) newWidth) / width;
							float scaleHeight = ((float) newHeight) / height;
							
							Matrix matrix = new Matrix();
							matrix.postScale(scaleWidth, scaleHeight);
							matrix.postRotate(90);

							sourceBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, width, height,
									matrix, true);

							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							sourceBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
*/
							dbAdapter.open();
							dbAdapter.insertIntoGallery(filename, outStreamthumb.toByteArray());
							dbAdapter.close();
							outStreamthumb.close();
		
						} catch (FileNotFoundException fne) {
							fne.printStackTrace();
							mPreview.mCamera.startPreview();
						} catch (IOException ioe) {
							Log.e("writing and scanning image ", ioe.toString());
							ioe.printStackTrace();
							mPreview.mCamera.startPreview();
						} catch (Exception e) {

						}
					} catch (Exception e) {
					}

					handler.post(doReturnRes);
				}
			};
			Thread thread2 = new Thread(null, doBackgroundProc1, "Background2");
			thread2.start();

			myProgressDialog = ProgressDialog.show(CameraControlNew.this,
					"Save Image...", "Please wait...", true);

		}
		
	};


	
	Camera.ShutterCallback mShutterCallback = new Camera.ShutterCallback() {
		public void onShutter() {
			Log.i(getClass().getSimpleName(), "SHUTTER CALLBACK");
		}
	};
	Camera.AutoFocusCallback cb = new Camera.AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera c) {
			if (success) {

				ToneGenerator tg = new ToneGenerator(
						AudioManager.STREAM_SYSTEM, 80);
				if (tg != null)
					tg.startTone(ToneGenerator.TONE_PROP_BEEP2);
				gFocussed = true;
				try {
					if (gCameraPressed) {
							mPreview.mCamera.takePicture(mShutterCallback,
									mPictureCallbackRaw, mPictureCallbackJpeg);
					}
				} catch (Exception e) {
					Log.i("Exc", e.toString());
				}
			} else {
				

				try {
					if (gCameraPressed) {
							mPreview.mCamera.takePicture(mShutterCallback,
									mPictureCallbackRaw, mPictureCallbackJpeg);
					}
				} catch (Exception e) {
					Log.i("Exc", e.toString());
				}

			}

		}
	};
}

class MyPreview extends SurfaceView implements SurfaceHolder.Callback {
	SurfaceHolder mHolder;
	Camera mCamera;

	MyPreview(Context context) {
		super(context);

		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface i s created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// The Surface has been created, acquire the camera and tell it where
		// to draw.
		// Log.i("surface created","--");

		try {
			mCamera = Camera.open();
			mCamera.setPreviewDisplay(holder);
		

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
	
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
          // preview surface does not exist
          return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
          // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e){
        }
    }

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	}
	
	
}