package com.mobile.udrove.u;


public class DriverStatus 
{
	int status;
	double hours;
   public DriverStatus(int status,double hours)
   {
	   this.status=status;
	   this.hours=hours;
   }
   public int getDutyStatus() {
	    return this.status;
	  }
   public void setDutyStatus(int status)
   {
	   this.status=status;
   }
   public double getHours()
   {
	   return this.hours;
   }
   public void setHours(double hours)
   {
	   this.hours=hours;
   }
}
