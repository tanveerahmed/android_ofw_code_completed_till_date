package com.mobile.udrove.u;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.util.AppUtil;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the Hours of Service information includes
 *              hours and logs of the drivers
 * 
 * @dated 16-Feb-2012
 */

public class uDroveHosHomeActivity extends uDroveParentActivity {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */
	TextView tv;

	uDroveApplication udroveapp;
	private ProgressDialog dialog;
	JSONObject json;
	uDroveDataBaseAdapter dbAdapter;

	int mServiceCount = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hoshome);
		tv = (TextView) findViewById(R.id.txtdutyhours);

		onclickListener listener = new onclickListener();
		findViewById(R.id.logsButton).setOnClickListener(listener);
		findViewById(R.id.hoursButton).setOnClickListener(listener);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveHosHomeActivity.this);
		udroveapp = ((uDroveApplication) getApplication());
		dbAdapter = new uDroveDataBaseAdapter(this);

		dbAdapter.open();
		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

			

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		dialog = new ProgressDialog(this);
		new GetLastSevenLogs().execute(HTTPConstants.GET_LAST_SEVEN_LOGS);

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.logsButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveHosLogsActivity");
				startActivity(openActivity);
				break;

			case R.id.hoursButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveHosHoursActivity");
				startActivity(openActivity);
				break;
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			String avhrs = AppUtil.getUpdatedAvailableDriveTime(this, tv);
			tv.setText(avhrs);
		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private class GetLastSevenLogs extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.v(">>>>>>>>>>>>>>>>", "onPreExecute");
			dialog.setMessage(HTTPConstants.PREPARING);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			json = new JSONObject();
			HttpPost httput = new HttpPost(urls[0]);
			httput.setHeader("Accept", "application/json");
			httput.setHeader("Content-type", "application/json");

			StringEntity se = null;
			String str = "";
			try {

				json.put("time_stamp", HTTPUtil.TodaysDate());
				json.put("userid", udroveapp.getUserid());
				json.put("password", udroveapp.getPassword());
				se = new StringEntity(json.toString());
				System.out.println("The SE is  request " + se.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httput.setEntity(se);
				HttpResponse response = httpclient.execute(httput);
				byte[] result = EntityUtils.toByteArray(response.getEntity());
				str = new String(result, "UTF-8");
				Log.v(">>>>>>>", "str:" + str);

				return str;

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {

			}
			return str;

		}

		@Override
		protected void onPostExecute(String posts) {

			dialog.dismiss();

			if (posts != null) {
				// dutyHoursTv.setText(HTTPUtil.TodaysDate());
				String result = HTTPUtil.ParseJson(posts);
				String spilt[] = result.split(",");
				if (spilt[0] != null && spilt[0].equals("FAIL")) {
					System.out.println("Paper Logss got failed");
				} else {

					populateLogsToDB(posts);

				}
			}
		}
	}

	public void populateLogsToDB(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);
			dbAdapter.open();
			JSONArray logsArray = jObject.getJSONArray("body");
			if (logsArray.length() > 0) { // Data at server side

				dbAdapter.deleteAllRecords("LOGS");
				dbAdapter.deleteAllRecords("LOGS_DUTY_STATUSES");

				for (int i = 0; i < logsArray.length(); i++) {

					dbAdapter.insertLogs((logsArray.getJSONObject(i))
							.getJSONObject("log_info"));

				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dbAdapter.close();
		}
		dbAdapter.close();
	}

}