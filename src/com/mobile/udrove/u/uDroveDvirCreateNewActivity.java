package com.mobile.udrove.u;

import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;

import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.DVIRItemModel;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.UIUtils;
import com.mobile.udrove.util.uDroveBase64;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the dvir create activity
 * 
 * @dated 16-Feb-2012
 */

public class uDroveDvirCreateNewActivity extends uDroveParentActivity implements
		UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	uDroveApplication udroveapp;
	HashMap<Integer, DVIRItemModel> hashmapdviritem = null;
	private JSONArray jArray = null;
	private JSONObject jsonObjSend = null;
	private int index;

	TableLayout truckLayout, trailerLayout;

	uDroveDataBaseAdapter dbAdapter;

	int[] powerUnitIds;
	String[] powerUnitNames;

	int[] truckDefectIds;
	String[] truckDefectNames;

	int[] trailerDefectIds;
	String[] trailerDefectNames;

	CheckBox currentCheckedCB;
	int fromCheckBoxChanged = 0;

	Iterator myVeryOwnIterator1;

	HashMap<Integer, String> inspectionData = new HashMap<Integer, String>();
	HashMap<Integer, String> inspectionTruckData = new HashMap<Integer, String>();
	HashMap<Integer, String> inspectionTrailerData = new HashMap<Integer, String>();

	String selectedItem = "";
	int intSelectedItem = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvircreate);

		udroveapp = ((uDroveApplication) getApplication());
		uDroveApplication.setDefaultHashmapdviritemmodel();
		uDroveApplication.initialiseDvirInspectionItems();

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveDvirCreateNewActivity.this);

		onclickListener listener = new onclickListener();

		findViewById(R.id.dvirapproveButton).setOnClickListener(listener);

		oncheckedChangeListener checkedlistner = new oncheckedChangeListener();

		truckLayout = (TableLayout) findViewById(R.id.TruckLayout1);
		trailerLayout = (TableLayout) findViewById(R.id.TrailerLayout1);

		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		MetaDataModel puModal = dbAdapter.getPowerUnitsTypes();
		MetaDataModel truckDefectModal = dbAdapter.getTruckDefectsTypes();
		MetaDataModel trailerDefectModal = dbAdapter.getTrailerDefectsTypes();

		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		if (puModal != null) {
			powerUnitIds = puModal.getIdList();
			powerUnitNames = puModal.getNameList();
		}

		Spinner powerUnit = (Spinner) findViewById(R.id.poweUnitspiner);

		powerUnit.setFocusable(true);
		powerUnit.setFocusableInTouchMode(true);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, powerUnitNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		powerUnit.setAdapter(adapter);
		powerUnit.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				selectedItem = powerUnitNames[arg2];
				intSelectedItem = powerUnitIds[arg2];
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		if (truckDefectModal != null) {
			truckDefectIds = truckDefectModal.getIdList();
			truckDefectNames = truckDefectModal.getNameList();
		}

		if (trailerDefectModal != null) {
			trailerDefectIds = trailerDefectModal.getIdList();
			trailerDefectNames = trailerDefectModal.getNameList();
		}

		for (int k = 0; k < truckDefectIds.length; k++) {

			inspectionData.put(truckDefectIds[k], truckDefectNames[k]);
			inspectionTruckData.put(truckDefectIds[k], truckDefectNames[k]);
		}

		for (int k = 0; k < trailerDefectIds.length; k++) {

			inspectionData.put(trailerDefectIds[k], trailerDefectNames[k]);
			inspectionTrailerData.put(trailerDefectIds[k],
					trailerDefectNames[k]);

		}

		TableRow tr = null;
		TextView tv = null;
		CheckBox cb1 = null;
		int i;
		String[] temp;
		for (i = 0; i < truckDefectIds.length; i++) {

			temp = truckDefectNames[i].split("-");

			tr = new TableRow(this);
			tr.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
			tv = new TextView(this);
			tv.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));

			tv.setText(temp[1]);

			tv.setTextColor(Color.parseColor("#000000"));

			cb1 = new CheckBox(this);
			cb1.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			cb1.setChecked(false);
			cb1.setOnCheckedChangeListener(checkedlistner);
			cb1.setTag(truckDefectIds[i]);

			tr.addView(tv);
			tr.addView(cb1);

			truckLayout.addView(tr);
		}

		for (i = 0; i < trailerDefectIds.length; i++) {

			temp = trailerDefectNames[i].split("-");

			System.out.println("Trailer Defect id = " + trailerDefectIds[i]
					+ " name = " + temp[1]);

			tr = new TableRow(this);
			tr.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

			tv = new TextView(this);
			tv.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));

			tv.setText(temp[1]);
			tv.setTextColor(Color.parseColor("#000000"));

			cb1 = new CheckBox(this);
			cb1.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			cb1.setChecked(false);
			cb1.setOnCheckedChangeListener(checkedlistner);
			cb1.setTag(trailerDefectIds[i]);

			tr.addView(tv);
			tr.addView(cb1);

			trailerLayout.addView(tr);
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (fromCheckBoxChanged == 1) {
			fromCheckBoxChanged = 0;

			if (uDroveApplication.fromBackButton == true) {
				uDroveApplication.fromBackButton = false;
				currentCheckedCB.setChecked(false);

			}

		}

	}

	protected class oncheckedChangeListener implements OnCheckedChangeListener {

		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			int checkboxId = (Integer) buttonView.getTag();
			String checkboxName = inspectionData.get(checkboxId);

			if (isChecked) {
				currentCheckedCB = (CheckBox) buttonView;
				fromCheckBoxChanged = 1;
				Intent openActivity = new Intent();
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.DvirSettingsActivity");
				openActivity.putExtra("checkboxName", checkboxName);
				openActivity.putExtra("checkboxId", checkboxId);
				startActivity(openActivity);
			} else {

				uDroveApplication.hashmapdviritemmodel.remove(checkboxId);

			}

		}
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {

			case R.id.dvirapproveButton:
				index = 0;
				String odometer = ((EditText) findViewById(R.id.txtodometer))
						.getText().toString();

				String trailer = ((EditText) findViewById(R.id.txttrailer))
						.getText().toString();

				validatedvir(odometer, trailer);

				break;

			}

		}
	}

	private void validatedvir(String odometer, String trailer) {

		String errormsg = "";
		boolean complete = true;

		if (intSelectedItem == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.POWERUNIT + "\n";
		}

		if (trailer == null || trailer.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.TRAILER + "\n";
		}

		if (!complete) {
			UIUtils.showError(uDroveDvirCreateNewActivity.this, errormsg);
		} else {

			jsonObjSend = new JSONObject();

			try {
				jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
				jsonObjSend.put("userid", udroveapp.getUserid());
				jsonObjSend.put("password", udroveapp.getPassword());
				jsonObjSend.put("odometer", odometer);
				jsonObjSend.put("power_unit", intSelectedItem);
				jsonObjSend.put("trailer", trailer);

				jArray = new JSONArray();

				JSONObject expenses = new JSONObject();
				hashmapdviritem = new HashMap<Integer, DVIRItemModel>();
				hashmapdviritem = uDroveApplication.getHashmapdviritemmodel();
				Log.v(">>>>>>>",
						"hashmapdviritem  size" + hashmapdviritem.size());
				if (!hashmapdviritem.isEmpty()) {

					myVeryOwnIterator1 = hashmapdviritem.keySet().iterator();
					index = 0;
					uploadImage();

					if (index == hashmapdviritem.size()) {
						makeServiceCall();
					}

				} else {

					// jArray.put(expenses);
					makeServiceCall();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete " + posts);

		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");

		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {
				Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
				finish();

			}

		} else {

			UIUtils.showSubscriptionChangedMessage(this);
		}

	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	public void uploadImage() {

		if (myVeryOwnIterator1.hasNext()) {

			Integer inspectionItemId = (Integer) myVeryOwnIterator1.next();

			DVIRItemModel dvirItem = hashmapdviritem.get(inspectionItemId);
			JSONObject json = new JSONObject();

			if (!dvirItem.getImageUri().equalsIgnoreCase("")) {

				dbAdapter.open();
				byte[] ba = dbAdapter.getImageFromGallery(dvirItem
						.getImageUri());
				dbAdapter.close();

				try {

					json.put("userid", udroveapp.getUserid());
					json.put("password", udroveapp.getPassword());
					int categoryId = inspectionItemId;

					if (inspectionTruckData.containsKey(categoryId)) {

						json.put("category", "DTU");

					} else if (inspectionTrailerData.containsKey(categoryId)) {

						json.put("category", "DTR");

					}

					json.put("sub_category", categoryId);
					json.put("file_ext", "jpg");
					json.put("bindata", uDroveBase64.encodeToString(ba,
							uDroveBase64.DEFAULT));
					ba = null;
					HttpServiceListener listener = new HttpServiceListener();

					UDroveService serv = new UDroveService(
							uDroveDvirCreateNewActivity.this, json, listener);
					serv.execute(HTTPConstants.IMAGE_UPLOAD,
							String.valueOf(categoryId));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {
				try {

					json.put("inspection_item_type_id", inspectionItemId);
					json.put("remarks", dvirItem.getRemarks());
					jArray.put(json);
					index++;

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	protected class HttpServiceListener implements UDroveServiceListener {

		public void onServiceComplete(String posts) {

		}

		public void onServiceComplete(String posts, String reference) {

			String result = HTTPUtil.ParseImageJson(posts);

			String split[] = result.split(",");
			/* Log.v(">>>>>>>>>>>>>", "result" + result); */
			if (split[0] != null && split[0].equals("FAIL")) {
				UIUtils.showServiceError(uDroveDvirCreateNewActivity.this,
						split[1]);
			} else {
				try {
					// String imageName =
					// split[2].substring(split[2].lastIndexOf("/") + 1);
					JSONObject expenses = new JSONObject();
					expenses.put("inspection_item_type_id", reference);
					expenses.put("inspection_item_image_name", split[2]);
					DVIRItemModel dvir = hashmapdviritem.get(Integer
							.parseInt(reference));
					expenses.put("remarks", dvir.getRemarks());
					jArray.put(expenses);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			index++;
			if (index == hashmapdviritem.size()) {
				makeServiceCall();
			} else {
				uploadImage();

			}

		}
	}

	private void makeServiceCall() {

		try {
			jsonObjSend.put("inspection_items", jArray);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UDroveService serv = new UDroveService(this, jsonObjSend, this);
		serv.execute(HTTPConstants.CREATE_DVIR_URL);

	}

	@Override
	public void finish() {
		
		dbAdapter.open();
		dbAdapter.deleteGallery();
		dbAdapter.close();

		super.finish();
	}

}