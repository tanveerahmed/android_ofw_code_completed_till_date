package com.mobile.udrove.u;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDrovePersonalUseService;
import com.mobile.udrove.service.UDrovePersonalUseServiceListener;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;

import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the Load Management home of the uDrove Mobile
 *              apps
 * 
 * @dated 16-Feb-2012
 */

public class uDroveCreateLoadActivity extends uDroveParentActivity implements
		UDroveServiceListener,UDrovePersonalUseServiceListener {

	/* uDrove Business Tools Home class variables */

	/**
	 * {@inheritDoc}
	 */
	private String selectedOrginState, selectedDestState;
	int intselectedOrginState, intselectedDestState;
	uDroveApplication udroveapp;
	EditText edttxtstartdate, edttxtenddate;
	JSONObject jsonObjSend;
	uDroveDataBaseAdapter dbAdapter;

	int[] stateIds;
	String[] stateNames;

	static final int DATE_DIALOG_ID = 0, DATE_DIALOG_IDS = 1;
	int gCheckService = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.createload);
		udroveapp = ((uDroveApplication) getApplication());
		dbAdapter = new uDroveDataBaseAdapter(this);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveCreateLoadActivity.this);

		onclickListener listener = new onclickListener();
		findViewById(R.id.submitButton).setOnClickListener(listener);

		dbAdapter.open();

		MetaDataModel stateModal = dbAdapter.getStatesList();

		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				System.out
						.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"
								+ cur.getString(0) + cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		if (stateModal != null) {
			stateIds = stateModal.getIdList();
			stateNames = stateModal.getNameList();

		}

		Spinner spinneroriginstate = (Spinner) findViewById(R.id.originstate);
		spinneroriginstate.setFocusable(true);
		spinneroriginstate.setFocusableInTouchMode(true);

		ArrayAdapter<String> stateadapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stateNames);
		stateadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinneroriginstate.setAdapter(stateadapter);
		spinneroriginstate
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						selectedOrginState = stateNames[arg2];
						intselectedOrginState = stateIds[arg2];
					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}

				});

		Spinner spinnerdeststate = (Spinner) findViewById(R.id.deststate);
		ArrayAdapter<String> deststateadapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stateNames);
		deststateadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinnerdeststate.setAdapter(deststateadapter);
		spinnerdeststate
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						selectedDestState = stateNames[arg2];
						intselectedDestState = stateIds[arg2];
					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}

				});

		/* DateTime Field set */
		edttxtstartdate = (EditText) findViewById(R.id.txtstartdatetime);
		edttxtenddate = (EditText) findViewById(R.id.txtenddatetime);

		// display the current date
		updateStartDateDisplay();
		updatEndDateDisplay();

		edttxtenddate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_IDS);
			}
		});

		edttxtstartdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_ID);
			}
		});

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.submitButton:

				String startdate = ((EditText) findViewById(R.id.txtstartdatetime))
						.getText().toString();
				String city = ((EditText) findViewById(R.id.txtcity)).getText()
						.toString();

				String shipper = ((EditText) findViewById(R.id.txtshipper))
						.getText().toString();

				String comm = ((EditText) findViewById(R.id.txtcomm)).getText()
						.toString();

				String proship = ((EditText) findViewById(R.id.txtproship))
						.getText().toString();

				String destcity = ((EditText) findViewById(R.id.txtdestcity))
						.getText().toString();

				String endate = ((EditText) findViewById(R.id.txtenddatetime))
						.getText().toString();

				String odometer = ((EditText) findViewById(R.id.txtodometer))
						.getText().toString();

				validatedutystatus(startdate, selectedOrginState, city,
						odometer, shipper, comm, proship, selectedDestState,
						destcity, endate);

				break;

			}

		}
	}

	private void validatedutystatus(String startdate,
			String selectedOrginState, String city, String odometer,
			String shipper, String comm, String proship,
			String selectedDestState, String destcity, String endate) {
		// TODO Auto-generated method stub

		String errormsg = "";
		boolean complete = true;
		if (startdate == null || startdate.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.START_DATE + "\n";
		}
		if (selectedOrginState.equals("Select any State")) {
			complete = false;
			errormsg = errormsg + AppConstants.ORIGIN_STATE + "\n";
		}
		if (city == null || city.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.ORIGIN_CITY + "\n";
		}
		if (odometer == null || odometer.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.ODOMETER + "\n";
		}
		if (shipper == null || shipper.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.SHIPPER + "\n";
		}

		if (proship == null || proship.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.PROSHIPPER + "\n";
		}

		/*
		 * if (selectedDestState.equals("Select any State")) { complete = false;
		 * errormsg = errormsg + AppConstants.DEST_STATE + "\n"; } if (destcity
		 * == null || destcity.trim().length() == 0) { complete = false;
		 * errormsg = errormsg + AppConstants.DEST_CITY + "\n"; } if (endate ==
		 * null || endate.trim().length() == 0) { complete = false; errormsg =
		 * errormsg + AppConstants.END_DATE + "\n"; }
		 */
		if (!complete) {
			UIUtils.showError(uDroveCreateLoadActivity.this, errormsg);
		}

		else {
			/* Get the status of the Current load */

			jsonObjSend = new JSONObject();
			try {
				jsonObjSend.put("time_stamp", startdate);
				jsonObjSend.put("userid", udroveapp.getUserid());
				jsonObjSend.put("password", udroveapp.getPassword());
				jsonObjSend.put("load_start_date", startdate);
				jsonObjSend.put("load_end_date", endate);
				jsonObjSend.put("origin_city", city);
				jsonObjSend.put("origin_state_id", intselectedOrginState);
				jsonObjSend.put("start_odometer_reading", odometer);
				jsonObjSend.put("shipper", shipper);
				jsonObjSend.put("commodity", comm);
				jsonObjSend.put("pro_ship", proship);
				jsonObjSend.put("dest_city", destcity);
				
				if (intselectedDestState > 0) {
					jsonObjSend.put("dest_state_id", intselectedDestState);
				} else {
					jsonObjSend.put("dest_state_id", "");
				}
				
			
				new UDrovePersonalUseService(this,this,udroveapp).execute();

			/*	
				UDroveService serv = new UDroveService(this, jsonObjSend, this);
				serv.execute(HTTPConstants.CREATE_LOAD_URL);*/
				
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private DateSlider.OnDateSetListener mDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute);
			String nowDateString = HTTPUtil.TodaysDateWithoutZone();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			Date date1;
			Date date2;
			try {
				date1 = df.parse(SelectedDateString);
				date2 = df.parse(nowDateString);
				if (date1.before(date2)) {
					edttxtstartdate.setText(SelectedDateString
							+ AppUtil.getTimeZoneString());
				} else {

					edttxtstartdate.setText(nowDateString
							+ AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/*
			 * edttxtstartdate.setText(String.format(AppConstants.
			 * DATE_SLIDER_DATE_FORMAT+AppUtil.getTimeZoneString(),
			 * selectedDate, selectedDate, selectedDate, selectedDate, minute));
			 */

		}
	};

	private DateSlider.OnDateSetListener mDateEndSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			/*
			 * int minute = selectedDate.get(Calendar.MINUTE) /
			 * DateTimeSlider.MINUTEINTERVAL DateTimeSlider.MINUTEINTERVAL;
			 * 
			 * edttxtenddate.setText(String.format(AppConstants.
			 * DATE_SLIDER_DATE_FORMAT+AppUtil.getTimeZoneString(),
			 * selectedDate, selectedDate, selectedDate, selectedDate, minute));
			 */

			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute);
			String nowDateString = HTTPUtil.TodaysDateWithoutZone();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			Date date1;
			Date date2;
			try {
				date1 = df.parse(SelectedDateString);
				date2 = df.parse(nowDateString);
				if (date1.before(date2)) {
					edttxtenddate.setText(SelectedDateString
							+ AppUtil.getTimeZoneString());
				} else {

					edttxtenddate.setText(nowDateString
							+ AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		final Calendar c = Calendar.getInstance();
		switch (id) {
		case DATE_DIALOG_ID:
			return new DateTimeSlider(this, mDateSetListener, c);

		case DATE_DIALOG_IDS:
			return new DateTimeSlider(this, mDateEndSetListener, c);
		}
		return null;
	}

	private void updateStartDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());

		this.edttxtstartdate.setText(formattedDate3
				+ AppUtil.getTimeZoneString());

	}

	private void updatEndDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());

		this.edttxtenddate
				.setText(formattedDate3 + AppUtil.getTimeZoneString());

	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		String result = HTTPUtil.ParseLoadCurrentJson(posts);
		System.out.println("the Results are " + result);

		String spilt[] = result.split(",");
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			UIUtils.showServiceError(this, spilt[1]);
		} else {
			dbAdapter.open();
			try {
				dbAdapter.insertCreatedLoads(jsonObjSend, spilt[2], spilt[3]);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			dbAdapter.close();
			System.out.println("the id is are " + spilt[2]);
			Toast.makeText(this, spilt[1] + "Tracking Id is " + spilt[3],
					Toast.LENGTH_SHORT).show();
			/*
			 * Intent openActivity = new Intent();
			 * openActivity.setAction(Intent.ACTION_VIEW);
			 * openActivity.setClassName("com.mobile.udrove.u",
			 * "com.mobile.udrove.u.uDroveLoadManagementHomeActivity");
			 * startActivity(openActivity);
			 */
			finish();

		}

	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		System.out.println("change duty status on finish method");
		super.finish();
	}

/* PERSONAL_CONVEYANCE*/
	
	public void onPersonalUseStatus(boolean result, boolean personaluseStatus,
			String message) {
		
		if(result){
			
			dbAdapter.open();
			dbAdapter.updatePersonalConveyance(String.valueOf(personaluseStatus));
			dbAdapter.close();
						
			if(personaluseStatus){
				
				UIUtils.showPersonalUseFlagEnabledPrompt(this,AppConstants.TASK_TERMINATED_AS_PC_ON);
				
				/*   
				 * navigate to homescreen
				 */
				
			//	AppUtil.navigateToHomeScreen(this);
				
				
			}else{
				
				UDroveService serv = new UDroveService(this, jsonObjSend, this);
				serv.execute(HTTPConstants.CREATE_LOAD_URL);
				
				
			}
			
		}else{
			
			UIUtils.showServiceError(this,message);
			
		}
		
		
		
	}


}