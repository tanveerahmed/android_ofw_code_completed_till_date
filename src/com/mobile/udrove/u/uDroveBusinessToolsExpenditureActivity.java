package com.mobile.udrove.u;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.ExpenditureHelper;
import com.mobile.udrove.util.UIUtils;
import com.mobile.udrove.util.uDroveBase64;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the BusinessTool Expenditure report
 * 
 * @dated 16-Feb-2012
 */

public class uDroveBusinessToolsExpenditureActivity extends
		uDroveParentActivity implements UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	HashMap<Integer, ExpenditureHelper> hashmap = new HashMap<Integer, ExpenditureHelper>();

	String selectedItem;
	static final int DATE_DIALOG_ID = 0;
	EditText edttxtdate;
	uDroveApplication udroveapp;
	ImageView image;

	ExpenditureHelper expenditure;

	String imageUri = null;
	String imageUrl;
	int index;

	TableLayout ll = null;
	TableRow tr = null, tr1 = null, tr2 = null;
	TextView txttotalamt;
	String amt;

	int imageIdFromGallery = -1;

	uDroveDataBaseAdapter dbAdapter;

	DecimalFormat decimalFormat = new DecimalFormat("0.00");

	int[] expenseIds;
	String[] expenseNames;

	/*
	 * ArrayList<Integer, Integer> calctotalamt; ArrayList<Integer>
	 * categorytype;
	 */
	int expensestype = 0;
	int count = 0;
	Spinner list;
	Iterator myVeryOwnIterator1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		/* Log.v(">>>>>>>>>>", "on Create"); */
		super.onCreate(savedInstanceState);
		setContentView(R.layout.businesstoolexpenditure);
		udroveapp = ((uDroveApplication) getApplication());
		onclickListener listener = new onclickListener();
		findViewById(R.id.addcatButton).setOnClickListener(listener);
		findViewById(R.id.saveButton).setOnClickListener(listener);
		image = (ImageView) findViewById(R.id.camera);
		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveBusinessToolsExpenditureActivity.this);

		udroveapp.setEditsignatureexpenditure(null);
		dbAdapter = new uDroveDataBaseAdapter(this);

		dbAdapter.open();
		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				System.out
						.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"
								+ cur.getString(0) + cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MetaDataModel expenseModal = dbAdapter.getExpenseTypes();

		dbAdapter.close();

		if (expenseModal != null) {
			expenseIds = expenseModal.getIdList();
			expenseNames = expenseModal.getNameList();

		}

		Spinner spinner = (Spinner) findViewById(R.id.dropdowncat);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, expenseNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		list = (Spinner) findViewById(R.id.dropdowncat);
		list.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				expensestype = expenseIds[arg2];
				selectedItem = expenseNames[arg2];
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}

		});

		edttxtdate = (EditText) findViewById(R.id.txtdatetime);
		// display the current date
		updateDateDisplay();

		edttxtdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_ID);
			}
		});

		// Camera button
		findViewById(R.id.camera).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (imageUri == null) {
					Intent openActivity = new Intent();
					openActivity.setAction(Intent.ACTION_VIEW);
					openActivity.setAction(Intent.ACTION_VIEW);
					openActivity
							.setClassName("com.mobile.udrove.u",
									"com.mobile.udrove.u.EditSignatureandExpenditureCameraActivity");
					openActivity.putExtra("intentcamefrom", "expenditure");
					startActivity(openActivity);
				} else {

					final CharSequence[] items = { "Preview", "Edit" };
					AlertDialog.Builder builder = new AlertDialog.Builder(
							uDroveBusinessToolsExpenditureActivity.this);
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialogInterface,
										int item) {

									Intent openActivity2 = new Intent();
									openActivity2.setAction(Intent.ACTION_VIEW);
									if (item == 0) {
										openActivity2
												.setClassName(
														"com.mobile.udrove.u",
														"com.mobile.udrove.u.PreviewImageActivity");
										openActivity2.putExtra("imgageuri",
												imageUri);
									} else if (item == 1) {
										openActivity2
												.setClassName(
														"com.mobile.udrove.u",
														"com.mobile.udrove.u.EditSignatureandExpenditureCameraActivity");
										openActivity2
												.putExtra("intentcamefrom",
														"expenditure");
									}

									startActivity(openActivity2);
									return;
								}
							});
					builder.create().show();
				}
			}
		});

		ll = (TableLayout) findViewById(R.id.dynamicbuttonslayout);
		TextView invoice = null;
		TextView cateogry = null;
		TextView amounttxt = null;
		ImageView background = null;
		ImageView background1 = null;
		tr = new TableRow(this);
		tr.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

		invoice = new TextView(uDroveBusinessToolsExpenditureActivity.this);
		invoice.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT, (float) 1.0));
		invoice.setGravity(Gravity.CENTER_HORIZONTAL);

		invoice.setTextColor(Color.BLACK);

		invoice.setText("Invoice Total"); // category added
		invoice.setTypeface(null, Typeface.BOLD);

		GradientDrawable whiteborder = (GradientDrawable) getResources()
				.getDrawable(R.drawable.cell_shape);
		invoice.setBackgroundDrawable(whiteborder);
		invoice.setGravity(Gravity.CENTER_HORIZONTAL);
		// invoice.setPadding(5, 0, 0, 0);
		txttotalamt = new TextView(uDroveBusinessToolsExpenditureActivity.this);

		txttotalamt.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT, (float) 1.0));
		txttotalamt.setId(001);

		txttotalamt.setTextColor(Color.BLACK);
		txttotalamt.setText("$" + decimalFormat.format(0.0)); // category added
		whiteborder = (GradientDrawable) getResources().getDrawable(
				R.drawable.cell_shape);
		txttotalamt.setBackgroundDrawable(whiteborder);
		txttotalamt.setGravity(Gravity.CENTER_HORIZONTAL);

		tr1 = new TableRow(this);
		tr1.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

		cateogry = new TextView(uDroveBusinessToolsExpenditureActivity.this);
		cateogry.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT, (float) 1.0));
		cateogry.setTextColor(Color.BLACK);
		cateogry.setText("Category"); // category added
		whiteborder = (GradientDrawable) getResources().getDrawable(
				R.drawable.cell_shape);
		invoice.setGravity(Gravity.CENTER_HORIZONTAL);
		cateogry.setBackgroundDrawable(whiteborder);
		cateogry.setGravity(Gravity.CENTER_HORIZONTAL);

		amounttxt = new TextView(uDroveBusinessToolsExpenditureActivity.this);

		amounttxt.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT, (float) 1.0));
		amounttxt.setId(002);
		amounttxt.setTextColor(Color.BLACK);
		amounttxt.setText("Amount($)"); // category added
		whiteborder = (GradientDrawable) getResources().getDrawable(
				R.drawable.cell_shape);
		amounttxt.setBackgroundDrawable(whiteborder);
		amounttxt.setGravity(Gravity.CENTER_HORIZONTAL);

		background = new ImageView(uDroveBusinessToolsExpenditureActivity.this);
		background.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
		background.setBackgroundResource(R.drawable.gray_slab_img);
		background.setScaleType(ScaleType.CENTER);

		background1 = new ImageView(uDroveBusinessToolsExpenditureActivity.this);
		background1.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
		background1.setBackgroundResource(R.drawable.gray_slab_img);
		background1.setScaleType(ScaleType.CENTER);

		LinearLayout linearlyt2 = new LinearLayout(
				uDroveBusinessToolsExpenditureActivity.this);
		linearlyt2.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT));
		linearlyt2.setOrientation(LinearLayout.HORIZONTAL);

		LinearLayout linearlyt3 = new LinearLayout(
				uDroveBusinessToolsExpenditureActivity.this);
		linearlyt3.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT));
		linearlyt3.setOrientation(LinearLayout.HORIZONTAL);

		LinearLayout linearlyt4 = new LinearLayout(
				uDroveBusinessToolsExpenditureActivity.this);
		linearlyt4.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT));
		linearlyt4.setOrientation(LinearLayout.HORIZONTAL);

		LinearLayout linearlyt5 = new LinearLayout(
				uDroveBusinessToolsExpenditureActivity.this);
		linearlyt5.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.FILL_PARENT,
				android.view.ViewGroup.LayoutParams.FILL_PARENT));
		linearlyt5.setOrientation(LinearLayout.HORIZONTAL);

		linearlyt2.addView(background);
		linearlyt3.addView(background1);

		tr.addView(invoice);
		tr.addView(txttotalamt);
		tr.addView(linearlyt2);

		tr1.addView(cateogry);
		tr1.addView(amounttxt);
		tr1.addView(linearlyt3);

		linearlyt4.addView(tr);
		ll.addView(linearlyt4);
		linearlyt5.addView(tr1);
		ll.addView(linearlyt5);
	}

	protected class HttpServiceListener implements UDroveServiceListener {

		public void onServiceComplete(String posts) {

		}

		public void onServiceComplete(String posts, String reference) {

			/* Log.v(">>>>>>>>>>>", "onservice complete inner"); */

			String result = HTTPUtil.ParseImageJson(posts);

			String split[] = result.split(",");
			if (split[0] != null && split[0].equals("FAIL")) {
				// UIUtils.showServiceError(uDroveBusinessToolsExpenditureActivity.this,
				// split[1]);
			} else {
				imageUrl = split[2];
				ExpenditureHelper exp = hashmap
						.get(Integer.parseInt(reference));
				exp.setImageUrl(imageUrl);
			}
			index++;
			if (index == hashmap.size()) {
				saveExpenditure();
			}{
				uploadImage();

			}
		}
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			// String amt;

			switch (v.getId()) {
			case R.id.addcatButton:

				amt = ((EditText) findViewById(R.id.txtamount)).getText()
						.toString();
				String date = edttxtdate.getText().toString();
				String vendor = ((EditText) findViewById(R.id.txtvendor))
						.getText().toString();
				validateExpense(date, vendor, amt);

				break;
			case R.id.saveButton:

				if (AppUtil
						.isInternetAvailable(uDroveBusinessToolsExpenditureActivity.this)) {

					if (!hashmap.isEmpty()) {

						myVeryOwnIterator1 = hashmap.keySet().iterator();
						index = 0;
						uploadImage();

						if (index == hashmap.size()) {
							saveExpenditure();
						}
					} else {
						UIUtils.showError(
								uDroveBusinessToolsExpenditureActivity.this,
								AppConstants.EXPENSE_CATEGORY);
					}
				} else {
					UIUtils.showError(
							uDroveBusinessToolsExpenditureActivity.this,
							AppConstants.INTERNET_CONNECTION_ERROR);
				}
				break;

			}

		}
	}

	public void uploadImage() {
		
		if (myVeryOwnIterator1.hasNext()) {

			int key = (Integer) myVeryOwnIterator1.next();

			expenditure = hashmap.get(key);

			if (expenditure.getImageUri() != null) {

				dbAdapter.open();
				byte[] ba = dbAdapter.getImageFromGallery(expenditure
						.getImageUri());
				dbAdapter.close();
				try {
					JSONObject json = new JSONObject();
					json.put("userid", udroveapp.getUserid());
					json.put("password", udroveapp.getPassword());
					json.put("category", "EXP");
					json.put("sub_category", expenditure.getExpenseType());
					json.put("file_ext", "jpg");
					json.put("bindata", uDroveBase64.encodeToString(ba,
							uDroveBase64.DEFAULT));
                    ba = null;
					HttpServiceListener listener = new HttpServiceListener();
					UDroveService serv = new UDroveService(
							uDroveBusinessToolsExpenditureActivity.this, json,
							listener);
					serv.execute(HTTPConstants.IMAGE_UPLOAD,
							String.valueOf(key));

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				index = index + 1;
				expenditure.setImageUrl("");
			}
		}
	}

	private void validateExpense(String date, String vendor, String amount) {

		String errormsg = "";
		boolean complete = true;

		if (date == null || date.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DATETIME + "\n";
		}
		if (amount == null || amount.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.AMOUNT + "\n";
		}
		if (list.getSelectedItem().toString()
				.equalsIgnoreCase("Select Category")) {
			complete = false;
			errormsg = errormsg + AppConstants.CATEGORY + "\n";
		}

		dbAdapter.open();
		if (dbAdapter.isExpenseReceiptRequired()) {

			if (imageUri == null) {
				complete = false;
				errormsg = errormsg + AppConstants.PHOTO + "\n";
			}

		}
		dbAdapter.close();

		if (amount != null && amount.length() > 0) {
			if (!AppUtil.isFloatNumberValid(amount)) {
				complete = false;
				errormsg = errormsg + AppConstants.INVALIDAMOUNT + "\n";
			}
		}

		if (!complete) {
			UIUtils.showError(this, errormsg);
		} else {

			expenditure = new ExpenditureHelper();
			expenditure.setDate(date);
			if (vendor != null && vendor.trim().length() > 0) {
				expenditure.setVendor(vendor);
			} else {
				expenditure.setVendor("");
			}

			expenditure.setImageUri(imageUri);
			imageUri = null;
			CheckBox reimbursable = (CheckBox) findViewById(R.id.checkboxreim);
			if (reimbursable.isChecked()) {
				expenditure.setReimbursable(true);
			} else {
				expenditure.setReimbursable(false);
			}

			expenditure.setExpenseType(expensestype);
			System.out.println("Category Item Added" + expensestype);
			expensestype = 0;
			expenditure.setAmount(Float.parseFloat(amt));

			System.out.println("Before Adding The size of the HASHMAP is "
					+ +hashmap.size());

			hashmap.put(count, expenditure);

			LinearLayout linearlyt1 = new LinearLayout(
					uDroveBusinessToolsExpenditureActivity.this);
			linearlyt1.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.FILL_PARENT));
			linearlyt1.setOrientation(LinearLayout.HORIZONTAL);

			LinearLayout linearlyt = new LinearLayout(
					uDroveBusinessToolsExpenditureActivity.this);
			linearlyt.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.FILL_PARENT));
			linearlyt.setOrientation(LinearLayout.HORIZONTAL);
			linearlyt.setGravity(Gravity.CENTER);

			tr2 = new TableRow(uDroveBusinessToolsExpenditureActivity.this);

			tr2.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

			tr2.setId(hashmap.size()); // assiging the id here

			final TextView txtcat;
			final TextView txtamt;
			final ImageView imgdelete;

			txtcat = new TextView(uDroveBusinessToolsExpenditureActivity.this);
			txtamt = new TextView(uDroveBusinessToolsExpenditureActivity.this);

			txtcat.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					(float) 1.0));
			// txtcat.setGravity(Gravity.CENTER_HORIZONTAL);

			txtcat.setTextColor(Color.BLACK);
			txtcat.setText(list.getSelectedItem().toString()); // category added
			GradientDrawable whiteborder = (GradientDrawable) getResources()
					.getDrawable(R.drawable.cell_shapewhite);
			txtcat.setBackgroundDrawable(whiteborder);
			txtcat.setGravity(Gravity.LEFT);

			txtamt.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					(float) 1.0));
			// txtamt.setGravity(Gravity.CENTER_HORIZONTAL);
			txtamt.setTextColor(Color.BLACK);

			txtamt.setText("$ " + decimalFormat.format(Double.parseDouble(amt))); // category
																					// added
			txtamt.setBackgroundDrawable(whiteborder);
			txtamt.setInputType(InputType.TYPE_CLASS_NUMBER);
			txtamt.setGravity(Gravity.CENTER_HORIZONTAL);

			imgdelete = new ImageView(
					uDroveBusinessToolsExpenditureActivity.this);
			imgdelete.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
			imgdelete.setBackgroundResource(R.drawable.udrove_delete);
			imgdelete.setScaleType(ScaleType.CENTER);
			imgdelete.setId(hashmap.size());

			imgdelete.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					LinearLayout img = (LinearLayout) v.getParent();
					TableRow row = (TableRow) img.getParent();
					LinearLayout ll_inner = (LinearLayout) row.getParent();
					ll.removeView(ll_inner);
					hashmap.remove(v.getId() - 1);
					double calc = 0;
					// Calculation logic
					if (hashmap.size() > 0) {

						Iterator myVeryOwnIterator = hashmap.keySet()
								.iterator();

						while (myVeryOwnIterator.hasNext()) {
							Integer key = (Integer) myVeryOwnIterator.next();

							ExpenditureHelper exp = hashmap.get(key);
							calc = exp.getAmount() + calc;
						}

					} else {
						calc = 0.00;
					}
					txttotalamt.setText("$ " + decimalFormat.format(calc));
				}
			});
			linearlyt1.addView(imgdelete);
			tr2.addView(txtcat);
			tr2.addView(txtamt);
			tr2.addView(linearlyt1);

			((EditText) findViewById(R.id.txtamount)).setText("");
			((EditText) findViewById(R.id.txtvendor)).setText("");
			reimbursable.setChecked(false);
			((Spinner) findViewById(R.id.dropdowncat)).setSelection(0);

			count += 1;

			// Calculation logic
			if (hashmap.size() > 0) {
				Iterator myVeryOwnIterator = hashmap.keySet().iterator();
				double calc = 0.00;
				ExpenditureHelper exp;
				while (myVeryOwnIterator.hasNext()) {
					exp = hashmap.get(myVeryOwnIterator.next());
					exp.getAmount();
					calc = exp.getAmount() + calc;
				}
				txttotalamt.setText("$ " + decimalFormat.format(calc));
			}

			linearlyt.addView(tr2);
			ll.addView(linearlyt);

			image.setImageResource(R.drawable.camera_hover);
		}

	}

	private void saveExpenditure() {

		String remarks = ((EditText) findViewById(R.id.txtdatenotes)).getText()
				.toString();

		JSONObject jsonObjSend = new JSONObject();
		try {
			jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
			jsonObjSend.put("userid", udroveapp.getUserid());
			jsonObjSend.put("password", udroveapp.getPassword());
			jsonObjSend.put("remarks", remarks);

			JSONObject expenses = new JSONObject();
			JSONArray jArray = new JSONArray();

			Iterator myVeryOwnIterator = hashmap.keySet().iterator();

			while (myVeryOwnIterator.hasNext()) {

				expenditure = hashmap.get(myVeryOwnIterator.next());

				expenses = new JSONObject();

				expenses.put("expense_date", expenditure.getDate());
				expenses.put("vendor", expenditure.getVendor());
				expenses.put("reimbursable", expenditure.isReimbursable());
				expenses.put("expense_type_id", expenditure.getExpenseType());
				expenses.put("amount", expenditure.getAmount());
				expenses.put("expense_image_name", expenditure.getImageUrl());
				jArray.put(expenses);
			}
			jsonObjSend.put("expenses", jArray);

			UDroveService serv = new UDroveService(this, jsonObjSend, this);
			serv.execute(HTTPConstants.EXPENSES_INFO_URL);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private DateSlider.OnDateSetListener mDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute);
			String nowDateString = HTTPUtil.TodaysDateWithoutZone();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			Date date1;
			Date date2;
			try {
				date1 = df.parse(SelectedDateString);
				date2 = df.parse(nowDateString);
				if (date1.before(date2)) {
					edttxtdate.setText(SelectedDateString
							+ AppUtil.getTimeZoneString());
				} else {

					edttxtdate.setText(nowDateString
							+ AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// edttxtdate.setText(String.format(AppConstants.DATE_SLIDER_DATE_FORMAT,
			// selectedDate, selectedDate, selectedDate, selectedDate, minute));

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		final Calendar c = Calendar.getInstance();
		switch (id) {
		case DATE_DIALOG_ID:
			return new DateTimeSlider(this, mDateSetListener, c);
		}
		return null;
	}

	private void updateDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());
		edttxtdate.setText(formattedDate3 + AppUtil.getTimeZoneString());
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (udroveapp.getEditsignatureexpenditure() != null) {
			try {
				imageUri = udroveapp.getEditsignatureexpenditure();
				AppUtil.setImagetoView1(imageUri, image, dbAdapter);
				udroveapp.setEditsignatureexpenditure(null);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public void onServiceComplete(String posts) {

		String result = HTTPUtil.ParseJson(posts);

		String spilt[] = result.split(",");
		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {

				Toast.makeText(uDroveBusinessToolsExpenditureActivity.this,
						spilt[1], Toast.LENGTH_SHORT).show();
				finish();
			}

		} else {
			UIUtils.showSubscriptionChangedMessage(this);
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		
		dbAdapter.open();
		dbAdapter.deleteGallery();
		dbAdapter.close();
		super.finish();
	}

}