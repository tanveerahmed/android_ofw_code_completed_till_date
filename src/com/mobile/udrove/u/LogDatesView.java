package com.mobile.udrove.u;

import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;


public class LogDatesView extends uDroveParentActivity implements OnClickListener{

	protected boolean _active = true;
	protected static boolean register = true;
    String[] logDates;
    
	String[] docIds;
	
	int index;

    
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.paperlogdates);
		if (getIntent() != null) {

			if (getIntent().getExtras() != null) {
				logDates=getIntent().getExtras().getStringArray("LogDates");
				docIds=getIntent().getExtras().getStringArray("LogIds");
				index=getIntent().getExtras().getInt("index");
			}
		}
		
		TextView titleView =(TextView) findViewById(R.id.datesViewTitle);
		
		switch (index) {
		case 1:
			//paperlog view
			titleView.setText("View");
			break;
		
		case 2:
		   //email view	
			titleView.setText("Email");
			break;
			
		case 3:
			//fax view
			titleView.setText("Fax");
			break;

		}
		
		
		
		int[] buttonIds={R.id.btn_day1,R.id.btn_day2,R.id.btn_day3,R.id.btn_day4,R.id.btn_day5,R.id.btn_day6,R.id.btn_day7,R.id.btn_day8};
        
		Button tempBtn;
		for(int i=0;i<logDates.length;i++){
			
			tempBtn=(Button)findViewById(buttonIds[i]);
			tempBtn.setText(logDates[i]);
			tempBtn.setVisibility(View.VISIBLE);
			tempBtn.setOnClickListener(this);
			tempBtn.setTag(i);
	  	}
	    tempBtn=(Button)findViewById(R.id.paperlogbackbtn);
	    tempBtn.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		String dateStr=((Button)findViewById(v.getId())).getText().toString();
		int index1=Arrays.asList(logDates).indexOf(dateStr);
		System.out.println("Selected index"+index1);
		Intent openActivity2 = new Intent();
		openActivity2.setAction(Intent.ACTION_VIEW);
		switch (index) {
		case 1:
			//paperlog view
			
			openActivity2.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.PaperLogActivity");
			openActivity2.putExtra("dateIndex",index1);
			openActivity2.putExtra("LogDates",logDates);
			
			break;
		
		case 2:
		   //email view	
			
			openActivity2.putExtra("DocType", "LOGS");
			openActivity2.putExtra("DocIds",docIds[index1]);
			openActivity2.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.EmailActivity");

			break;
			
		case 3:
			//fax view
			
			openActivity2.putExtra("DocType", "LOGS");
			openActivity2.putExtra("DocIds",docIds[index1]);
			openActivity2.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.FaxActivity");
			break;

		}
		
		startActivity(openActivity2);
		finish();
		
	}

	
}