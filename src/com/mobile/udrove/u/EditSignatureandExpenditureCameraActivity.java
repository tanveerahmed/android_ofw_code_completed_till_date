package com.mobile.udrove.u;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;
import com.mobile.udrove.util.uDroveBase64;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers to on,off,sleeper,and driving
 * 
 * @dated 16-Feb-2012
 */

public class EditSignatureandExpenditureCameraActivity extends Activity
		implements UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	private static final int CAMERA_PIC_REQUEST = 1;
	TextView lblchangestatusofftodr;
	String imageUri = null;
	ImageView image;
	uDroveApplication udroveapp;
	String calledActivity;
	String driverSignUrl;
	Button saveBtn;
	uDroveDataBaseAdapter dbAdapter;

	int imageIdFromGallery = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editsignature);

		udroveapp = ((uDroveApplication) getApplication());

		onclickListener listener = new onclickListener();
		findViewById(R.id.saveButton).setOnClickListener(listener);
		findViewById(R.id.back_header_button).setOnClickListener(listener);
		findViewById(R.id.camera_header_button).setOnClickListener(listener);
		udroveapp.setEditsignatureexpenditure(imageUri);
		dbAdapter = new uDroveDataBaseAdapter(this);

		lblchangestatusofftodr = (TextView) findViewById(R.id.txtdynamicheader);
		image = (ImageView) findViewById(R.id.imageView3);
		saveBtn = (Button) findViewById(R.id.saveButton);

		String changestatustitle = "Edit Signature";
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {

				calledActivity = getIntent().getExtras().getString(
						"intentcamefrom");
				changestatustitle = calledActivity;
				System.out.println("calledactivity:" + calledActivity);

				if (changestatustitle.equals("signature")) { // came from
																// signature
					lblchangestatusofftodr.setText("Edit Signature");

					driverSignUrl = uDroveApplication.getSettingsImageUrl();
					if (driverSignUrl.length() == 0
							|| driverSignUrl.equalsIgnoreCase("null")) {

						image.setImageResource(R.drawable.udrove_logo);
						saveBtn.setEnabled(false);
					} else {

						if (AppUtil.isInternetAvailable(this)) {

							new DownloadImageTask().execute(driverSignUrl);
						} else {

							Toast.makeText(this,
									AppConstants.INTERNET_CONNECTION_ERROR,
									Toast.LENGTH_SHORT).show();
						}

					}

				} else if (changestatustitle.equals("expenditure")) {
					lblchangestatusofftodr
							.setText("Business Tools - Expenditures");
				} else if (changestatustitle.equals("fuel")) {
					lblchangestatusofftodr.setText(getIntent().getExtras()
							.getString("title"));
				} else if (changestatustitle.equals("dvirreceipt")) {
					lblchangestatusofftodr.setText(getIntent().getExtras()
							.getString("title"));
				}
			}
		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			String errormsg = "Please take the picture before saving";

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {

			case R.id.saveButton:
				if (imageUri == null) {
					UIUtils.showError(
							EditSignatureandExpenditureCameraActivity.this,
							errormsg);
				} else {
					if (imageUri != null
							&& calledActivity.equalsIgnoreCase("signature")) {

						if (AppUtil
								.isInternetAvailable(EditSignatureandExpenditureCameraActivity.this)) {
							uploadImage(imageUri, "SIG");
						} else {

							UIUtils.showError(
									EditSignatureandExpenditureCameraActivity.this,
									AppConstants.INTERNET_CONNECTION_ERROR);

						}
					} else {

						finish();
					}
				}
				break;

			case R.id.back_header_button:
				System.out.println("I clicked on backbutton");
				udroveapp.setEditsignatureexpenditure(null);
				uDroveApplication.setFromBackButton(true);
				finish();
				break;

			case R.id.camera_header_button:
				StartCamera();

				break;
			}

		}

	}

	int lastImageId = -1;

	private void StartCamera() {
		/*
		 * // start camera
		 * 
		 * final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		 * 
		 * 
		 * if (uDroveApplication.galleryImageCounter <= 0) {
		 * 
		 * lastImageId = getLastImageID();
		 * 
		 * }
		 * 
		 * ContentValues values = new ContentValues();
		 * values.put(MediaColumns.TITLE, "New Picture");
		 * values.put(ImageColumns.DESCRIPTION, "From your Camera"); imageUri =
		 * getContentResolver().insert(
		 * MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		 * intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		 * startActivityForResult(intent, CAMERA_PIC_REQUEST);
		 */

		System.gc();
		Intent intent = new Intent(this, CameraControlNew.class);
		startActivityForResult(intent, CAMERA_PIC_REQUEST);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			switch (requestCode) {

			case CAMERA_PIC_REQUEST:

				if (!saveBtn.isEnabled()) {
					saveBtn.setEnabled(true);
				}
				Bundle bdl = data.getExtras();
				imageUri = bdl.getString("filename");
				AppUtil.setImagetoView1(imageUri, image, dbAdapter);

				// AppUtil.setImagetoView(imageUri, image,
				// getContentResolver());
				udroveapp.setEditsignatureexpenditure(imageUri);
				saveBtn.setEnabled(true);
				break;
			}
		} else if (resultCode == RESULT_CANCELED) {
			imageUri = null;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {

			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void uploadImage(String filename, String imageName) {
		try {
			dbAdapter.open();
			byte[] ba = dbAdapter.getImageFromGallery(filename);
			dbAdapter.close();

			System.gc();

			JSONObject json = new JSONObject();
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());
			json.put("category", imageName);
			json.put("sub_category", 0);
			json.put("file_ext", "jpg");
			json.put("bindata",
					uDroveBase64.encodeToString(ba, uDroveBase64.DEFAULT));
			ba = null;
			HttpServiceListener listener = new HttpServiceListener();
			UDroveService serv = new UDroveService(
					EditSignatureandExpenditureCameraActivity.this, json,
					listener);
			serv.execute(HTTPConstants.IMAGE_UPLOAD, imageName);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {

			System.out.println("Exception caught" + e.getMessage());
		}
	}

	public void updateDriverSign() {
		try {

			JSONObject json = new JSONObject();
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());
			json.put("signature_url", driverSignUrl);
			UDroveService serv = new UDroveService(this, json, this);
			serv.execute(HTTPConstants.SIGN_URL);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected class HttpServiceListener implements UDroveServiceListener {

		public void onServiceComplete(String posts) {

		}

		public void onServiceComplete(String posts, String reference) {

			String result = HTTPUtil.ParseImageJson(posts);
			String split[] = result.split(",");
			if (split[0] != null && split[0].equals("FAIL")) {

				UIUtils.showServiceError(
						EditSignatureandExpenditureCameraActivity.this,
						split[1]);
			} else {

				driverSignUrl = split[2];
				updateDriverSign();
			}
		}
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		String result = HTTPUtil.ParseLoginJson(posts);
		String spilt[] = result.split(",");
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			UIUtils.showServiceError(this, spilt[1]);
		} else {
			JSONObject jObject;
			try {
				jObject = new JSONObject(posts);
				JSONObject bodyObject = jObject.getJSONObject("body");
				driverSignUrl = bodyObject.getString("signature_url");
				dbAdapter.open();
				dbAdapter.updateDriverSignImage(driverSignUrl);
				dbAdapter.deleteGallery();
				dbAdapter.close();
				uDroveApplication.setSettingsImageUrl(driverSignUrl);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Toast.makeText(EditSignatureandExpenditureCameraActivity.this,
					spilt[1], Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(
					EditSignatureandExpenditureCameraActivity.this);
			dialog.setMessage("Loading Image...");
			dialog.show();
		}

		@Override
		protected Bitmap doInBackground(String... urls) {

			Bitmap bmp = null;

			InputStream inputStream = null;
			byte[] tempStorage = new byte[1024];// try to read 1Kb at time
			int bLength;
			try {

				File f = new File(
						AppUtil.getCacheDirectory(image.getContext()),
						"temp.jpg");
				FileOutputStream outputByteArrayStream = new FileOutputStream(f);
				inputStream = new URL(urls[0]).openStream();
				while ((bLength = inputStream.read(tempStorage)) != -1) {
					outputByteArrayStream.write(tempStorage, 0, bLength);
				}
				outputByteArrayStream.flush();
				outputByteArrayStream.close();
				inputStream.close();

				BitmapFactory.Options bitopt = new BitmapFactory.Options();
				bitopt.outHeight = image.getHeight();
				bitopt.outWidth = image.getWidth();
				bitopt.inSampleSize = 8;

				bmp = BitmapFactory.decodeFile(f.getPath(), bitopt);
				ByteArrayOutputStream outStreamthumb = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.JPEG, 75, outStreamthumb);
				if (bmp != null) {
					bmp = Bitmap.createScaledBitmap(bmp, image.getWidth(),
							image.getHeight(), true);
				}

			} catch (Exception e) {
				e.printStackTrace();
				if (inputStream != null)
					try {
						inputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}

			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			if (result != null) {
				image.setImageBitmap(result);
				saveBtn.setEnabled(false);
			}
			dialog.cancel();
		}
	}

}