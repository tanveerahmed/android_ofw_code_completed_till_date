package com.mobile.udrove.u;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationProvider;
import android.os.Bundle;

public class LocationHandler implements LocationListener{
        // LocationListener Interface   
        public Location location; 
        public double lastKnownLatitude = 0;
        public double lastKnownLongitude = 0;   
    	
    	public void onLocationChanged(Location mlocation) {
    		
    		if (mlocation != null) {			
    			lastKnownLatitude = mlocation.getLatitude();
    			lastKnownLongitude = mlocation.getLongitude();
    			location = mlocation;
    		}
    	}

    	
    	public void onProviderDisabled(String provider) {
    		// Reset latitude and longitude in case provider is disabled
    		lastKnownLatitude = 0;
    		lastKnownLongitude = 0;
    	}

    	
    	public void onProviderEnabled(String provider) {
    	}
    	
    	
    	public void onStatusChanged(String provider, int status, Bundle extras) {
    		if( status == LocationProvider.OUT_OF_SERVICE || 
    				status == LocationProvider.TEMPORARILY_UNAVAILABLE ) {
    			lastKnownLatitude = 0;
    			lastKnownLongitude = 0;
    		}		
    	}
}
