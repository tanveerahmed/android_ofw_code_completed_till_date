package com.mobile.udrove.u;

import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

public class EmailActivity extends uDroveParentActivity implements UDroveServiceListener {

	protected boolean _active = true;
	protected static boolean register = true;
	uDroveApplication udroveapp;
   String docType;
   String docIds;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (getIntent() != null) {
			
			if (getIntent().getExtras() != null) {
				
				docType=getIntent().getExtras().getString("DocType");
				docIds=getIntent().getExtras().getString("DocIds");
				
			}
		}

		if(docType.equalsIgnoreCase("DVIR")){
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}else{
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
		
		setContentView(R.layout.emailbox);
		udroveapp = ((uDroveApplication) getApplication());

		findViewById(R.id.emailcanclebtn).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		findViewById(R.id.emailsendbtn).setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String errormsg = "";
				boolean complete = true;
				String email = ((EditText) findViewById(R.id.edtemail)).getText()
						.toString();
				if (email == null || email.trim().length() == 0) {
					complete = false;
					errormsg = errormsg + AppConstants.EMAIL;
				}
				if(email!=null && email.length()>0)
				{
					if(!AppUtil.isEmailValid(email))
					{
						UIUtils.showError(EmailActivity.this, AppConstants.INVALIDEMAIL);
						return;
					}
				}

				if (!complete) {
					UIUtils.showError(EmailActivity.this, errormsg);
				}
				else {
				
					JSONObject jsonObjSend = new JSONObject();
					try {
											
						jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
						jsonObjSend.put("userid", udroveapp.getUserid());
						jsonObjSend.put("password", udroveapp.getPassword());
						jsonObjSend.put("doc_type",docType);
						jsonObjSend.put("doc_id",docIds);
						jsonObjSend.put("comm_type","EMAIL");
						jsonObjSend.put("send_to",email);

						UDroveService serv = new UDroveService(EmailActivity.this, jsonObjSend, EmailActivity.this);
						serv.execute(HTTPConstants.SEND_DOCUMENT);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					
		//			finish();
				}
			}
		});
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub
		
		System.out.println("the onService Complete image" + posts);
		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if(spilt[2]!=null && spilt[2].equalsIgnoreCase("false")){
			
				if (spilt[0] != null && spilt[0].equals("FAIL")) {
			UIUtils.showServiceError(this, spilt[1]);
		//	finish();

		} else {
			Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
			finish();

		}
		}else{
			
	        UIUtils.showSubscriptionChangedMessage(this);			
		}


		
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}
}