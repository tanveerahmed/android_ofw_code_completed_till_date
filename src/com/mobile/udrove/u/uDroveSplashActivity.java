package com.mobile.udrove.u;

import org.json.JSONException;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;

public class uDroveSplashActivity extends Activity {

	protected boolean _active = true;
	protected static boolean register = true;
    uDroveDataBaseAdapter dbAdapter;
	uDroveApplication udroveapp;


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		uDroveApplication.fromLoginOrSplash=true;

		if (AppUtil.isInternetAvailable(this)) {
          
			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				public void run() {
					
					udroveapp = ((uDroveApplication) getApplication());
					udroveapp.ResetAllApplicationFields();
					dbAdapter=new uDroveDataBaseAdapter(uDroveSplashActivity.this);
					dbAdapter.open();
					
					if(dbAdapter.didAlreadyLoggedIn()){
						

						Cursor cur;
						try {
							cur = dbAdapter.getDriverDetails();
							if(cur!=null&&cur.getCount()>0){
						     
								udroveapp.setUserid(cur.getString(0));
								udroveapp.setPassword(cur.getString(1));
								uDroveApplication.setSettingsImageUrl(cur.getString(2));
								udroveapp.setSignUrl(cur.getString(2));
								udroveapp.setServiceIds(cur.getString(3));
							}
						    cur.close(); 
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						dbAdapter.close();
					  
						Intent myIntent = new Intent();
						myIntent.setClass(uDroveSplashActivity.this,
								uDroveHomeActivity.class);
						startActivity(myIntent);
						finish();
						
					}else{
                      
						dbAdapter.close();

					Intent myIntent = new Intent();
					myIntent.setClass(uDroveSplashActivity.this,
							uDroveSignInActivity.class);
					startActivity(myIntent);
					finish();
					}
				}
			}, 2000);

		} else {
			UIUtils.showErrorExit(this, AppConstants.INTERNET_CONNECTION_ERROR);
		}

	}
}