package com.mobile.udrove.u;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @param <PerformBackgroundTask>
 * @description This is a Home screen of the uDrove application which controls
 *              the dutystatus,hos,driver,trip management and business tools
 * @dated 16-Feb-2012
 * 
 */

public class uDroveHomeActivity extends uDroveParentActivity implements
		UDroveServiceListener {

	/* uDrove Home class variables */
	TextView _edittxtdutystatus;
	/**
	 * {@inheritDoc}
	 */
	uDroveApplication udroveapp;
	int gcheckservice = 0;
	uDroveDataBaseAdapter dbAdapter;

	Button btnDutyStatus, btnHos, btnDvir, btnTripMang, btnBusinessTools;
	JSONObject json;

	Button tbtn_personaluse;
	LinearLayout ll_personaluse;

	TimerTask doAsynchronousTask;
	final Handler handler = new Handler();
	Timer timer;
	TextView dutyHoursTv;

	boolean personalUse_status = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.udrovehome);
		onclickListener listener = new onclickListener();

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveHomeActivity.this);

		uDroveApplication.fromLoginOrSplash = true;

		findViewById(R.id.home_header_button).setBackgroundResource(
				R.drawable.home_normal);
		findViewById(R.id.home_header_button).setFocusable(false);
		findViewById(R.id.back_header_button).setVisibility(View.GONE);

		btnDutyStatus = (Button) findViewById(R.id.dutystatusButton);
		btnDutyStatus.setOnClickListener(listener);

		btnHos = (Button) findViewById(R.id.hosButton);
		btnHos.setOnClickListener(listener);

		btnDvir = (Button) findViewById(R.id.dvirButton);
		btnDvir.setOnClickListener(listener);

		btnTripMang = (Button) findViewById(R.id.tripmangementButton);
		btnTripMang.setOnClickListener(listener);

		btnBusinessTools = (Button) findViewById(R.id.businesstoolButton);
		btnBusinessTools.setOnClickListener(listener);

		ll_personaluse = (LinearLayout) findViewById(R.id.ll_personaluse);
		tbtn_personaluse = (Button) findViewById(R.id.tbtn_personaluse);
		tbtn_personaluse.setOnClickListener(listener);

		udroveapp = ((uDroveApplication) getApplication());
		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		String temp = udroveapp.getServiceIds();
		if (!temp.equalsIgnoreCase("null") && temp.length() > 0) {
			String[] serviceids = temp.split(",");
			for (int i = 0; i < serviceids.length; i++) {

				switch (Integer.parseInt(serviceids[i])) {
				case 1:
					btnHos.setEnabled(true);
					break;
				case 2:
					btnDvir.setEnabled(true);
					break;
				case 4:
				case 5:
				case 6:
					btnBusinessTools.setEnabled(true);
					break;
				}
			}

		}
		_edittxtdutystatus = (TextView) findViewById(R.id.txt_dutystatus);

		gcheckservice = 2;
		dbAdapter.open();
		dbAdapter.deleteAllRecords("LOGS");
		dbAdapter.deleteAllRecords("LOGS_DUTY_STATUSES");
		dbAdapter.deleteAllRecords("HOS_DETAILS");
		dbAdapter.deleteAllRecords("DRIVER_lOADS");
		dbAdapter.deleteAllRecords("LOG_INFO");
		// dbAdapter.deleteAllRecords("PERSONAL_CONVEYANCE");

		dbAdapter.close();

		callLogsService();

	}

	public void callLocation() {

		startService(new Intent(getApplicationContext(),
				LocationHelperService.class));

	}

	public void updateDriverStatus() {

		String str_suffix="OFF";
		/* Driver Status logic comes here */
		dbAdapter.open();

		int driverStatus = dbAdapter.getCurrentDutyStatus();

		boolean pc_flag = dbAdapter.getPersonalConveyanceStatus();

//		str_suffix = dbAdapter.getCurrentDutyStatusCode();
		str_suffix = dbAdapter.getDutyStatusCodeFromID(driverStatus);
		
		dbAdapter.close();
		
		
		switch (driverStatus) {
		case 3: // off duty
			_edittxtdutystatus.setText(str_suffix);
			_edittxtdutystatus.setTextColor(Color.parseColor("#FF6699"));
			break;
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
			_edittxtdutystatus.setText(str_suffix);
			_edittxtdutystatus.setTextColor(Color.parseColor("#99CCFF"));
			break;

		case 2:
			_edittxtdutystatus.setText(str_suffix);
			_edittxtdutystatus.setTextColor(Color.parseColor("#FF6600"));

			break;

		case 1:
			_edittxtdutystatus.setText(str_suffix);
			_edittxtdutystatus.setTextColor(Color.parseColor("#99CC00"));
			break;
		case 11:						// Oil-Field Waiting
			_edittxtdutystatus.setText(str_suffix); 
			_edittxtdutystatus.setTextColor(Color.parseColor("#FF6699"));
			break;
		default:
			_edittxtdutystatus.setText(str_suffix);
			_edittxtdutystatus.setTextColor(Color.parseColor("#FF6699"));
			break;

		}

		dutyHoursTv = (TextView) findViewById(R.id.txtdutyhours);
		try {
			String avhrs = AppUtil.getUpdatedAvailableDriveTime(this,
					dutyHoursTv);
			dutyHoursTv.setText(avhrs);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		updatePersonalUse(pc_flag);

	}

	public void callLogsService() {
		// TODO Auto-generated method stub

		json = new JSONObject();
		try {

			json.put("time_stamp", HTTPUtil.TodaysDate());
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());
			UDroveService serv = new UDroveService(this, json, this);
			switch (gcheckservice) {
			case 1:
				System.out.println("get logs");
				serv.execute(HTTPConstants.GET_LAST_SEVEN_LOGS);
				break;
			case 2:
				System.out.println("get loads");
				serv.execute(HTTPConstants.GET_LOAD_URL + udroveapp.getUserid());
				break;
			case 3:
				System.out.println("get current day logs");
				serv.execute(HTTPConstants.GET_CURRENT_DAY_LOG);
				break;

			case 4:
				dbAdapter.open();
				boolean res = dbAdapter.didAlreadyMetaDataLoaded();
				dbAdapter.close();
				if (res) {
					scheduleTimerTask();
				} else {
					System.out.println("get meta data service");
					serv.execute(HTTPConstants.GET_META_DATA);
				}
				break;

			default:
				break;
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			switch (v.getId()) {

			case R.id.dutystatusButton:

				checkForFirstDutyStatusChange();

				break;
			case R.id.hosButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveHosHomeActivity");
				startActivity(openActivity);
				break;
			case R.id.dvirButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveDvirHomeActivity");
				startActivity(openActivity);
				break;
			case R.id.tripmangementButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveLoadManagementHomeActivity");
				startActivity(openActivity);
				break;
			case R.id.businesstoolButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveBusinessToolsHomeActivity");
				startActivity(openActivity);
				break;

			case R.id.tbtn_personaluse:

				alerPersonalUseChange();

			}

		}

	}

	public void checkForFirstDutyStatusChange() {

		dbAdapter.open();
		String logDate = dbAdapter.getpromptedLogDate();
		String nowDate = HTTPUtil.TodayDateOnly();

		if (logDate == null) {
			System.out.println("Logdate is null:::" + logDate);
			logDate = "nodate";
		}
		if (logDate.equalsIgnoreCase(nowDate)) {

			System.out.println("Logdate  equals  nowdate " + logDate);

			dbAdapter.close();
			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.uDroveChangestatusHomeActivity");

			startActivity(openActivity);

		} else {

			System.out.println("Logdate  not equals  nowdate " + logDate);

			dbAdapter.upDateLogDateForPromt();
			dbAdapter.close();

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Please update the LogInfo.")
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									Intent openActivity = new Intent();
									openActivity.setAction(Intent.ACTION_VIEW);
									openActivity
											.setClassName(
													"com.mobile.udrove.u",
													"com.mobile.udrove.u.uDroveChangestatusLogInfoActivity");
									startActivity(openActivity);
								}
							}).create().show();
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {

			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		if (gcheckservice == 1)// for logs
		{
			System.out.println("service one");

			String result = HTTPUtil.ParseJson(posts);
			String spilt[] = result.split(",");
			if (spilt[0] != null && spilt[0].equals("FAIL")) {

			} else {

				populateLogsToDB(posts);

			}

			gcheckservice = 2;
			callLogsService();

		} else if (gcheckservice == 2)// for load management
		{

			// call the service here

			System.out.println("service two");

			String result = HTTPUtil.ParseJson(posts);
			String spilt[] = result.split(",");
			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				System.out.println("loads got failed");

			} else {

				System.out.println("loads got successfully");
				populateLoadsToDB(posts);

			}

			gcheckservice = 3;
			callLogsService();
		} else if (gcheckservice == 3) {
			// handle
			System.out.println("service three");
			String result = HTTPUtil.ParseJson(posts);
			String spilt[] = result.split(",");
			if (spilt[0] != null && spilt[0].equals("FAIL")) {

				System.out.println("Current day logs got failed");

			} else {

				System.out.println("Current day logs  got successfully");
				populateCurrentDayLogToDB(posts);

			}

			// updateDriverStatus();
			gcheckservice = 4;
			callLogsService();

		} else if (gcheckservice == 4) {
			// handle
			System.out.println("service four");
			String result = HTTPUtil.ParseJson(posts);
			String spilt[] = result.split(",");
			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				System.out.println("meta dats failed");
			} else {
				System.out.println("meta data  got successfully");
				populateMetaDataToDB(posts);
			}

			scheduleTimerTask();
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	public void populateLogsToDB(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);
			dbAdapter.open();
			JSONArray logsArray = jObject.getJSONArray("body");
			if (logsArray.length() > 0) { // Data at server side

				for (int i = 0; i < logsArray.length(); i++) {

					dbAdapter.insertLogs((logsArray.getJSONObject(i))
							.getJSONObject("log_info"));

				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dbAdapter.close();
		}
		dbAdapter.close();
	}

	public void populateCurrentDayLogToDB(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);
			dbAdapter.open();

			JSONObject bodyObject = jObject.getJSONObject("body");
			JSONArray dutyStatusesArray = bodyObject
					.getJSONArray("duty_statuses");
			if (dutyStatusesArray.length() > 0) { // Data at server side

				SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy");
				String todaysDate = "";
				try {
					todaysDate = df3.format(df3.parse(dutyStatusesArray
							.getJSONObject(0).getString("duty_date_time")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dbAdapter.deleteCurrentDayDutyStatuses(todaysDate);
				for (int i = 0; i < dutyStatusesArray.length(); i++) {
					dbAdapter.insertDutyStatuses(
							dutyStatusesArray.getJSONObject(i), todaysDate);
				}
			}

			JSONObject hosSummary = jObject.getJSONObject("summary");
			dbAdapter.deleteAllRecords("HOS_DETAILS");
			dbAdapter.insertHosInfo(hosSummary, HTTPUtil.TodaysDate1());

			JSONObject logInfo = jObject.getJSONObject("log_info");
			dbAdapter.deleteCurrentDayLog(logInfo.getString("log_date"));
			dbAdapter.insertLogsForCurrentDay(
					logInfo.getString("paper_log_id"),
					logInfo.getString("log_date"));
			dbAdapter.deleteAllRecords("LOG_INFO");
			dbAdapter.insertLogInfo(logInfo);

			/* check for personal use */

			JSONObject headerObject = jObject.getJSONObject("header");

			String pc_flag = headerObject.getString("personal_conveyance_flag");

			dbAdapter.updatePersonalConveyance(pc_flag);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dbAdapter.close();

		}
		dbAdapter.close();

	}

	public void populateLoadsToDB(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);
			dbAdapter.open();
			dbAdapter.deleteAllRecords("DRIVER_lOADS");
			JSONObject bodyObject = jObject.getJSONObject("body");
			JSONArray loadarray = bodyObject.getJSONArray("load_details");
			if (loadarray.length() > 0) {
				for (int i = 0; i < loadarray.length(); i++) {
					dbAdapter.insertLoads(loadarray.getJSONObject(i));
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dbAdapter.close();

		}
		dbAdapter.close();
	}

	public void populateMetaDataToDB(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);
			dbAdapter.open();
			JSONObject bodyObject = jObject.getJSONObject("body");
			dbAdapter.clearAllMetaDataTabelsRecords();
			dbAdapter.insertMetaData(bodyObject);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dbAdapter.close();
		}
		dbAdapter.close();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		System.out.println("On Resume Method starting timer at "
				+ HTTPUtil.TodaysDate());
		if (!uDroveApplication.fromLoginOrSplash) {
			scheduleTimerTask();
		} else {

			uDroveApplication.fromLoginOrSplash = false;
		}
	}

	private void scheduleTimerTask() {

		doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				handler.post(new Runnable() {
					public void run() {
						try {
							new GetCurrentDayLogTask().execute();
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
					}
				});
			}
		};
		timer = new Timer();
		timer.schedule(doAsynchronousTask, 0, AppConstants.TIMER_INTERVAL);// execute
																			// in
																			// every
																			// 1min
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		System.out.println("On Pause Method stopping timer at "
				+ HTTPUtil.TodaysDate());
		timer.cancel();
		timer = null;
		super.onPause();
	}

	private class GetCurrentDayLogTask extends AsyncTask<String, Void, String> {

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.v(">>>>>>>>>>>>>>>>", "onPreExecute");
			dialog = new ProgressDialog(uDroveHomeActivity.this);
			dialog.setMessage(HTTPConstants.CONNECTING);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			json = new JSONObject();
			HttpPost httput = new HttpPost(HTTPConstants.GET_CURRENT_DAY_LOG);

			httput.setHeader("Accept", "application/json");
			httput.setHeader("Content-type", "application/json");

			StringEntity se = null;
			String str = "";
			try {

				json.put("time_stamp", HTTPUtil.TodaysDate());
				json.put("userid", udroveapp.getUserid());
				json.put("password", udroveapp.getPassword());
				se = new StringEntity(json.toString());
				System.out.println("The SE is  request " + se.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httput.setEntity(se);
				HttpResponse response = httpclient.execute(httput);
				byte[] result = EntityUtils.toByteArray(response.getEntity());
				str = new String(result, "UTF-8");
				Log.v(">>>>>>>", "str:" + str);

				return str;

			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {

			}
			return str;

		}

		@Override
		protected void onPostExecute(String posts) {

			dialog.dismiss();

			if (posts != null) {

				// dutyHoursTv.setText(HTTPUtil.TodaysDate());

				String result = HTTPUtil.ParseJson(posts);
				String spilt[] = result.split(",");
				if (spilt[0] != null && spilt[0].equals("FAIL")) {

					System.out.println("Current day logs got failed");

				} else {
					System.out.println("Current day logs  got successfully");
					populateCurrentDayLogToDB(posts);

				}
				updateDriverStatus();

			}
		}
	}

	public void alerPersonalUseChange() {

		if (tbtn_personaluse.isSelected()) {

			// personaluse is enabled so show prompt for disable

			AlertDialog.Builder builder = new AlertDialog.Builder(
					uDroveHomeActivity.this);
			builder.setMessage(
					"Are you sure you want to turn OFF PERSONAL USE option")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {

									new UpdatePersonalUseTask(false).execute();
									// updatePersonalUse(false);

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {

								}
							}).setTitle("uDrove Mobile");

			AlertDialog alert = builder.create();
			alert.show();

		} else {

			// personaluse is disabled so show prompt for enabling

			AlertDialog.Builder builder = new AlertDialog.Builder(
					uDroveHomeActivity.this);
			builder.setMessage(
					"Are you sure you want to turn ON PERSONAL USE option")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {

									new UpdatePersonalUseTask(true).execute();

									// updatePersonalUse(true);

								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {

								}
							}).setTitle("uDrove Mobile");

			AlertDialog alert = builder.create();
			alert.show();

		}

	}

	public void updatePersonalUse(boolean setpersonalUse) {

		if (setpersonalUse) {

			tbtn_personaluse.setSelected(true);
			ll_personaluse.setBackgroundColor(getResources().getColor(
					R.color.red));
			btnDutyStatus.setEnabled(false);

			// btnTripMang.setEnabled(false);

			/*
			 * //set current duty status to OFF Duty
			 * _edittxtdutystatus.setText("OF");
			 * _edittxtdutystatus.setTextColor(Color.parseColor("#FF6699"));
			 */

		} else {

			tbtn_personaluse.setSelected(false);
			ll_personaluse.setBackgroundColor(getResources().getColor(
					R.color.udrove_blue));
			btnDutyStatus.setEnabled(true);
			// btnTripMang.setEnabled(true);

		}

	}

	private class UpdatePersonalUseTask extends AsyncTask<String, Void, String> {

		ProgressDialog dialog;

		boolean setPersonalUse = false;

		public UpdatePersonalUseTask(boolean flag) {

			setPersonalUse = flag;

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(uDroveHomeActivity.this);
			dialog.setMessage(HTTPConstants.CONNECTING);
			dialog.show();
		}

		@Override
		protected String doInBackground(String... urls) {

			StringEntity se = null;
			String str = "";
			if (AppUtil.isInternetAvailable(uDroveHomeActivity.this)) {

				HttpClient httpclient = new DefaultHttpClient();
				HttpParams myParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(myParams, 10000);
				HttpConnectionParams.setSoTimeout(myParams, 10000);

				json = new JSONObject();
				HttpPost httput = new HttpPost(
						HTTPConstants.SET_PERSONAL_CONVEYANCE_FLAG);

				httput.setHeader("Accept", "application/json");
				httput.setHeader("Content-type", "application/json");

				try {

					json.put("time_stamp", HTTPUtil.TodaysDate());
					json.put("userid", udroveapp.getUserid());
					json.put("password", udroveapp.getPassword());

					json.put("personal_conveyance_flag", setPersonalUse);

					se = new StringEntity(json.toString());

					System.out.println("The SE is  request " + se.toString());

					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
							"application/json"));
					httput.setEntity(se);

					HttpResponse response = httpclient.execute(httput);

					byte[] result = EntityUtils.toByteArray(response
							.getEntity());

					str = new String(result, "UTF-8");

					Log.v(">>>>>>>", "str:" + str);

					return str;

				} catch (UnsupportedEncodingException e) {

					return AppConstants.SERVER_CONNECTION_ERROR;
				} catch (Exception e) {

					return AppConstants.SERVER_CONNECTION_ERROR;

				}

			} else {

				return AppConstants.INTERNET_CONNECTION_ERROR1;

			}

		}

		@Override
		protected void onPostExecute(String posts) {

			dialog.dismiss();

			if (posts != null) {
				// dutyHoursTv.setText(HTTPUtil.TodaysDate());
				String result = HTTPUtil.ParseLoginJson(posts);
				String spilt[] = result.split(",");
				if (spilt[0] != null && spilt[0].equals("FAIL")) {

					// update personal use status fail
					UIUtils.showServiceError(uDroveHomeActivity.this, spilt[1]);

				} else {
					Log.v("PERSONAL CONVEYANCE", "Success");

					dbAdapter.open();
					dbAdapter.updatePersonalConveyance(String
							.valueOf(setPersonalUse));
					dbAdapter.close();

					updatePersonalUse(setPersonalUse);

					new GetCurrentDayLogTask().execute();

				}

			}
		}
	}

}
