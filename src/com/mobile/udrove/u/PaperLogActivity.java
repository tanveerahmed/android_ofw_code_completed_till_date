package com.mobile.udrove.u;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class PaperLogActivity extends Activity implements OnClickListener,
		OnTouchListener, UDroveServiceListener {
	Button strbtnback, btn_email, btn_fax, btn_Approve;
	int dateIndex;
	int min = -1, max = -1;
	Button prevBtn, nextBtn;
	ImageView paperLogImgView;
	Cursor logDetails;

	static final int LOGID_INDEX = 0;
	static final int LOGDATE_INDEX = 1;
	static final int LOGAPPROVED_INDEX = 2;
	static final int LOGURL_INDEX = 3;

	uDroveDataBaseAdapter dbAdapter;
	uDroveApplication udroveapp;

	String[] logDates;
	String[] logIds;
	String[] logApproved;
	String[] logImageURL;

	private static final String TAG = "Touch";
	@SuppressWarnings("unused")
	private static final float MIN_ZOOM = 1f, MAX_ZOOM = 1f;

	// These matrices will be used to scale points of the image
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();

	// The 3 states (events) which the user is trying to perform
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;

	// these PointF objects are used to record the point(s) the user is touching
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	double max_zoom = 3, min_zoom = 1; // or put other limits, 3 and 0.4 is

	private int serviceIndex = -1;// example

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.driverpaperlog1);

		if (getIntent() != null) {

			if (getIntent().getExtras() != null) {
				dateIndex = getIntent().getExtras().getInt("dateIndex");
			}
		}

		paperLogImgView = (ImageView) findViewById(R.id.imageView1);
		paperLogImgView.setOnTouchListener(this);
		prevBtn = (Button) findViewById(R.id.btn_previous);
		nextBtn = (Button) findViewById(R.id.btn_next);
		prevBtn.setOnClickListener(this);
		nextBtn.setOnClickListener(this);

		strbtnback = (Button) findViewById(R.id.strbtnback);
		btn_email = (Button) findViewById(R.id.btn_email);
		btn_fax = (Button) findViewById(R.id.btn_fax);
		btn_Approve = (Button) findViewById(R.id.btn_approve);

		strbtnback.setOnClickListener(this);
		btn_email.setOnClickListener(this);
		btn_fax.setOnClickListener(this);
		btn_Approve.setOnClickListener(this);

		udroveapp = ((uDroveApplication) getApplication());

		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		try {

			logDetails = dbAdapter.getLastSevenLogDetails();

		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbAdapter.close();

		if (logDetails != null && logDetails.getCount() > 0) {
			min = 0;
			max = logDetails.getCount() - 1;
			logDetails.moveToPosition(0);
			logIds = new String[logDetails.getCount()];
			logDates = new String[logDetails.getCount()];
			logApproved = new String[logDetails.getCount()];
			logImageURL = new String[logDetails.getCount()];

			int i = 0;
			do {
				logIds[i] = logDetails.getString(LOGID_INDEX);
				logDates[i] = logDetails.getString(LOGDATE_INDEX);
				logApproved[i] = logDetails.getString(LOGAPPROVED_INDEX);
				logImageURL[i] = logDetails.getString(LOGURL_INDEX);
				i++;
			} while (logDetails.moveToNext());
			logDetails.close();

			setPaperLogImage(dateIndex);

		}

	}

	private void setPaperLogImage(int dateIndex2) {
		// TODO Auto-generated method stub
		if (logApproved[dateIndex2].equals("1")
				|| logApproved[dateIndex2].equals("true")) {
			btn_Approve.setText("Approved");
			btn_Approve.setEnabled(false);
		} else {
			btn_Approve.setText("Approve");
			btn_Approve.setEnabled(true);
		}

		if (min == max) {
			prevBtn.setEnabled(false);
			nextBtn.setEnabled(false);
			btn_Approve.setText("Approve");
			btn_Approve.setEnabled(false);
		} else if (dateIndex2 == min) {
			prevBtn.setEnabled(true);
			nextBtn.setEnabled(false);
			btn_Approve.setText("Approve");
			btn_Approve.setEnabled(false);
		} else if (dateIndex2 == max) {
			nextBtn.setEnabled(true);
			prevBtn.setEnabled(false);

		} else {
			prevBtn.setEnabled(true);
			nextBtn.setEnabled(true);
		}

		if (AppUtil.isInternetAvailable(this)) {

			if (logImageURL[dateIndex2] != null
					&& logImageURL[dateIndex2].length() != 0) {

				new DownloadImageTask().execute(logImageURL[dateIndex2]);

			} else {

				serviceIndex = 0;
				callPaperLogService();

			}

		} else {
			Toast.makeText(this, AppConstants.INTERNET_CONNECTION_ERROR,
					Toast.LENGTH_SHORT).show();
		}

	}

	public void onClick(View v) {
		int id = v.getId();
		switch (id) {

		case R.id.strbtnback:
			finish();
			break;
		case R.id.btn_previous:
			dateIndex++;
			setPaperLogImage(dateIndex);
			break;
		case R.id.btn_next:
			dateIndex--;
			setPaperLogImage(dateIndex);
			break;
		case R.id.btn_email:

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.putExtra("DocType", "LOGS");
			openActivity.putExtra("DocIds", logIds[dateIndex]);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.EmailActivity");
			startActivity(openActivity);
			break;

		case R.id.btn_fax:

			Intent openActivity1 = new Intent();
			openActivity1.setAction(Intent.ACTION_VIEW);
			openActivity1.putExtra("DocType", "LOGS");
			openActivity1.putExtra("DocIds", logIds[dateIndex]);
			openActivity1.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.FaxActivity");
			startActivity(openActivity1);

			break;

		case R.id.btn_approve:
			/* call approve service if driver signature is available */

			if (!uDroveApplication.getSettingsImageUrl().equalsIgnoreCase(
					"null")
					&& uDroveApplication.getSettingsImageUrl().length() > 0) {

				serviceIndex = 1;
				callPaperLogService();

			} else {
				Toast.makeText(PaperLogActivity.this,
						"Cannot Approve the Log as Signature is not uploaded",
						Toast.LENGTH_SHORT).show();
			}

			break;
		}

	}

	private void callPaperLogService() {
		// TODO Auto-generated method stub

		JSONObject json = new JSONObject();
		String serviceUrl = null;
		try {

			json.put("time_stamp", HTTPUtil.TodaysDate());
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());

			if (serviceIndex == 1) {
				json.put("log_id", logIds[dateIndex]);
				json.put("log_date", logDates[dateIndex]);

				serviceUrl = HTTPConstants.APPROVE_LOG;
			} else if (serviceIndex == 0) {

				json.put("paper_log_date", logDates[dateIndex]);
				serviceUrl = HTTPConstants.GET_PAPER_LOG;
			}

			UDroveService serv = new UDroveService(this, json, this);
			serv.execute(serviceUrl);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			System.out.println("Approve logs Failed");
			Toast.makeText(PaperLogActivity.this, spilt[1], Toast.LENGTH_SHORT)
					.show();

		} else {

			try {
				JSONObject jObject = new JSONObject(posts);

				if (serviceIndex == 0) {

					JSONObject bodyObject = jObject.getJSONObject("body");

					dbAdapter.open();
					dbAdapter.upDateLogsPaperLogUrl(logIds[dateIndex],
							bodyObject.getString("paper_log_url"));
					dbAdapter.close();
					
					logImageURL[dateIndex] = bodyObject
							.getString("paper_log_url");
					
					new DownloadImageTask().execute(logImageURL[dateIndex]);
                   
					return;
				} else if (serviceIndex == 1) {

					dbAdapter.open();
					dbAdapter.upDatePaperLogUrl(logIds[dateIndex]);
					dbAdapter.close();
					logApproved[dateIndex] = "1";
					setPaperLogImage(dateIndex);

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.v(">>>>>>>>>>>>>>>>", "onPreExecute");
			dialog = new ProgressDialog(PaperLogActivity.this);
			dialog.setMessage("Loading Image...");
			dialog.show();
		}

		@Override
		protected Bitmap doInBackground(String... urls) {

			Bitmap bmp = null;
			try {
				bmp = BitmapFactory.decodeStream((InputStream) new URL(urls[0])
						.getContent());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			dialog.dismiss();
			if (result != null) {
				paperLogImgView.setImageBitmap(result);
			}
		}
	}

	// @Override
	public boolean onTouch(View v, MotionEvent event) {
		ImageView view = (ImageView) v;
		view.setScaleType(ImageView.ScaleType.MATRIX);
		float scale;

		// dumpEvent(event);
		// Handle touch events here...

		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: // first finger down only
			savedMatrix.set(matrix);
			start.set(event.getX(), event.getY());
			Log.d(TAG, "mode=DRAG"); // write to LogCat
			mode = DRAG;
			break;

		case MotionEvent.ACTION_UP: // first finger lifted

			float f[] = new float[9];
			matrix.getValues(f);
			if (f[0] > max_zoom) {
				matrix.postScale((float) max_zoom / f[0], (float) max_zoom
						/ f[0], mid.x, mid.y);
			}
			if (f[0] < min_zoom) {
				matrix.postScale((float) min_zoom / f[0], (float) min_zoom
						/ f[0], mid.x, mid.y);
			}

		case MotionEvent.ACTION_POINTER_UP: // second finger lifted

			mode = NONE;
			Log.d(TAG, "mode=NONE");
			break;

		case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down

			oldDist = spacing(event);
			Log.d(TAG, "oldDist=" + oldDist);
			if (oldDist > 5f) {
				savedMatrix.set(matrix);
				midPoint(mid, event);
				mode = ZOOM;
				Log.d(TAG, "mode=ZOOM");
			}
			break;

		case MotionEvent.ACTION_MOVE:

			if (mode == DRAG) {
				matrix.set(savedMatrix);

				System.out.println("DRAGGING" + "event.getX()" + event.getX());
				System.out.println("DRAGGING" + "event.getY()" + event.getY());
				System.out.println("DRAGGING" + "start.x" + start.x);
				System.out.println("DRAGGING" + "start.y" + start.y);

				matrix.postTranslate(event.getX() - start.x, event.getY()
						- start.y); // create the transformation in the matrix
									// of points
			} else if (mode == ZOOM) {
				// pinch zooming
				float newDist = spacing(event);

				System.out.println("ZOOOMING" + "newDist" + newDist);

				// Log.d(TAG, "newDist=" + newDist);
				if (newDist > 5f) {
					matrix.set(savedMatrix);
					scale = newDist / oldDist; // setting the scaling of the
												// matrix...if scale > 1 means
												// zoom in...if scale < 1 means
					System.out.println("ZOOOMING" + "oldDist" + newDist);
					System.out.println("ZOOOMING" + "Scale" + scale);

					matrix.postScale(scale, scale, mid.x, mid.y);
				}
			}
			break;
		}

		view.setImageMatrix(matrix); // display the transformation on screen

		return true; // indicate event was handled
	}

	
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return FloatMath.sqrt(x * x + y * y);
	}

	
	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}

}
