package com.mobile.udrove.u;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers to on,off,sleeper,and driving
 * 
 * @dated 16-Feb-2012
 */

public class DvirViewSettingsActivity extends uDroveParentActivity {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	Uri imageUri = null;
	TextView lbltxtheader;
	ImageView image;
	EditText remarksText;

	private float curScale = 1F;
	private float curRotate = 0F;

	String changestatustitle, checkboxid;

	uDroveApplication udroveapp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvirviewsettings);

		udroveapp = ((uDroveApplication) getApplication());

		onclickListener listener = new onclickListener();
		findViewById(R.id.back_header_button).setOnClickListener(listener);
		findViewById(R.id.camera_header_button).setOnClickListener(listener);
		image = (ImageView) findViewById(R.id.imageview);

		lbltxtheader = (TextView) findViewById(R.id.txthead);
		changestatustitle = AppConstants.changestatustooffduty;
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {

				findViewById(R.id.camera_header_button).setEnabled(false);
				lbltxtheader
						.setText(getIntent().getExtras().getString("title"));
				remarksText = (EditText) findViewById(R.id.txtremarks);

				remarksText.setText(getIntent().getExtras()
						.getString("remarks"));
				remarksText.setEnabled(false);
				remarksText.setFocusable(false);
				if (getIntent().getExtras().getString("imgageuri") != null) {

					if (AppUtil.isInternetAvailable(this)) {

						new DownloadImageTask().execute(getIntent().getExtras()
								.getString("imgageuri"));
					} else {

						Toast.makeText(this,
								AppConstants.INTERNET_CONNECTION_ERROR,
								Toast.LENGTH_SHORT).show();
					}

				}
			}
		}
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.back_header_button:
				finish();
				break;

			}

		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// preventing default implementation previous to
			// android.os.Build.VERSION_CODES.ECLAIR
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(DvirViewSettingsActivity.this);
			dialog.setMessage("Loading Image...");
			dialog.show();
		}

		@Override
		protected Bitmap doInBackground(String... urls) {

			Bitmap bmp = null;

			InputStream inputStream = null;
			byte[] tempStorage = new byte[1024];// try to read 1Kb at time
			int bLength;
			try {

				File f = new File(
						AppUtil.getCacheDirectory(image.getContext()),
						"temp.jpg");
				FileOutputStream outputByteArrayStream = new FileOutputStream(f);
				inputStream = new URL(urls[0]).openStream();
				while ((bLength = inputStream.read(tempStorage)) != -1) {
					outputByteArrayStream.write(tempStorage, 0, bLength);
				}
				outputByteArrayStream.flush();
				outputByteArrayStream.close();
				inputStream.close();

				BitmapFactory.Options bitopt = new BitmapFactory.Options();
				bitopt.outHeight = image.getHeight();
				bitopt.outWidth = image.getWidth();
				bitopt.inSampleSize = 8;

				bmp = BitmapFactory.decodeFile(f.getPath(), bitopt);
				ByteArrayOutputStream outStreamthumb = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.JPEG, 75, outStreamthumb);
				if (bmp != null) {
					bmp = Bitmap.createScaledBitmap(bmp, image.getWidth(),
							image.getHeight(), true);
				}

			} catch (Exception e) {
				e.printStackTrace();
				if (inputStream != null)
					try {
						inputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}

			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			if (result != null) {
				image.setImageBitmap(result);
			}
			dialog.cancel();
		}
	}

}