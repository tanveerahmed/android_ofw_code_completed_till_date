package com.mobile.udrove.u;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TableRow.LayoutParams;

import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.db.uDroveDbAdapter;
import com.mobile.udrove.model.DVIRItemModel;
import com.mobile.udrove.model.DVIRModel;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;
import com.mobile.udrove.util.uDroveBase64;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers change status of driving
 * 
 * @dated 16-Feb-2012
 */

public class uDroveDvirViewNewActivity extends uDroveParentActivity implements
		UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	EditText edttxtdate;
	uDroveApplication udroveapp;
	uDroveDbAdapter db;
	private final int Thumbnail_WIDTH = 42;
	private final int Thumbnail_HEIGHT = 42;
	static final int DATETIMESELECTOR_ID = 5;
	int dvirId = 0;
	// ImageView img18,img19,img20,img21;
	ImageView signImageView, receiptImageView;
	String imageUri;
	JSONObject jsonObjSend;
	String driverSignUrl;
	RadioButton rb_DefectsCorretced;
	RadioButton rb_DefectsNotCorrected;

	TableLayout truckLayout, trailerLayout;

	uDroveDataBaseAdapter dbAdapter;

	int sigOrReceipt = 0;

	int[] truckDefectIds;
	String[] truckDefectNames;

	int[] trailerDefectIds;
	String[] trailerDefectNames;

	CheckBox currentCheckedCB;
	int fromCheckBoxChanged = 0;

	HashMap<Integer, String> inspectionData = new HashMap<Integer, String>();
	HashMap<Integer, String> inspectionTruckData = new HashMap<Integer, String>();
	HashMap<Integer, String> inspectionTrailerData = new HashMap<Integer, String>();
	HashMap<Integer, DVIRItemModel> dvirInspecthashmap;

	String selectedItem = "";
	int intSelectedItem = 0;

	int i;

	Bitmap bmp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvirview);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveDvirViewNewActivity.this);
		udroveapp = ((uDroveApplication) getApplication());
		signImageView = (ImageView) findViewById(R.id.sign_imageView);
		receiptImageView = (ImageView) findViewById(R.id.receipt_image_camera1);
		onclickListener listener = new onclickListener();
		receiptImageView.setOnClickListener(listener);
		findViewById(R.id.dvirapproveButton).setOnClickListener(listener);
		rb_DefectsCorretced = (RadioButton) findViewById(R.id.rb_DefectsCorrected);
		rb_DefectsNotCorrected = (RadioButton) findViewById(R.id.rb_DefectsNotCorrected);

		if (getIntent().getExtras() != null) {

			dvirId = getIntent().getExtras().getInt("dvirid");
		}

		truckLayout = (TableLayout) findViewById(R.id.TruckLayout1);
		trailerLayout = (TableLayout) findViewById(R.id.TrailerLayout1);

		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		MetaDataModel truckDefectModal = dbAdapter.getTruckDefectsTypes();
		MetaDataModel trailerDefectModal = dbAdapter.getTrailerDefectsTypes();

		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				System.out
						.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"
								+ cur.getString(0) + cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		if (truckDefectModal != null) {
			truckDefectIds = truckDefectModal.getIdList();
			truckDefectNames = truckDefectModal.getNameList();
		}

		if (trailerDefectModal != null) {
			trailerDefectIds = trailerDefectModal.getIdList();
			trailerDefectNames = trailerDefectModal.getNameList();
		}

		for (int k = 0; k < truckDefectIds.length; k++) {
			inspectionData.put(truckDefectIds[k], truckDefectNames[k]);
			inspectionTruckData.put(truckDefectIds[k], truckDefectNames[k]);
		}

		for (int k = 0; k < trailerDefectIds.length; k++) {
			inspectionData.put(trailerDefectIds[k], trailerDefectNames[k]);
			inspectionTrailerData.put(trailerDefectIds[k],
					trailerDefectNames[k]);
		}

		readDvirValues();

		TableRow tr = null;
		TextView tv = null;
		CheckBox cb1 = null;
		ImageView imgv = null;
		String[] temp;

		onInfoButtonClickListener infoListener = new onInfoButtonClickListener();

		System.out.println("Selected dvirMapDefected id "
				+ dvirInspecthashmap.keySet().size());

		for (i = 0; i < truckDefectIds.length; i++) {
			temp = truckDefectNames[i].split("-");

			tr = new TableRow(this);
			tr.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
			tv = new TextView(this);
			tv.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			tv.setText(temp[1]);
			tv.setTextColor(Color.parseColor("#000000"));

			cb1 = new CheckBox(this);
			cb1.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			cb1.setChecked(false);
			cb1.setClickable(false);

			imgv = new ImageView(this);
			imgv.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			imgv.setImageResource(R.drawable.info_normal);
			imgv.setPadding(0, 4, 0, 0);
			imgv.setTag(truckDefectIds[i]);
			imgv.setVisibility(View.INVISIBLE);

			if (dvirInspecthashmap.get(truckDefectIds[i]) != null) {

				cb1.setChecked(true);
				imgv.setVisibility(View.VISIBLE);
				imgv.setOnClickListener(infoListener);
			}
			tr.addView(tv);
			tr.addView(cb1);
			tr.addView(imgv);
			truckLayout.addView(tr);

		}

		for (i = 0; i < trailerDefectIds.length; i++) {

			temp = trailerDefectNames[i].split("-");
			tr = new TableRow(this);
			tr.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

			tv = new TextView(this);
			tv.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			tv.setText(temp[1]);
			tv.setTextColor(Color.parseColor("#000000"));

			cb1 = new CheckBox(this);
			cb1.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			cb1.setChecked(false);
			cb1.setClickable(false);

			imgv = new ImageView(this);
			imgv.setLayoutParams(new LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f));
			imgv.setImageResource(R.drawable.info_normal);
			imgv.setPadding(0, 4, 0, 0);
			imgv.setTag(trailerDefectIds[i]);
			imgv.setVisibility(View.INVISIBLE);

			if (dvirInspecthashmap.containsKey(trailerDefectIds[i])) {
				cb1.setChecked(true);
				imgv.setVisibility(View.VISIBLE);
				imgv.setOnClickListener(infoListener);
			}
			tr.addView(tv);
			tr.addView(cb1);
			tr.addView(imgv);
			trailerLayout.addView(tr);
		}

		/* date and time code */
		edttxtdate = (EditText) findViewById(R.id.txtdatetime);
		// updateDateDisplay();
		edttxtdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				showDialog(DATETIMESELECTOR_ID);
			}
		});

	}

	public void readDvirValues() {

		DVIRModel dvirModal = udroveapp.getLastSevenDvirsHashMap().get(dvirId);

		String odometer = dvirModal.getOdometer();
		String powerunit = dvirModal.getPowerUnit();
		String powerunitname = dvirModal.getPowerUnitName();
		String trailer = dvirModal.getTrailerId();
		String mechnanic_name = dvirModal.getMechanic_name();
		String driver_name = dvirModal.getDriver_name();
		String mechanic_inspection_date = dvirModal
				.getMechanic_inspection_date();
		String defects_corrected = dvirModal.getDefects_corrected();
		String dvir_receipt_image_name = dvirModal.getDvir_receipt_image_name();
		String approved = dvirModal.getApproved();
		String remarks = dvirModal.getRemarks();
		dvirInspecthashmap = dvirModal.getHashmapdviritem();
		if (!odometer.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txtodometer)).setText(odometer);
		}

		/*
		 * if(!powerunit.equalsIgnoreCase("null")){
		 * if(puHash.containsKey(Integer.parseInt(powerunit))){ ((EditText)
		 * findViewById
		 * (R.id.txtPwerunit)).setText(powerUnitNames[puHash.get(Integer
		 * .parseInt(powerunit))]); } }
		 */

		if (!powerunitname.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txtPwerunit)).setText(powerunitname);
		}

		if (!trailer.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txttrailer)).setText(trailer);
		}
		if (!mechnanic_name.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txtmechanic)).setText(mechnanic_name);
		}
		if (!driver_name.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txtdriver)).setText(driver_name);
		}
		if (!remarks.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txtremarks)).setText(remarks);
		}
		if (!mechanic_inspection_date.equalsIgnoreCase("null")) {
			((EditText) findViewById(R.id.txtdatetime))
					.setText(mechanic_inspection_date);
		}
		if (defects_corrected.equalsIgnoreCase("true")) {
			rb_DefectsCorretced.setChecked(true);
		} else if (defects_corrected.equalsIgnoreCase("false")) {
			rb_DefectsNotCorrected.setChecked(true);
		} else {
			rb_DefectsCorretced.setChecked(false);
			rb_DefectsNotCorrected.setChecked(false);
		}
		if (dvir_receipt_image_name.length() == 0
				|| dvir_receipt_image_name.equalsIgnoreCase("null")) {

			// receiptImageView.setImageResource(R.drawable.no_sign_image);
		} else {

			if (AppUtil.isInternetAvailable(this)) {

				new DownloadImageTask2().execute(dvir_receipt_image_name);
			} else {

				Toast.makeText(this, AppConstants.INTERNET_CONNECTION_ERROR,
						Toast.LENGTH_SHORT).show();
			}

		}

		/* checking for signature, if it is there */
		if (uDroveApplication.getSettingsImageUrl() != null
				&& uDroveApplication.getSettingsImageUrl().length() > 0) {

			driverSignUrl = uDroveApplication.getSettingsImageUrl();
			if (driverSignUrl.length() == 0
					|| driverSignUrl.equalsIgnoreCase("null")) {
				signImageView.setImageResource(R.drawable.no_sign_image);
			} else {

				if (AppUtil.isInternetAvailable(this)) {

					new DownloadImageTask().execute(driverSignUrl);
				} else {

					Toast.makeText(this,
							AppConstants.INTERNET_CONNECTION_ERROR,
							Toast.LENGTH_SHORT).show();
				}
			}
		}

	}

	protected class onInfoButtonClickListener implements OnClickListener {

		public void onClick(View v) {
			DVIRItemModel itemModal = dvirInspecthashmap.get(v.getTag());
			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.DvirViewSettingsActivity");
			openActivity.putExtra("title", inspectionData.get(v.getTag()));
			openActivity.putExtra("remarks", itemModal.getRemarks());
			openActivity.putExtra("imgageuri", itemModal.getImageUrl());
			openActivity.putExtra("camefrom", "dvirview");
			startActivity(openActivity);

		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {

			case R.id.receipt_image_camera1:
				Startcamera();
				break;
			case R.id.dvirapproveButton:

				String remarks = ((EditText) findViewById(R.id.txtremarks))
						.getText().toString();
				String mechnanic = ((EditText) findViewById(R.id.txtmechanic))
						.getText().toString();
				String driver = ((EditText) findViewById(R.id.txtdriver))
						.getText().toString();
				String datetime = ((EditText) findViewById(R.id.txtdatetime))
						.getText().toString();

				validatedvir(remarks, mechnanic, driver, datetime);
				break;

			}

		}
	}

	private void validatedvir(String remarks, String mechnanic, String driver,
			String datetime) {

		String errormsg = "";
		boolean complete = true;

		if (driver == null || driver.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DRIVER + "\n";
		}

		if (datetime == null || datetime.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DATETIME + "\n";
		}

		if (imageUri != null) {

			if (mechnanic == null || mechnanic.trim().length() == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.MECHANIC + "\n";
			}

		}

		if (!complete) {
			UIUtils.showError(uDroveDvirViewNewActivity.this, errormsg);
		} else {

			if (AppUtil.isInternetAvailable(this)) {

				try {
					String defectCorrecStr;
					if (rb_DefectsCorretced.isChecked()) {
						defectCorrecStr = "true";
					} else {
						defectCorrecStr = "false";

					}

					jsonObjSend = new JSONObject();
					jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
					jsonObjSend.put("userid", udroveapp.getUserid());
					jsonObjSend.put("password", udroveapp.getPassword());
					jsonObjSend.put("remarks", remarks);
					jsonObjSend.put("defects_corrected", defectCorrecStr);
					jsonObjSend.put("mechanic_name", mechnanic);
					jsonObjSend.put("driver_name", driver);
					jsonObjSend.put("mechanic_inspection_date", datetime);
					jsonObjSend.put("driver_sign_image_name", driverSignUrl);

					if (imageUri != null) {
						uploadImage(imageUri, "dvir_receipt_image_name");
					} else {
						// jsonObjSend.put("dvir_receipt_image_name", "");
						makeServiceCall();

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				UIUtils.showError(this, AppConstants.INTERNET_CONNECTION_ERROR);
			}

		}

	}

	private DateSlider.OnDateSetListener mDateTimeSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute);
			String nowDateString = HTTPUtil.TodaysDateWithoutZone();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			Date date1;
			Date date2;
			try {
				date1 = df.parse(SelectedDateString);
				date2 = df.parse(nowDateString);
				if (date1.before(date2)) {
					edttxtdate.setText(SelectedDateString
							+ AppUtil.getTimeZoneString());
				} else {

					edttxtdate.setText(nowDateString
							+ AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {

		System.out.println("On Create Dialog");
		final Calendar c = Calendar.getInstance();
		switch (id) {

		case DATETIMESELECTOR_ID:
			return new DateTimeSlider(this, mDateTimeSetListener, c);
		}
		return null;
	}

	private void updateDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());

		this.edttxtdate.setText(formattedDate3 + AppUtil.getTimeZoneString());
	}

	private void makeServiceCall() {

		UDroveService serv = new UDroveService(this, jsonObjSend, this);
		serv.execute(HTTPConstants.UPDATE_DVIR_URL + String.valueOf(dvirId));
	}

	public void Startcamera() {
		// TODO Auto-generated method stub

		Intent openActivity = new Intent();
		openActivity.setAction(Intent.ACTION_VIEW);

		openActivity
				.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.EditSignatureandExpenditureCameraActivity");
		openActivity.putExtra("intentcamefrom", "dvirreceipt");
		openActivity.putExtra("title", "DVIR Receipt");

		startActivity(openActivity);

	}

	public void uploadImage(String imageUri, String imageName) {
		try {

			dbAdapter.open();
			byte[] ba = dbAdapter.getImageFromGallery(imageUri);
			dbAdapter.close();

			System.gc();

			JSONObject json = new JSONObject();
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());
			json.put("category", "DRM");
			json.put("sub_category", "0");
			json.put("file_ext", "jpg");
			json.put("bindata",
					uDroveBase64.encodeToString(ba, uDroveBase64.DEFAULT));

			ba = null;

			HttpServiceListener listener = new HttpServiceListener();

			UDroveService serv = new UDroveService(
					uDroveDvirViewNewActivity.this, json, listener);
			serv.execute(HTTPConstants.IMAGE_UPLOAD, imageName);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete image" + posts);
		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {
				uDroveApplication.updateDvirsFromServer = true;
				Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
				finish();
			}

		} else {

			UIUtils.showSubscriptionChangedMessage(this);
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	protected class HttpServiceListener implements UDroveServiceListener {

		/*
		 * protected void uploadImage(Bitmap bitmap){
		 * 
		 * }
		 */

		public void onServiceComplete(String posts) {

		}

		public void onServiceComplete(String posts, String reference) {

			/* Log.v(">>>>>>>>>>>", "onservice complete inner: fuel receipts"); */

			String result = HTTPUtil.ParseImageJson(posts);

			String split[] = result.split(",");
			/* Log.v(">>>>>>>>>>>>>", "result" + result); */
			if (split[0] != null && split[0].equals("FAIL")) {
				UIUtils.showServiceError(uDroveDvirViewNewActivity.this,
						split[1]);
			} else {
				try {
					// String imageName =
					// split[2].substring(split[2].lastIndexOf("/") + 1);
					Log.v(">>>>>>>>>>", "IMAGENAME:" + split[2]);
					jsonObjSend.put("dvir_receipt_image_name", split[2]);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			makeServiceCall();

		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (udroveapp.getEditsignatureexpenditure() != null) {
			try {
				imageUri = udroveapp.getEditsignatureexpenditure();

				AppUtil.setImagetoView1(imageUri, receiptImageView, dbAdapter);

				udroveapp.setEditsignatureexpenditure(null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(uDroveDvirViewNewActivity.this);
			dialog.setMessage("Loading Image...");
			dialog.show();
		}

		@Override
		protected Bitmap doInBackground(String... urls) {

			Bitmap bmp = null;

			InputStream inputStream = null;
			byte[] tempStorage = new byte[1024];// try to read 1Kb at time
			int bLength;
			try {

				File f = new File(AppUtil.getCacheDirectory(signImageView
						.getContext()), "temp.jpg");
				FileOutputStream outputByteArrayStream = new FileOutputStream(f);
				inputStream = new URL(urls[0]).openStream();
				while ((bLength = inputStream.read(tempStorage)) != -1) {
					outputByteArrayStream.write(tempStorage, 0, bLength);
				}
				outputByteArrayStream.flush();
				outputByteArrayStream.close();
				inputStream.close();

				BitmapFactory.Options bitopt = new BitmapFactory.Options();
				bitopt.outHeight = signImageView.getHeight();
				bitopt.outWidth = signImageView.getWidth();
				bitopt.inSampleSize = 8;

				bmp = BitmapFactory.decodeFile(f.getPath(), bitopt);
				ByteArrayOutputStream outStreamthumb = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.JPEG, 75, outStreamthumb);
				if (bmp != null) {
					bmp = Bitmap.createScaledBitmap(bmp,
							signImageView.getWidth(),
							signImageView.getHeight(), true);
				}

			} catch (Exception e) {
				e.printStackTrace();
				if (inputStream != null)
					try {
						inputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}

			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			if (result != null) {
				signImageView.setImageBitmap(result);
			}
			dialog.cancel();
		}
	}

	private class DownloadImageTask2 extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new ProgressDialog(uDroveDvirViewNewActivity.this);
			dialog.setMessage("Loading Image...");
			dialog.show();
		}

		@Override
		protected Bitmap doInBackground(String... urls) {

			Bitmap bmp = null;

			InputStream inputStream = null;
			byte[] tempStorage = new byte[1024];// try to read 1Kb at time
			int bLength;
			try {

				File f = new File(AppUtil.getCacheDirectory(receiptImageView
						.getContext()), "temp1.jpg");
				FileOutputStream outputByteArrayStream = new FileOutputStream(f);
				inputStream = new URL(urls[0]).openStream();
				while ((bLength = inputStream.read(tempStorage)) != -1) {
					outputByteArrayStream.write(tempStorage, 0, bLength);
				}
				outputByteArrayStream.flush();
				outputByteArrayStream.close();
				inputStream.close();

				BitmapFactory.Options bitopt = new BitmapFactory.Options();
				bitopt.outHeight = receiptImageView.getHeight();
				bitopt.outWidth = receiptImageView.getWidth();
				bitopt.inSampleSize = 8;

				bmp = BitmapFactory.decodeFile(f.getPath(), bitopt);
			
				if (bmp != null) {
					bmp = Bitmap.createScaledBitmap(bmp,
							receiptImageView.getWidth(),
							receiptImageView.getHeight(), true);
				}

			} catch (Exception e) {
				e.printStackTrace();
				if (inputStream != null)
					try {
						inputStream.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}

			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {

			if (result != null) {
				receiptImageView.setImageBitmap(result);
			}
			dialog.cancel();
		}
	}

	@Override
	public void finish() {

		dbAdapter.open();
		dbAdapter.deleteGallery();
		dbAdapter.close();

		super.finish();
	}

}