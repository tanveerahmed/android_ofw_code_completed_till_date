package com.mobile.udrove.u;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TableRow.LayoutParams;

import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.model.PODModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;
import com.mobile.udrove.util.uDroveBase64;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the BusinessTool Pod report
 * 
 * @dated 16-Feb-2012
 */

public class uDroveBusinessToolsPodActivity extends uDroveParentActivity
		implements UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	private String selectedItem;
	private int intselectedItem;
	private int index;
	private int podTypeId;

	static final int DATE_DIALOG_ID = 0;
	EditText edttxtdate;
	uDroveApplication udroveapp;
	ImageView image;
	TableLayout ll = null;
	TableRow tr = null;

	private PODModel p = null;
	private JSONArray jArray = null;
	private JSONObject jsonObjSend = null;

	private boolean imageUploadCompleted = true;

	uDroveDataBaseAdapter dbAdapter;

	int[] stateIds;
	String[] stateNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.businesstoolpod);

		udroveapp = ((uDroveApplication) getApplication());
		
		
		

		onclickListener listener = new onclickListener();
		findViewById(R.id.saveButton).setOnClickListener(listener);
		findViewById(R.id.docphotos).setOnClickListener(listener);

		// image = (ImageView) findViewById(R.id.docimage);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveBusinessToolsPodActivity.this);

		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		MetaDataModel stateModal = dbAdapter.getStatesList();

		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		if (stateModal != null) {
			stateIds = stateModal.getIdList();
			stateNames = stateModal.getNameList();

		}

		udroveapp.setDefaultPODArrayListModel();
		Spinner state = (Spinner) findViewById(R.id.statespineer);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stateNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		state.setFocusable(true);
		state.setFocusableInTouchMode(true);
		state.setAdapter(adapter);
		state.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

				selectedItem = stateNames[arg2];
				intselectedItem = stateIds[arg2];
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		edttxtdate = (EditText) findViewById(R.id.txtdatetime);

		// display the current date
		updateDateDisplay();

		edttxtdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_ID);
			}
		});

	}

	public void ReDrawDocPhotos() {

		// Create a new hashamp

		ll = (TableLayout) findViewById(R.id.dynamicbuttonslayout);
		TextView doctype = null;
		ImageView thumbnail = null;
		ImageView imgdelete = null;

		if (udroveapp.getPODArrayListModel().size() > 0) {
			for (int i = 0; i < udroveapp.getPODArrayListModel().size(); i++) {

				p = udroveapp.getPODArrayListModel().get(i);

				tr = new TableRow(this);
				tr.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.FILL_PARENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

				doctype = new TextView(this);
				doctype.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				doctype.setTextColor(Color.BLACK);

				String[] temp;
				temp = p.getPodtype().split(",");
				doctype.setText(temp[0]);

				thumbnail = new ImageView(this);
				thumbnail.setLayoutParams(new LayoutParams(42, 42));
				
				thumbnail.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent openActivity2 = new Intent();
						openActivity2.setAction(Intent.ACTION_VIEW);
						openActivity2.setClassName("com.mobile.udrove.u",
								"com.mobile.udrove.u.PreviewImageActivity");
						openActivity2.putExtra("imgageuri", p.getImageuri());
						startActivity(openActivity2);
					}
				});
				try {
           
					AppUtil.setImagetoView1(p.getImageuri(), thumbnail,
							dbAdapter,42,42,getResources().getDisplayMetrics().density);

				} catch (Exception e) {
					e.printStackTrace();
				}

				imgdelete = new ImageView(this);
				imgdelete.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				imgdelete.setId(i);
				imgdelete.setBackgroundResource(R.drawable.udrove_delete);
				imgdelete.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						udroveapp.getPODArrayListModel().remove(v.getId());
						ll.removeAllViews();
						ReDrawDocPhotos();
					}
				});
				tr.addView(doctype);
				tr.addView(thumbnail);
				tr.addView(imgdelete);

			}
			ll.addView(tr);

		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.saveButton:

				index = 0;

				String shipper = ((EditText) findViewById(R.id.txtshipper))
						.getText().toString();
				String con = ((EditText) findViewById(R.id.txtcon)).getText()
						.toString();
				String receivedby = ((EditText) findViewById(R.id.txtreceivedby))
						.getText().toString();

				String poddate = ((EditText) findViewById(R.id.txtdatetime))
						.getText().toString();

				String destcity = ((EditText) findViewById(R.id.txtdestcity))
						.getText().toString();

				String broker = ((EditText) findViewById(R.id.txtbroker))
						.getText().toString();

				String referenceNo = ((EditText) findViewById(R.id.txtreferenceno))
						.getText().toString();

				String email = ((EditText) findViewById(R.id.txtemail))
						.getText().toString();

				validatePod(shipper, con, receivedby, poddate, destcity,
						broker, email, referenceNo);

				break;

			case R.id.docphotos:

				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.PodsSettingsActivity");
				startActivity(openActivity);
				// finish();

				break;
			}

		}

	}

	private void validatePod(String shipper, String con, String receivedby,
			String poddate, String destcity, String broker, String email,
			String referenceNo) {

		String errormsg = "";
		boolean complete = true;

		if (intselectedItem == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DEST_STATE + "\n";
		}

		if (destcity == null || destcity.trim().length() == 0) {
			Log.v(">>>>>>>>>>m", "DESTCITY TRUE");
			complete = false;
			errormsg = errormsg + AppConstants.DEST_CITY + "\n";
		}

		if (shipper == null || shipper.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.SHIPPER + "\n";
		}

		if (con == null || con.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.CON + "\n";
		}
		if (receivedby == null || receivedby.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.REC + "\n";
		}
		if (email != null && email.length() > 0) {
			if (!AppUtil.isEmailValid(email)) {
				complete = false;
				errormsg = errormsg + AppConstants.INVALIDEMAIL + "\n";
			}
		}

		System.out.println("the size of the podarraylist is  "
				+ udroveapp.getPODArrayListModel().size());
		if (udroveapp.getPODArrayListModel().size() <= 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DOCPHOTO + "\n";
		}

		if (!complete) {
			UIUtils.showError(this, errormsg);
		}

		else {

			if (AppUtil.isInternetAvailable(this)) {

				jsonObjSend = new JSONObject();
				try {
					jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
					jsonObjSend.put("userid", udroveapp.getUserid());
					jsonObjSend.put("password", udroveapp.getPassword());
					jsonObjSend.put("pod_date", poddate);
					jsonObjSend.put("dest_city", destcity);
					jsonObjSend.put("dest_state_id", intselectedItem);
					jsonObjSend.put("shipper", shipper);
					jsonObjSend.put("consignee", con);
					jsonObjSend.put("broker", broker);
					jsonObjSend.put("recieved_by", receivedby);
					jsonObjSend.put("reference_no", referenceNo);
					jsonObjSend.put("email_to", email);
					jArray = new JSONArray();

					if (udroveapp.getPODArrayListModel().size() > 0) {
						index = 0;

						if (imageUploadCompleted) {
							uploadImage(index);
						} else {
							Toast.makeText(
									this,
									"Connection to the Server failed. Please try again.",
									Toast.LENGTH_LONG).show();
						}

					} else {
						JSONObject expenses = new JSONObject();
						jArray.put(expenses);
						makeServiceCall();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				UIUtils.showError(this, AppConstants.INTERNET_CONNECTION_ERROR);
			}

		}

	}

	private DateSlider.OnDateSetListener mDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute);
			String nowDateString = HTTPUtil.TodaysDateWithoutZone();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			Date date1;
			Date date2;
			try {
				date1 = df.parse(SelectedDateString);
				date2 = df.parse(nowDateString);
				if (date1.before(date2)) {
					edttxtdate.setText(SelectedDateString
							+ AppUtil.getTimeZoneString());
				} else {

					edttxtdate.setText(nowDateString
							+ AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		final Calendar c = Calendar.getInstance();
		switch (id) {
		case DATE_DIALOG_ID:
			return new DateTimeSlider(this, mDateSetListener, c);

		}
		return null;
	}

	private void updateDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());

		this.edttxtdate.setText(formattedDate3 + AppUtil.getTimeZoneString());
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (udroveapp.getPODArrayListModel() != null) {
			if (udroveapp.getPODArrayListModel().size() > 0) {
				if (uDroveApplication.fromBackButton) {
					Log.v("%%%%%%%%%%%%%%", "From Back Button");
				} else {
					Log.v("%%%%%%%%%%%%%%", "Not From Back Button");
					ReDrawDocPhotos();
				}
			}
		}
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {
				Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
				finish();
			}

		} else {

			UIUtils.showSubscriptionChangedMessage(this);
		}

	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	public void uploadImage(int i) {
		try {
			p = udroveapp.getPODArrayListModel().get(i);
			podTypeId = p.getPodTypeId();
			String[] temp;
			temp = p.getPodtype().split(",");

			dbAdapter.open();
			byte[] ba = dbAdapter.getImageFromGallery(p.getImageuri());
			dbAdapter.close();

			System.gc();

			JSONObject json = new JSONObject();
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());
			json.put("category", "POD");
			json.put("sub_category", String.valueOf(temp[1]));
			json.put("file_ext", "jpg");
			json.put("bindata",
					uDroveBase64.encodeToString(ba, uDroveBase64.DEFAULT));

			ba = null;

			HttpServiceListener listener = new HttpServiceListener();
			UDroveService serv = new UDroveService(
					uDroveBusinessToolsPodActivity.this, json, listener);
			serv.execute(HTTPConstants.IMAGE_UPLOAD, temp[1]);
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected class HttpServiceListener implements UDroveServiceListener {

		public void onServiceComplete(String posts) {

		}

		public void onServiceComplete(String posts, String reference) {

			String result = HTTPUtil.ParseImageJson(posts);

			String split[] = result.split(",");
			if (split[0] != null && split[0].equals("FAIL")) {
				UIUtils.showServiceError(uDroveBusinessToolsPodActivity.this,
						split[1]);
				imageUploadCompleted = false;
			} else {
				try {
					JSONObject expenses = new JSONObject();
					expenses.put("pod_type_id", reference);
					expenses.put("pod_image_name", split[2]);
					jArray.put(expenses);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			index++;
			if (index == udroveapp.getPODArrayListModel().size()) {
				makeServiceCall();
			}
			{
				uploadImage(index);
			}

		}
	}

	private void makeServiceCall() {

		try {
			jsonObjSend.put("pod_items", jArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		UDroveService serv = new UDroveService(this, jsonObjSend, this);
		serv.execute(HTTPConstants.POD_URL);
	}

	@Override
	public void finish() {
		
		dbAdapter.open();
		dbAdapter.deleteGallery();
		dbAdapter.close();

		super.finish();
	}

}