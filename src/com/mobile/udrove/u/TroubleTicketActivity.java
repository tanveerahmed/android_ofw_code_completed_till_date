package com.mobile.udrove.u;

import java.util.HashMap;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers to on,off,sleeper,and driving
 * 
 * @dated 16-Feb-2012
 */

public class TroubleTicketActivity extends Activity implements
		UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	uDroveApplication udroveapp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.troubleticket);

		udroveapp = ((uDroveApplication) getApplication());
		onclickListener listener = new onclickListener();
		findViewById(R.id.submitButton).setOnClickListener(listener);
		findViewById(R.id.back_header_button).setOnClickListener(listener);

	}

	private void validateTroubleTicket(String description) {

		String errormsg = "";
		boolean complete = true;

		if (description == null || description.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DESC + "\n";
		}

		if (!complete) {
			UIUtils.showError(this, errormsg);
		}

		else {
			description = description.trim();

			System.out.println("The user id " + udroveapp.getUserid());
			HashMap<String, String> data;
			data = new HashMap<String, String>();
			data.put("time_stamp", HTTPUtil.TodaysDate());
			data.put("userid", udroveapp.getUserid());
			data.put("password", udroveapp.getPassword());
			data.put("issue_description", description);
			data.put("device_model",Build.MODEL + "," + Build.VERSION.RELEASE);
			UDroveService serv = new UDroveService(this, data, this);
			serv.execute(HTTPConstants.TROUBLE_URL);

		}
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.submitButton:
				// UIUtils.showInfo(TroubleTicketActivity.this,
				// "Record is Save");
				String description = ((EditText) findViewById(R.id.txtremarks))
						.getText().toString();
				validateTroubleTicket(description);
				break;

			case R.id.back_header_button:
				finish();
				break;

			}

		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// preventing default implementation previous to
			// android.os.Build.VERSION_CODES.ECLAIR
			System.out.println("Pressed Back Key");
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete " + posts);
		String result = HTTPUtil.ParseTroubleTicketJson(posts);
		String spilt[] = result.split(",");
		if(spilt[2]!=null && spilt[2].equalsIgnoreCase("false")){
	
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			UIUtils.showServiceError(this, spilt[1]);
		} else {
			Toast.makeText(this,"Issue Submitted Succesfully.TicketiDd is: "+spilt[1], Toast.LENGTH_LONG).show();
		
			finish();

		}
		
		}else{
		
        UIUtils.showSubscriptionChangedMessage(this);			
	}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}

}