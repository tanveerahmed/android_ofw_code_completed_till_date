package com.mobile.udrove.u;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers to on,off,sleeper,and driving
 * 
 * @dated 16-Feb-2012
 */

public class uDroveChangestatusHomeActivity extends uDroveParentActivity implements UDroveServiceListener{

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */
	JSONObject json;
	TimerTask doAsynchronousTask;
	final Handler handler = new Handler();
	Timer timer;
	TextView tv;
	uDroveDataBaseAdapter dbAdapter;
	uDroveApplication udroveapp;

	Button btnOF, btnON, btnSB, btnDR, btnOilField;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changestatushome);

		tv = (TextView) findViewById(R.id.txtdutyhours);

		btnOF = (Button) findViewById(R.id.offdutyButton);
		btnON = (Button) findViewById(R.id.ondutyButton);
		btnSB = (Button) findViewById(R.id.sleeperberthButton);
		btnDR = (Button) findViewById(R.id.drivingButton);
		btnOilField = (Button) findViewById(R.id.oilField);
		
		onclickListener listener = new onclickListener();
		btnON.setOnClickListener(listener);
		btnOF.setOnClickListener(listener);
		btnSB.setOnClickListener(listener);
		btnDR.setOnClickListener(listener);
		btnOilField.setOnClickListener(listener);
		
		findViewById(R.id.loginfoButton).setOnClickListener(listener);
		udroveapp = ((uDroveApplication) getApplication());

		this.checkDriverType();
		
		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveChangestatusHomeActivity.this);

		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				System.out
						.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"
								+ cur.getString(0) + cur.getString(1));
				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

	}

	public void UpdateExemptionStatusOnUI()
	{
		switch(udroveapp.getDriverType()){
		
		case 0:
			{
				btnOilField.setVisibility(View.GONE);
				break;
			}
		case AppConstants.OIL_FIELD_EXEMPTION:
			{
				break;
			}
		default:
			{
				btnOilField.setVisibility(View.GONE);
				break;
			}
	}
}
	
	public void checkDriverType()
	{
		HashMap<String, String> data;
		data = new HashMap<String, String>();
		data.put("userid", udroveapp.getUserid());
		data.put("password", udroveapp.getPassword());
		
		// data.put("userid", "t_driver");
		// data.put("password", "driver123");
		System.out.println("Posted string is:"+data.toString());
		
		UDroveService serv = new UDroveService(this, data, this);
		serv.execute(HTTPConstants.GET_USER_EXEMPTIONS);
	}
	
	public void updateDriverStatus() {

		System.out.println("Am i able to view it");
		/* Driver Status logic comes here */
		dbAdapter.open();
		int driverStatus = dbAdapter.getCurrentDutyStatus();
		dbAdapter.close();
		switch (driverStatus) {
		case 4: // ON Duty
			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);
			btnOilField.setEnabled(true);
			btnOilField.setTextColor(Color.WHITE);
			btnOF.setEnabled(true);
			btnOF.setTextColor(Color.WHITE);
			btnSB.setEnabled(true);
			btnSB.setTextColor(Color.WHITE);
			btnDR.setEnabled(true);
			btnDR.setTextColor(Color.WHITE);
			break;
		case 3: // off duty
			btnOF.setEnabled(false);
			btnOF.setTextColor(Color.parseColor("#FF6699"));
			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);
			btnSB.setEnabled(true);
			btnSB.setTextColor(Color.WHITE);
			btnDR.setEnabled(true);
			btnDR.setTextColor(Color.WHITE);
			btnOilField.setEnabled(true);
			btnOilField.setTextColor(Color.WHITE);
			break;
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
			btnOF.setEnabled(true);
			btnOF.setTextColor(Color.WHITE);

			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);

			btnSB.setEnabled(true);
			btnSB.setTextColor(Color.WHITE);

			btnDR.setEnabled(true);
			btnDR.setTextColor(Color.WHITE);
			
			btnOilField.setEnabled(true);
			btnOilField.setTextColor(Color.WHITE);
			break;

		case 2: // sleeper
			btnSB.setEnabled(false);
			btnSB.setTextColor(Color.parseColor("#FF6600"));
			
			btnOF.setEnabled(true);
			btnOF.setTextColor(Color.WHITE);

			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);

			btnDR.setEnabled(true);
			btnDR.setTextColor(Color.WHITE);
			
			btnOilField.setEnabled(true);
			btnOilField.setTextColor(Color.WHITE);
			break;

		case 1: // driving
			btnDR.setEnabled(false);
			btnDR.setTextColor(Color.parseColor("#99CC00"));
			
			btnOF.setEnabled(true);
			btnOF.setTextColor(Color.WHITE);

			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);

			btnSB.setEnabled(true);
			btnSB.setTextColor(Color.WHITE);

			btnOilField.setEnabled(true);
			btnOilField.setTextColor(Color.WHITE);
			break;
		case 11: 				//Oil-Field Waiting
			btnOF.setEnabled(true);
			btnOF.setTextColor(Color.WHITE);

			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);

			btnSB.setEnabled(true);
			btnSB.setTextColor(Color.WHITE);

			btnDR.setEnabled(true);
			btnDR.setTextColor(Color.WHITE);
			
			btnOilField.setEnabled(false);
			btnOilField.setTextColor(Color.parseColor("#FF6699"));
			break;
		default: // off duty
			btnOF.setEnabled(false);
			btnOF.setTextColor(Color.parseColor("#FF6699"));

			btnON.setEnabled(true);
			btnON.setTextColor(Color.WHITE);

			btnSB.setEnabled(true);
			btnSB.setTextColor(Color.WHITE);

			btnDR.setEnabled(true);
			btnDR.setTextColor(Color.WHITE);
			
			btnOilField.setEnabled(true);
			btnOilField.setTextColor(Color.WHITE);
			break;
		}

		TextView tv = (TextView) findViewById(R.id.txtdutyhours);
		try {
			String avhrs = AppUtil.getUpdatedAvailableDriveTime(this, tv);
			tv.setText(avhrs);
			/*
			 * if(!avhrs.equalsIgnoreCase("00:00")){
			 * 
			 * tv.setTextColor(Color.WHITE); }
			 */
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			// buttonClickAction(v.getId());

			if (isPersonalConveyanceOn()) {

				UIUtils.showPersonalUseFlagEnabledPromptFinish(
						uDroveChangestatusHomeActivity.this,
						AppConstants.TASK_TERMINATED_AS_PC_ON);

			} else {

				buttonClickAction(v.getId());

			}

		}

	}

	private void buttonClickAction(int id) {

		Intent openActivity = new Intent();
		openActivity.setAction(Intent.ACTION_VIEW);

		switch (id) {

		case R.id.ondutyButton:
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.uDroveChangestatusOnDutyActivity");
			startActivity(openActivity);

			break;
		case R.id.offdutyButton:
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.uDroveChangestatusOfftoSbActivity");
			openActivity.putExtra("changestatuscamefrom", "offdutybutton");
			startActivity(openActivity);

			break;
		case R.id.sleeperberthButton:
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.uDroveChangestatusOfftoSbActivity");
			openActivity.putExtra("changestatuscamefrom", "sleeperberthbutton");
			startActivity(openActivity);
			break;
		case R.id.drivingButton:
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.uDroveChangestatusDrivingActivity");
			startActivity(openActivity);
			break;
		case R.id.oilField:
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.OilFieldActivity");
			startActivity(openActivity);
			break;			
		case R.id.loginfoButton:
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u",
					"com.mobile.udrove.u.uDroveChangestatusLogInfoActivity");
			startActivity(openActivity);
			break;

		}

	}

	private boolean isPersonalConveyanceOn() {

		dbAdapter.open();
		boolean flag = dbAdapter.getPersonalConveyanceStatus();
		dbAdapter.close();
		return flag;

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		updateDriverStatus();
		scheduleTimerTask();
	}

	private void scheduleTimerTask() {

		doAsynchronousTask = new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				handler.post(new Runnable() {
					public void run() {
						try {
							System.out.println("TimerTask called at "
									+ HTTPUtil.TodaysDate());
							new GetCurrentDayLogTask().execute();
						} catch (Exception e) {
							// TODO Auto-generated catch block
						}
					}
				});
			}
		};
		timer = new Timer();
		timer.schedule(doAsynchronousTask, 0, AppConstants.TIMER_INTERVAL);// execute
																			// in
																			// every
																			// 1min
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		System.out.println("On Pause Method stopping timer at "
				+ HTTPUtil.TodaysDate());
		timer.cancel();
		timer = null;
		super.onPause();
	}

	private class GetCurrentDayLogTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... urls) {
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			json = new JSONObject();
			HttpPost httput = new HttpPost(HTTPConstants.GET_CURRENT_DAY_LOG);

			httput.setHeader("Accept", "application/json");
			httput.setHeader("Content-type", "application/json");

			StringEntity se = null;
			String str = "";
			try {
				json.put("time_stamp", HTTPUtil.TodaysDate());
				json.put("userid", udroveapp.getUserid());
				json.put("password", udroveapp.getPassword());
				se = new StringEntity(json.toString());
				System.out.println("The SE is  request " + se.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httput.setEntity(se);
				HttpResponse response = httpclient.execute(httput);
				byte[] result = EntityUtils.toByteArray(response.getEntity());
				str = new String(result, "UTF-8");
				Log.v(">>>>>>>", "str:" + str);
				return str;
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();

			}
			return str;
		}

		@Override
		protected void onPostExecute(String posts) {
			if (posts != null) {
				// dutyHoursTv.setText(HTTPUtil.TodaysDate());
				String result = HTTPUtil.ParseJson(posts);
				String spilt[] = result.split(",");
				if (spilt[0] != null && spilt[0].equals("FAIL")) {

					System.out.println("Current day logs got failed");

				} else {
					System.out.println("Current day logs  got successfully");
					populateCurrentDayLogToDB(posts);

				}
				updateDriverStatus();

			}
		}
	}

	public void populateCurrentDayLogToDB(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);
			dbAdapter.open();
			JSONObject bodyObject = jObject.getJSONObject("body");
			JSONArray dutyStatusesArray = bodyObject
					.getJSONArray("duty_statuses");
			if (dutyStatusesArray.length() > 0) { // Data at server side

				SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy");
				String todaysDate = "";
				try {
					todaysDate = df3.format(df3.parse(dutyStatusesArray
							.getJSONObject(0).getString("duty_date_time")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dbAdapter.deleteCurrentDayDutyStatuses(todaysDate);
				for (int i = 0; i < dutyStatusesArray.length(); i++) {
					dbAdapter.insertDutyStatuses(
							dutyStatusesArray.getJSONObject(i), todaysDate);
				}
			}

			JSONObject hosSummary = jObject.getJSONObject("summary");
			dbAdapter.deleteAllRecords("HOS_DETAILS");
			dbAdapter.insertHosInfo(hosSummary, HTTPUtil.TodaysDate1());

			JSONObject logInfo = jObject.getJSONObject("log_info");
			dbAdapter.deleteCurrentDayLog(logInfo.getString("log_date"));
			dbAdapter.insertLogsForCurrentDay(
					logInfo.getString("paper_log_id"),
					logInfo.getString("log_date"));
			dbAdapter.deleteAllRecords("LOG_INFO");
			dbAdapter.insertLogInfo(logInfo);

			/* check for personal use */

			JSONObject headerObject = jObject.getJSONObject("header");

			String pc_flag = headerObject.getString("personal_conveyance_flag");

			dbAdapter.updatePersonalConveyance(pc_flag);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dbAdapter.close();

		}
		dbAdapter.close();

	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub
		System.out.println("the onService Complete " + posts);
		String result = HTTPUtil.ParseLoginJson(posts);
		System.out.println("IN ChangeDutyStatusHome" + result);
		String spilt[] = result.split(",");
		
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			ShowAlert(spilt[1]);
		}
		else
		{   
			JSONObject jObject;
			try {
			jObject = new JSONObject(posts);
		
			JSONObject bodyObject = jObject.getJSONObject("body");
            
/*			Database Update pending. Need to complete this once UI 
 * 			update is done based on remote data.
 * 
 * 			dbAdapter.open();
			dbAdapter.insertDriverDetails(userName,password,bodyObject.getString("signature_url"),headerObject.getString("subscription_info"),
					bodyObject.getString("fuel_receipt_required"),bodyObject.getString("expense_receipt_required"));
			dbAdapter.close();
			*/
			JSONArray JArray = bodyObject.getJSONArray("exemption_ids");

			for(int i=0; i < JArray.length(); i++)
			{
				//Server responds with Status-ID 1 to indicate OIL_FIELD_EXEMPTION. 
				if(Integer.parseInt(JArray.getString(i)) == 1)
				{
					udroveapp.setDriverType(AppConstants.OIL_FIELD_EXEMPTION);
				}
			}
			
			UpdateExemptionStatusOnUI();
			// Done with the UI updation. Hence now we should reset the DriverType flag.
			udroveapp.setDriverType(0);
			} 
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}
	
	 public void ShowAlert(String Message)
	 {
			AlertDialog.Builder builder = new AlertDialog.Builder(uDroveChangestatusHomeActivity.this);
			builder.setMessage(Message).setCancelable(false).setPositiveButton("OK", null).setTitle(
					"uDrove Mobile").setIcon(R.drawable.icon_info);
			AlertDialog alert = builder.create();
			alert.show();
	 }
}