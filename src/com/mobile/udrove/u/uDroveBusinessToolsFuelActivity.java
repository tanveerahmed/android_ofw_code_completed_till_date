package com.mobile.udrove.u;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;
import com.mobile.udrove.util.uDroveBase64;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the BusinessTool Fuel report
 * 
 * @dated 16-Feb-2012
 */

public class uDroveBusinessToolsFuelActivity extends uDroveParentActivity
		implements UDroveServiceListener {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	String selectedItem = "";
	int intselectedItem = 0;

	static final int DATE_DIALOG_ID = 0;
	EditText edttxtdate;
	ImageView image1, image2;
	uDroveApplication udroveapp;
	boolean checkfromcamera = true;
	int index;

	CheckBox puReimburs, trReimburs;

	private JSONObject jsonObjSend;
	private String imageUri1, imageUri2;

	uDroveDataBaseAdapter dbAdapter;

	int[] stateIds;
	String[] stateNames;

	int[] powerUnitIds;
	String[] powerUnitNames;

	String selectedPodItem = "";
	int intSelectedPodItem = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.businesstoolfuel);

		onclickListener listener = new onclickListener();
		findViewById(R.id.saveButton).setOnClickListener(listener);

		udroveapp = ((uDroveApplication) getApplication());
		image1 = (ImageView) findViewById(R.id.businesstoolfuel_camera1);
		image1.setOnClickListener(listener);
		image2 = (ImageView) findViewById(R.id.businesstoolfuel_camera2);
		image2.setOnClickListener(listener);

		imageUri1 = null;
		imageUri2 = null;
		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveBusinessToolsFuelActivity.this);

		puReimburs = (CheckBox) findViewById(R.id.checkboxpureim);
		trReimburs = (CheckBox) findViewById(R.id.checkboxtrreim);

		dbAdapter = new uDroveDataBaseAdapter(this);

		dbAdapter.open();
		MetaDataModel stateModal = dbAdapter.getStatesList();
		MetaDataModel puModal = dbAdapter.getPowerUnitsTypes();
		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				System.out
						.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"
								+ cur.getString(0) + cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		if (stateModal != null) {
			stateIds = stateModal.getIdList();
			stateNames = stateModal.getNameList();

		}
		if (puModal != null) {
			powerUnitIds = puModal.getIdList();
			powerUnitNames = puModal.getNameList();
		}

		Spinner powerUnit = (Spinner) findViewById(R.id.poweUnitspiner);

		powerUnit.setFocusable(true);
		powerUnit.setFocusableInTouchMode(true);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, powerUnitNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		powerUnit.setAdapter(adapter);
		powerUnit.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				selectedPodItem = powerUnitNames[arg2];
				intSelectedPodItem = powerUnitIds[arg2];
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		Spinner state = (Spinner) findViewById(R.id.statespinner);
		ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stateNames);
		adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		state.setAdapter(adapter1);
		state.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub

				selectedItem = stateNames[arg2];
				intselectedItem = stateIds[arg2];

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		edttxtdate = (EditText) findViewById(R.id.txtdatetime);
		// get the current date

		// display the current date
		updateDateDisplay();

		edttxtdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_ID);
			}
		});


	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.businesstoolfuel_camera1:
				checkfromcamera = true;

				if (imageUri1 == null) {
					Startcamera(0);
				} else {

					final CharSequence[] items = { "Preview", "Edit" };
					AlertDialog.Builder builder = new AlertDialog.Builder(
							uDroveBusinessToolsFuelActivity.this);
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialogInterface,
										int item) {

									if (item == 0) {
										Intent openActivity2 = new Intent();
										openActivity2
												.setAction(Intent.ACTION_VIEW);
										openActivity2
												.setClassName(
														"com.mobile.udrove.u",
														"com.mobile.udrove.u.PreviewImageActivity");
										openActivity2.putExtra("imgageuri",
												imageUri1);
										startActivity(openActivity2);
									} else if (item == 1) {
										Startcamera(0);
									}
									return;
								}
							});
					builder.create().show();

				}
				break;

			case R.id.businesstoolfuel_camera2:
				checkfromcamera = false;

				if (imageUri2 == null) {
					Startcamera(1);
				} else {

					final CharSequence[] items = { "Preview", "Edit" };
					AlertDialog.Builder builder = new AlertDialog.Builder(
							uDroveBusinessToolsFuelActivity.this);
					builder.setItems(items,
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialogInterface,
										int item) {

									if (item == 0) {
										Intent openActivity2 = new Intent();
										openActivity2
												.setAction(Intent.ACTION_VIEW);
										openActivity2
												.setClassName(
														"com.mobile.udrove.u",
														"com.mobile.udrove.u.PreviewImageActivity");
										openActivity2.putExtra("imgageuri",
												imageUri2);
										startActivity(openActivity2);
									} else if (item == 1) {
										Startcamera(1);
									}
									return;
								}
							});
					builder.create().show();
				}

				break;
			case R.id.saveButton:

				index = 0;

				String vendor = ((EditText) findViewById(R.id.txtvendor))
						.getText().toString();

				String datetime = ((EditText) findViewById(R.id.txtdatetime))
						.getText().toString();

				String gallons1 = ((EditText) findViewById(R.id.txtgallons1))
						.getText().toString();

				String amount1 = ((EditText) findViewById(R.id.txtamount1))
						.getText().toString();

				String trailerid = ((EditText) findViewById(R.id.txttrailerid))
						.getText().toString();

				String gallons2 = ((EditText) findViewById(R.id.txtgallons2))
						.getText().toString();

				String amount2 = ((EditText) findViewById(R.id.txtamount2))
						.getText().toString();

				validatefuel(vendor, selectedItem, datetime, gallons1, amount1,
						"photo", trailerid, gallons2, amount2);

				break;

			}

		}

	}

	private void validatefuel(String vendor, String state, String datetime,

	String gallons1, String amount, String photo,

	String trailerid, String gallons2, String amount2) {

		String errormsg = "";
		boolean complete = true;

		if (vendor == null || vendor.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.VENDOR + "\n";
		}
		if (intselectedItem == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.STATE + "\n";
		}
		if (datetime == null || datetime.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DATETIME + "\n";
		}
		if (intSelectedPodItem == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.POWERUNIT + "\n";
		}

		if (gallons1 == null || gallons1.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.GALLONS_PU + "\n";
		}

		if (amount == null || amount.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.AMOUNT_PU + "\n";
		}

		boolean istrailerenterted = false;

		if (trailerid.trim().length() > 0 || gallons2.trim().length() > 0
				|| amount2.trim().length() > 0) {

			// Second half

			istrailerenterted = true;
			if (trailerid == null || trailerid.trim().length() == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.TRAILER + "\n";
			}
			if (gallons2 == null || gallons2.trim().length() == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.GALLONS_TR + "\n";
			}

			if (amount2 == null || amount2.trim().length() == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.AMOUNT_TR + "\n";
			}

		}

		int amountAdded = 0;

		if (amount != null && amount.length() > 0) {
			if (!AppUtil.isFloatNumberValid(amount)) {
				amountAdded = 1;
				complete = false;
				errormsg = errormsg + AppConstants.INVALIDAMOUNT_PU + "\n";
			}
		}
		if (amount2 != null && amount2.length() > 0) {
			if (!AppUtil.isFloatNumberValid(amount2) && amountAdded == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.INVALIDAMOUNT_TR + "\n";
			}
		}

		int gallonsAdded = 0;
		if (gallons1 != null && gallons1.length() > 0) {
			if (!AppUtil.isFloatNumberValid(gallons1)) {
				gallonsAdded = 1;
				complete = false;
				errormsg = errormsg + AppConstants.INVALIDGALLONS_PU + "\n";
			}
		}
		if (gallons2 != null && gallons2.length() > 0) {
			if (!AppUtil.isFloatNumberValid(gallons2) && gallonsAdded == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.INVALIDGALLONS_TR + "\n";
			}
		}

		dbAdapter.open();

		if (dbAdapter.isFuelReceiptRequired()) {

			if (imageUri1 == null) {
				complete = false;
				errormsg = errormsg + AppConstants.PHOTO_PU + "\n";
			}
			if (istrailerenterted) {
				if (imageUri2 == null) {
					complete = false;
					errormsg = errormsg + AppConstants.PHOTO_TR + "\n";
				}
			}

		}
		dbAdapter.close();

		if (!complete) {
			UIUtils.showError(this, errormsg);
		}

		else {
			/* Get the status of the Current load */

			if (AppUtil.isInternetAvailable(this)) {

				try {
					jsonObjSend = new JSONObject();
					jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
					jsonObjSend.put("userid", udroveapp.getUserid());
					jsonObjSend.put("password", udroveapp.getPassword());
					jsonObjSend.put("date_on_reciept", datetime);
					jsonObjSend.put("vendor", vendor);
					jsonObjSend.put("state_id", intselectedItem);
					jsonObjSend.put("power_unit_id", intSelectedPodItem);
					jsonObjSend.put("pu_gallons", gallons1);
					jsonObjSend.put("pu_gallons_amount", amount);
					// jsonObjSend.put("pu_receipt_image", "No image");
					jsonObjSend.put("trailer_id", trailerid);
					jsonObjSend.put("tr_gallons", gallons2);
					jsonObjSend.put("tr_gallons_amount", amount2);
					// jsonObjSend.put("tr_receipt_image", "No image");
					jsonObjSend.put("pu_reimbursable",
							String.valueOf(puReimburs.isChecked()));
					jsonObjSend.put("tr_reimbursable",
							String.valueOf(trReimburs.isChecked()));

					if (imageUri1 != null) {
						uploadImage(imageUri1, "pu_receipt_image");
					} else {
						jsonObjSend.put("pu_receipt_image", "");
						index += 1;
					}
					
					if (index == 2) {
						makeServiceCall();
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				UIUtils.showError(this, AppConstants.INTERNET_CONNECTION_ERROR);
			}
		}

	}

	private void makeServiceCall() {

		UDroveService serv = new UDroveService(this, jsonObjSend, this);
		serv.execute(HTTPConstants.FUEL_RECEIPT_URL);
	}

	public void Startcamera(int i) {
		// TODO Auto-generated method stub

		Intent openActivity = new Intent();
		openActivity.setAction(Intent.ACTION_VIEW);
		openActivity
				.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.EditSignatureandExpenditureCameraActivity");
		openActivity.putExtra("intentcamefrom", "fuel");

		if (i == 0)
			openActivity.putExtra("title", "Truck-Fuel Receipts");
		else
			openActivity.putExtra("title", "Trailer-FuelReceipts");

		startActivity(openActivity);

	}

	@Override
	public void onResume() {
		super.onResume();
		if (udroveapp.getEditsignatureexpenditure() != null) {
			try {

				if (checkfromcamera) {

					AppUtil.setImagetoView1(
							udroveapp.getEditsignatureexpenditure(), image1,
							dbAdapter);
					imageUri1 = udroveapp.getEditsignatureexpenditure();

				} else {

					AppUtil.setImagetoView1(
							udroveapp.getEditsignatureexpenditure(), image2,
							dbAdapter);
					imageUri2 = udroveapp.getEditsignatureexpenditure();
				}
				udroveapp.setEditsignatureexpenditure(null);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private DateSlider.OnDateSetListener mDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute);
			String nowDateString = HTTPUtil.TodaysDateWithoutZone();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			Date date1;
			Date date2;
			try {
				date1 = df.parse(SelectedDateString);
				date2 = df.parse(nowDateString);
				if (date1.before(date2)) {
					edttxtdate.setText(SelectedDateString
							+ AppUtil.getTimeZoneString());
				} else {

					edttxtdate.setText(nowDateString
							+ AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				e.printStackTrace();
			}

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {
		final Calendar c = Calendar.getInstance();
		switch (id) {
		case DATE_DIALOG_ID:
			return new DateTimeSlider(this, mDateSetListener, c);
		}
		return null;
	}

	private void updateDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());

		this.edttxtdate.setText(formattedDate3 + AppUtil.getTimeZoneString());
	}

	public void uploadImage(String imageUri, String imageName) {
		try {

			dbAdapter.open();
			byte[] ba = dbAdapter.getImageFromGallery(imageUri);
			dbAdapter.close();

			JSONObject json = new JSONObject();
			json.put("userid", udroveapp.getUserid());
			json.put("password", udroveapp.getPassword());
			if (imageName == "pu_receipt_image") {
				json.put("category", "FRP");
				json.put("sub_category", 1);
			} else {
				json.put("category", "FRT");
				json.put("sub_category", 2);
			}
			json.put("file_ext", "jpg");
			json.put("bindata",
					uDroveBase64.encodeToString(ba, uDroveBase64.DEFAULT));
			ba = null;
			HttpServiceListener listener = new HttpServiceListener();

			UDroveService serv = new UDroveService(
					uDroveBusinessToolsFuelActivity.this, json, listener);
			serv.execute(HTTPConstants.IMAGE_UPLOAD, imageName);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete image" + posts);
		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {
				Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
				finish();
			}

		} else {

			UIUtils.showSubscriptionChangedMessage(this);
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	protected class HttpServiceListener implements UDroveServiceListener {

		/*
		 * protected void uploadImage(Bitmap bitmap){
		 * 
		 * }
		 */

		public void onServiceComplete(String posts) {

		}

		public void onServiceComplete(String posts, String reference) {

			/* Log.v(">>>>>>>>>>>", "onservice complete inner: fuel receipts"); */

			String result = HTTPUtil.ParseImageJson(posts);

			String split[] = result.split(",");
			/* Log.v(">>>>>>>>>>>>>", "result" + result); */
			if (split[0] != null && split[0].equals("FAIL")) {
				UIUtils.showServiceError(uDroveBusinessToolsFuelActivity.this,
						split[1]);
			} else {
				try {
					// String imageName =
					// split[2].substring(split[2].lastIndexOf("/") + 1);
					Log.v(">>>>>>>>>>", "IMAGENAME:" + split[2]);
					if (reference == "pu_receipt_image") {

						jsonObjSend.put("pu_receipt_image", split[2]);

					} else if (reference == "tr_receipt_image") {
						jsonObjSend.put("tr_receipt_image", split[2]);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			index++;
			if (index == 2) {
				makeServiceCall();
			}else{
				if (imageUri2 != null) {
					uploadImage(imageUri2, "tr_receipt_image");
				} else {
					try {
						jsonObjSend.put("tr_receipt_image", "");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					index += 1;
					makeServiceCall();

				}
			}

		}
	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		dbAdapter.open();
		dbAdapter.deleteGallery();
		dbAdapter.close();
		super.finish();
	}

	
}
