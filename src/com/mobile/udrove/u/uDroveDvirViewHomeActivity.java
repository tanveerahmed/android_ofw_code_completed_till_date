package com.mobile.udrove.u;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.db.uDroveDbAdapter;
import com.mobile.udrove.model.DVIRItemModel;
import com.mobile.udrove.model.DVIRModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles DVIR View Screen checkbox listing
 * 
 * @dated 16-Feb-2012
 */

public class uDroveDvirViewHomeActivity extends uDroveParentActivity implements
		UDroveServiceListener, OnCheckedChangeListener {

	/* uDrove Change Status Home class variables */

	uDroveDbAdapter db;
	uDroveApplication udroveapp;
	ArrayList<DVIRModel> arraylistdvirmodel;
	HashMap<Integer,DVIRItemModel> dvirInspecthashmap;
    private HashMap<Integer,DVIRModel> lastSevenDvirsHashMap;

	int[] dvirids=new int[7];
	TableLayout dynamicLayout;
	int count=0;
    uDroveDataBaseAdapter dbAdapter;

	/**
	 * {@inheritDoc}
	 */
	/*
	 * CheckBox checkbox1, checkbox2, checkbox3, checkbox4, checkbox5,
	 * checkbox6, checkbox7;
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvirviewhome);
		udroveapp = ((uDroveApplication) getApplication());
		 onclickListener listener = new onclickListener();
		 findViewById(R.id.sendEmailButton).setOnClickListener(listener);
		 findViewById(R.id.sendFaxButton).setOnClickListener(listener);
		dynamicLayout= (TableLayout) findViewById(R.id.dynamicbuttonslayout);
		dbAdapter=new uDroveDataBaseAdapter(this);
		 dbAdapter.open(); 
 		 Cursor cur;
 		try {
			cur = dbAdapter.getDriverDetails();
			if(cur!=null&&cur.getCount()>0){
		     
				System.out.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"+cur.getString(0)+cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
		    cur.close(); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 dbAdapter.close(); 
		
		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveDvirViewHomeActivity.this);
		//ReadDVIRRecords();
	}

	private void ReadDVIRRecordsFromServer() {
		// TODO Auto-generated method stub
		JSONObject jsonObjSend = new JSONObject();
		try {
			jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
			jsonObjSend.put("userid", udroveapp.getUserid());
			jsonObjSend.put("password", udroveapp.getPassword());

			UDroveService serv = new UDroveService(this, jsonObjSend, this);
			serv.execute(HTTPConstants.GET_VIEWDVIR_HOME_URL+ udroveapp.getUserid());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.putExtra("DocType","DVIR");

			switch (v.getId()) {
	
			case R.id.sendEmailButton:
			
				String docIdString=getSelectedDvirIds();
				if(docIdString.length()>0){
					openActivity.putExtra("DocIds",docIdString);
					openActivity.setClassName("com.mobile.udrove.u",
							"com.mobile.udrove.u.EmailActivity");
					startActivity(openActivity);

				}else{
					
					UIUtils.showError(uDroveDvirViewHomeActivity.this,AppConstants.ErrorCHECKBOXDVIR);

				}
				break;

			case R.id.sendFaxButton:
				
				String docIdString1=getSelectedDvirIds();
				if(docIdString1.length()>0){
					
					openActivity.putExtra("DocIds",docIdString1);
					openActivity.setClassName("com.mobile.udrove.u",
							"com.mobile.udrove.u.FaxActivity");
					startActivity(openActivity);
					
				}else{
					UIUtils.showError(uDroveDvirViewHomeActivity.this,AppConstants.ErrorCHECKBOXDVIR);
				}
				break;
			}

		}
	}

	public String getSelectedDvirIds(){
		
		StringBuilder selectedIds=new StringBuilder();
		
		for(int i=0;i<count;i++){
			CheckBox tempBox=(CheckBox)findViewById(dvirids[i]);
		
			if(tempBox.isChecked()){
				selectedIds.append(dvirids[i]+",");
			}
		}
		if(selectedIds.length()>0){
			return selectedIds.substring(0,selectedIds.length()-1);
		}
		else{
			return "";
		}
	}


	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete " + posts);

		String result = HTTPUtil.DVIRVIEWHOMEParseJson(posts);
		if (result != null && result.equals("FAIL")) {
			UIUtils.showServiceError(this, HTTPConstants.DVIR_VIEW_HOME_FAIL);
		} else {
			ParseJSonResponse(posts);
		}

	}

	private void ParseJSonResponse(String posts) {

		try {
			JSONObject jObject = new JSONObject(posts);

			JSONArray bodyarray = jObject.getJSONArray("body");

			if (bodyarray.length() > 0) {
		
				dynamicLayout.removeAllViews();
				lastSevenDvirsHashMap=new HashMap<Integer, DVIRModel>();
				arraylistdvirmodel = new  ArrayList<DVIRModel>();

				CheckBox cb;
				Button btn;
				View v;

				findViewById(R.id.sendEmailButton).setEnabled(true);
				findViewById(R.id.sendFaxButton).setEnabled(true);
				count=bodyarray.length();
                     
				for (int i = 0; i < bodyarray.length(); i++) { 

					TableRow tr = new TableRow(this);
					tr.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.FILL_PARENT,
							android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					tr.setGravity(Gravity.CENTER_HORIZONTAL);

					JSONObject childJSONObject = bodyarray.getJSONObject(i);
					String inspectionitems = childJSONObject
							.getString("inspection");
					System.out.println("The json inspection items is " + inspectionitems);
					JSONObject jsoninspect = new JSONObject(inspectionitems);

					DVIRModel dvirmodel = new DVIRModel();
					String id = jsoninspect.getString("id");
					String trailer = jsoninspect.getString("trailer");
					String odometer = jsoninspect.getString("odometer");
					String power_unit = jsoninspect.getString("power_unit");
					String power_unit_name = jsoninspect.getString("power_unit_name");
					String date_reported = jsoninspect.getString("date_reported");
					String mechanic_name = jsoninspect.getString("mechanic_name");
					String driver_name = jsoninspect.getString("driver_name");
					String mechanic_inspection_date = jsoninspect.getString("mechanic_inspection_date");
					String approved = jsoninspect.getString("approved");
					String defects_corrected = jsoninspect.getString("defects_corrected");
					String dvir_receipt_image_name = jsoninspect.getString("dvir_receipt_image_name");
                    String remarks1=jsoninspect.getString("remarks");
					
					
					dvirmodel.setDvirId(id);
					dvirmodel.setDvirDate(date_reported);
					dvirmodel.setTrailerId(trailer);
					dvirmodel.setPowerUnit(power_unit);
					dvirmodel.setPowerUnitName(power_unit_name);
					dvirmodel.setOdometer(odometer);
					dvirmodel.setMechanic_name(mechanic_name);
					dvirmodel.setDriver_name(driver_name);
					dvirmodel.setMechanic_inspection_date(mechanic_inspection_date);
					dvirmodel.setApproved(approved);
					dvirmodel.setDefects_corrected(defects_corrected);
					dvirmodel.setDvir_receipt_image_name(dvir_receipt_image_name);
					dvirmodel.setRemarks(remarks1);
					cb = new CheckBox(this);
					cb.setId(Integer.parseInt(id));
					dvirids[i]=Integer.parseInt(id);
					cb.setOnCheckedChangeListener(this);
					 v = new View(this);
					 v.setLayoutParams(new LayoutParams(15, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					btn = new Button(this);
					btn.setId(Integer.parseInt(id));
					btn.setText(date_reported);
					btn.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
					btn.setBackgroundResource(R.drawable.btn_image_color);
					btn.setTextColor(Color.rgb(255, 255, 255));
					btn.setTextSize(20);
					btn.setOnClickListener(new OnClickListener() {
						
						public void onClick(View v) {
							// TODO Auto-generated method stub
							
							System.out.println("Selected DVIR id is "+v.getId());
							Intent openActivity = new Intent();
							openActivity.setAction(Intent.ACTION_VIEW);
							openActivity.setAction(Intent.ACTION_VIEW);
							openActivity.setClassName("com.mobile.udrove.u","com.mobile.udrove.u.uDroveDvirViewNewActivity");
							openActivity.putExtra("dvirid", v.getId());
							startActivity(openActivity);
						}
					});
					
				
					JSONArray inspectionarray = jsoninspect.getJSONArray("inspection_items");
					
					dvirInspecthashmap= new HashMap<Integer, DVIRItemModel>();

					if (inspectionarray.length() > 0) {
						
						for (int ii = 0; ii < inspectionarray.length(); ii++) {

							JSONObject childinspectionitem = inspectionarray.getJSONObject(ii);
							
							String iid = childinspectionitem.getString("id");
							String inspection_item_image_name = childinspectionitem.getString("inspection_item_image_name");
							String remarks = childinspectionitem.getString("remarks");
							String inspection_item_type_id = childinspectionitem.getString("inspection_item_type_id");
							
							DVIRItemModel dvirItemModal=new DVIRItemModel();
							
							if(inspection_item_type_id.equalsIgnoreCase("null")){
								inspection_item_type_id="0";
							}
							dvirItemModal.setDefectId(Integer.parseInt(inspection_item_type_id));

							dvirItemModal.setImageUrl(inspection_item_image_name);
							dvirItemModal.setRemarks(remarks);
							System.out.println("The remarks are " + remarks);
							
						   	dvirInspecthashmap.put(Integer.parseInt(inspection_item_type_id), dvirItemModal);
						}
					}
				    
					dvirmodel.setHashmapdviritem(dvirInspecthashmap);
					
					dvirInspecthashmap=null;
					arraylistdvirmodel.add(dvirmodel);
					
					
					lastSevenDvirsHashMap.put(Integer.parseInt(id), dvirmodel);
	
					tr.addView(cb);
					tr.addView(v);
					tr.addView(btn);
					View v1 = new View(this);
					v1.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.FILL_PARENT, 5));

					dynamicLayout.addView(tr);
					dynamicLayout.addView(v1);
				}
				  udroveapp.setDVIRArrayListModel(arraylistdvirmodel);
				  udroveapp.setLastSevenDvirsHashMap(lastSevenDvirsHashMap);
			}

			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}

	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
           
		if(uDroveApplication.updateDvirsFromServer){
			uDroveApplication.updateDvirsFromServer=false;
			ReadDVIRRecordsFromServer();
			
		}
		
	}

	
	
}