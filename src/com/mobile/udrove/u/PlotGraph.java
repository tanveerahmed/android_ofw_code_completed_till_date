package com.mobile.udrove.u;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

class PlotGraph extends ImageView {

	List<int[]> gList = new ArrayList<int[]>();

	String totalOffHours = "00:00", totalOnHours = "00:00",
			totalSbHours = "00:00", totalDrHours = "00:00";

	double screenDensityFactor = 1;

	int X1 = 140;
	int Y1 = 177;
	int X2 = 340;
	int Y2 = 204;
	double strokewidth = 2.5;
	double font = 14;
	Paint mpaint = new Paint();;

	public PlotGraph(Context context) {
		super(context);

		setScaleType(ImageView.ScaleType.CENTER);

	}

	public PlotGraph(Context context, AttributeSet str) {
		super(context, str);

	}


	@Override
	public void onDraw(Canvas canvas) {

		if (gList.size() > 0) {
			
			mpaint.setAntiAlias(true);
			mpaint.setStrokeWidth((float) (strokewidth * screenDensityFactor));
			mpaint.setColor(Color.RED);


			float startX, startY, stopX, stopY;

			for (int k = 0; k < gList.size() - 1; k++) {

				int[] tep = gList.get(k);
				startX = tep[0];
				startY = tep[1];

				if (k != gList.size() - 1) {
					tep = gList.get(k + 1);
					stopX = tep[0];
					stopY = tep[1];
				} else {
					stopX = 0;
					stopY = 0;

				}
				canvas.drawLine(startX, startY, stopX, stopY, mpaint);

			}

			mpaint.setColor(Color.BLACK);
			mpaint.setTextSize((float) (font * screenDensityFactor));

			canvas.drawText(totalOffHours, (float) (X1 * screenDensityFactor),
					(float) (Y1 * screenDensityFactor), mpaint);
			canvas.drawText(totalDrHours, (float) (X2 * screenDensityFactor),
					(float) (Y1 * screenDensityFactor), mpaint);
			canvas.drawText(totalSbHours, (float) (X1 * screenDensityFactor),
					(float) (Y2 * screenDensityFactor), mpaint);
			canvas.drawText(totalOnHours, (float) (X2 * screenDensityFactor),
					(float) (Y2 * screenDensityFactor), mpaint);

			invalidate();

		}

	}

}
