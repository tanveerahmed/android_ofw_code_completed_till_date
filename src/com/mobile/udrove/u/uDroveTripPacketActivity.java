package com.mobile.udrove.u;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class uDroveTripPacketActivity extends uDroveParentActivity implements
		UDroveServiceListener {

	EditText edttxtfromdate, edttxttodate;
	private int mYear;
	private int mMonth;
	private int mDay;
	static final int DATE_DIALOG_ID = 0, DATE_DIALOG_IDS = 1;
	CheckBox checkbox1, checkbox2, checkbox3, checkbox4, checkbox5;
	uDroveApplication udroveapp;
	uDroveDataBaseAdapter dbAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trippacket);
		udroveapp = ((uDroveApplication) getApplication());

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveTripPacketActivity.this);

		 dbAdapter=new uDroveDataBaseAdapter(this);
		 dbAdapter.open(); 
 		 Cursor cur;
 		try {
			cur = dbAdapter.getDriverDetails();
			if(cur!=null&&cur.getCount()>0){
		     
				System.out.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"+cur.getString(0)+cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
		    cur.close(); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 dbAdapter.close(); 

		
		checkbox1 = (CheckBox) findViewById(R.id.checkBox1);

		checkbox2 = (CheckBox) findViewById(R.id.checkBox2);

		checkbox3 = (CheckBox) findViewById(R.id.checkBox3);

		checkbox4 = (CheckBox) findViewById(R.id.checkBox4);

		checkbox5 = (CheckBox) findViewById(R.id.checkBox5);

		String[] serviceids = udroveapp.getServiceIds().split(",");
		for (int i = 0; i < serviceids.length; i++) {

			switch (Integer.parseInt(serviceids[i])) {

			case 1:
				checkbox1.setEnabled(true);
				break;
			case 2:
				checkbox2.setEnabled(true);
				break;
			case 4:
				checkbox4.setEnabled(true);
				break;
			case 5:
				checkbox3.setEnabled(true);
				break;
			case 6:
				checkbox5.setEnabled(true);
				break;
			}
		}

		edttxtfromdate = (EditText) findViewById(R.id.txtfromdate);
		edttxttodate = (EditText) findViewById(R.id.txttodate);

		// display the current date
		updateFromDisplay();
		updateToDisplay();

		onclickListener listener = new onclickListener();
		findViewById(R.id.tripsendButton).setOnClickListener(listener);

		edttxtfromdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_ID);
			}
		});

		edttxttodate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATE_DIALOG_IDS);
			}
		});

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.tripsendButton:
				String fromdate = ((EditText) findViewById(R.id.txtfromdate))
						.getText().toString();
				String todate = ((EditText) findViewById(R.id.txttodate))
						.getText().toString();
				String email = ((EditText) findViewById(R.id.txtemail))
						.getText().toString();
				validatetrip(fromdate, todate, email);

				break;

			}

		}
	}

	private void validatetrip(String fromdate, String todate, String email) {

		String errormsg = "";
		boolean complete = true, ckcomplete = false;
		if (fromdate == null || fromdate.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.FROMDATE + "\n";
		}
		if (todate == null || todate.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.TODATE + "\n";
		}
		if (email == null || email.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.EMAIL + "\n";
		}
		if (email != null && email.length() > 0) {
			String[] temp = email.split(",");
			for (int i = 0; i < temp.length; i++) {
				System.out.println("emailId at  " + i + " is  " + temp[i]);
				if (temp[i].length() > 0) {
					if (!AppUtil.isEmailValid(temp[i].trim())) {
						System.out.println("emailId invalid is  " + temp[i]);
						complete = false;
						errormsg = errormsg + AppConstants.INVALIDEMAIL + "\n";
						break;
					}
				}
			}
		}
        
		if (checkbox1.isChecked()) {

			ckcomplete = true;
		} else if (checkbox2.isChecked()) {
			ckcomplete = true;
		} else if (checkbox3.isChecked()) {
			ckcomplete = true;
		} else if (checkbox4.isChecked()) {
			ckcomplete = true;
		} else if (checkbox5.isChecked()) {
			ckcomplete = true;
		}

		if (!ckcomplete) {
			complete = false;
			errormsg = errormsg + AppConstants.TRIP_PACKET_NO_CHECKBOX + "\n";
		}
		
		 if(complete){
				if (!validateDatesRange(fromdate, todate)) {
					complete = false;
					if(checkbox1.isChecked()==false&&checkbox2.isChecked()==false&&checkbox3.isChecked()==false&&
							checkbox4.isChecked()==true&&checkbox5.isChecked()==false){					
					errormsg = errormsg + "Max date range of 1-year allowed" + "\n";
				}else{
					
					errormsg = errormsg + "Max date range of 1-week allowed" + "\n";

				}
		
	         }
		 }
		
		if (!complete) {
			UIUtils.showError(this, errormsg);
		}

		else {
			/* Get the status of the Current load */

			StringBuilder result = new StringBuilder();

			if (checkbox1.isChecked()) {
				result.append("1,");
			}
			if (checkbox2.isChecked()) {
				result.append("2,");
			}
			if (checkbox3.isChecked()) {
				result.append("5,");
			}
			if (checkbox4.isChecked()) {
				result.append("6,");
			}
			if (checkbox5.isChecked()) {
				result.append("4,");
			}

			if (result.length() != 0) {
				String serviceids = result.substring(0, result.length() - 1);
				System.out.println(serviceids);

				JSONObject json = new JSONObject();
				try {

					json.put("time_stamp", HTTPUtil.TodaysDate());
					json.put("userid", udroveapp.getUserid());
					json.put("password", udroveapp.getPassword());
					json.put("from_date", fromdate);
					json.put("to_date", todate);
					json.put("email_to",email);
					json.put("service_ids", serviceids);
					UDroveService serv = new UDroveService(this, json, this);
					serv.execute(HTTPConstants.SEND_TRIP_PACKET);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			/*
			 * UIUtils.ShowInfoandNavigates(this, "Trip Packet is sent",
			 * "com.mobile.udrove.u.uDroveHomeActivity");
			 */

		}

	}

	public boolean validateDatesRange(String fromDate, String toDate) {

		SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy");
		Date date1;
		Date date2;
		try {
			date1 = df.parse(fromDate);
			date2 = df.parse(toDate);
			long diff = date2.getTime() - date1.getTime();
			System.out.println("Number of Days difference between  " + date1
					+ "  and " + date2 + "  is  " + diff
					/ (24 * 60 * 60 * 1000));
			
			if(checkbox1.isChecked()==false&&checkbox2.isChecked()==false&&checkbox3.isChecked()==false&&
					checkbox4.isChecked()==true&&checkbox5.isChecked()==false){
				
				if (diff / (24 * 60 * 60 * 1000) >= 0
						&& diff / (24 * 60 * 60 * 1000) < 365) {
					return true;
				} else {
					return false;
				}

			}else{
			
			if (diff / (24 * 60 * 60 * 1000) >= 0
					&& diff / (24 * 60 * 60 * 1000) < 7) {
				return true;
			} else {
				return false;
			}
			
		}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub
		System.out.println("the onService Complete image" + posts);
		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {
				Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
				Intent openActivity = new Intent();
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveHomeActivity");
				this.startActivity(openActivity);
				// this.overridePendingTransition(android.R.anim.slide_out_right,0);
				finish();
			}
		} else {

			UIUtils.showSubscriptionChangedMessage(this);
		}

	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	private DateSlider.OnDateSetListener mFromDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
		
			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT_ONLY, selectedDate,
					selectedDate, selectedDate);
			String nowDateString = HTTPUtil.TodayDateOnly();
			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_ONLY_FORMAT);

			Calendar c = Calendar.getInstance();
			c.roll(Calendar.DATE, -7);
			Date minDate = c.getTime();
			String minDateString = df.format(minDate);

			Date selectedDateTime;
			Date maxDate;
			try {
				selectedDateTime = df.parse(SelectedDateString);
				maxDate = df.parse(nowDateString);
				if (selectedDateTime.before(maxDate)) {
					edttxtfromdate.setText(SelectedDateString + AppUtil.getTimeZoneString());
				} else {

					edttxtfromdate.setText(nowDateString + AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};

	private DateSlider.OnDateSetListener mToDateSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
		
			String SelectedDateString = String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT_ONLY, selectedDate,
					selectedDate, selectedDate);
			String nowDateString = HTTPUtil.TodayDateOnly();

			SimpleDateFormat df = new SimpleDateFormat(AppConstants.DATE_ONLY_FORMAT);

			Calendar c = Calendar.getInstance();
			c.roll(Calendar.DATE, -7);
			Date minDate = c.getTime();
		
			String minDateString = df.format(minDate);

			Date selectedDateTime;
			Date maxDate;
			try {
				selectedDateTime = df.parse(SelectedDateString);
				maxDate = df.parse(nowDateString);
				if (selectedDateTime.before(maxDate)) {
					edttxttodate.setText(SelectedDateString + AppUtil.getTimeZoneString());
				} else {

					edttxttodate.setText(nowDateString + AppUtil.getTimeZoneString());
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	};

	@Override
	protected Dialog onCreateDialog(int id) {
		final Calendar c = Calendar.getInstance();
		switch (id) {
		case DATE_DIALOG_ID:
			return new DateTimeSlider(this, mFromDateSetListener, c);

		case DATE_DIALOG_IDS:
			return new DateTimeSlider(this, mToDateSetListener, c);

		}
		return null;
	}

	private void updateFromDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_ONLY_FORMAT);
		String formattedDate3 = df3.format(c.getTime());
		this.edttxtfromdate.setText(formattedDate3
				+ AppUtil.getTimeZoneString());
		
	}

	private void updateToDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_ONLY_FORMAT);
		String formattedDate3 = df3.format(c.getTime());
		this.edttxttodate.setText(formattedDate3 + AppUtil.getTimeZoneString());

	}

}