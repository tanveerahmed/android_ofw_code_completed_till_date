package com.mobile.udrove.u;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers to on,off,sleeper,and driving
 * 
 * @dated 16-Feb-2012
 */

public class SettingsActivity extends uDroveParentActivity{

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);

		onclickListener listener = new onclickListener();
		findViewById(R.id.listofexemptions).setOnClickListener(listener);
		findViewById(R.id.editsignature_button).setOnClickListener(listener);
		findViewById(R.id.troubleticket_button).setOnClickListener(listener);
		findViewById(R.id.phonesettings_button).setOnClickListener(listener);

		Header header = (Header) findViewById(R.id.common_header_button);
	    header.initHeader(SettingsActivity.this);
	    
	    findViewById(R.id.settings_header_button).setBackgroundResource(R.drawable.settings_normal);
	    findViewById(R.id.settings_header_button).setFocusable(false);
		

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			
			switch (v.getId()) {
			case R.id.listofexemptions:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveExemptionList");
				openActivity.putExtra("intentcamefrom", "exemptions");
				startActivity(openActivity);
				break;
			case R.id.editsignature_button:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.EditSignatureandExpenditureCameraActivity");
				openActivity.putExtra("intentcamefrom", "signature");
				startActivity(openActivity);
				break;
			case R.id.troubleticket_button:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.TroubleTicketActivity");
				startActivity(openActivity);

				break;
			case R.id.phonesettings_button:
				
				Intent languageIntent = new Intent(Intent.ACTION_MAIN);
				languageIntent.setClassName("com.android.settings", "com.android.settings.Settings");
				startActivity(languageIntent);
				
			   break;

		
			}

		}

	}

}