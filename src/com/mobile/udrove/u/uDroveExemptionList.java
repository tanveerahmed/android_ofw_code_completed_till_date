package com.mobile.udrove.u;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;


public class uDroveExemptionList extends Activity implements
UDroveServiceListener{

	
	uDroveApplication udroveapp;
	TableLayout exempt_tbl_layout;
	TableRow exempt_tbl_row;
	LinearLayout exempt_linearbar;
	int tot_exemptions_support=13;
	int row_count = 0;
	uDroveDataBaseAdapter dbAdapter;
	MetaDataModel ExemptionModal;
	int[] ExemptionIds;
	String[] ExemptionNames;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exemptionlist);
		
		udroveapp = ((uDroveApplication) getApplication());
		dbAdapter=new uDroveDataBaseAdapter(this);
	    dbAdapter.open();

        ExemptionModal=dbAdapter.getExemptions();
		onclickListener listener = new onclickListener();
		
		findViewById(R.id.back_header_button).setOnClickListener(listener);
		
		exempt_tbl_layout=(TableLayout) findViewById(R.id.tblLayoutExemption);
		
		initiateExemptionList();
		getCurrentExemptions();
			
	}
	
	public void initiateExemptionList()
	{
		//Sets all the views to Invisible.
		
		TableRow tr = (TableRow) findViewById(R.id.tr_exemption1);
		tr.setVisibility(View.GONE);
		LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar1);
		ll.setVisibility(View.GONE);
		

		tr = (TableRow) findViewById(R.id.tr_exemption2);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar2);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption3);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar3);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption4);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar4);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption5);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar5);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption6);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar6);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption7);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar7);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption8);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar8);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption9);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar9);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption10);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar10);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption11);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar11);
		ll.setVisibility(View.GONE);

		tr = (TableRow) findViewById(R.id.tr_exemption12);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar12);
		ll.setVisibility(View.GONE);
		
		tr = (TableRow) findViewById(R.id.tr_exemption13);
		tr.setVisibility(View.GONE);
		ll = (LinearLayout)findViewById(R.id.linearbar13);
		ll.setVisibility(View.GONE);
	}
	
	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.back_header_button:
				finish();
				break;
			}

		}

	}

	public void getCurrentExemptions()
	{
		HashMap<String, String> data;
		data = new HashMap<String, String>();
		data.put("userid", udroveapp.getUserid());
		data.put("password", udroveapp.getPassword());
		
		// data.put("userid", "t_driver");
		// data.put("password", "driver123");
		System.out.println("Posted string is:"+data.toString());
		
		UDroveService serv = new UDroveService(this, data, this);
		serv.execute(HTTPConstants.GET_USER_EXEMPTIONS);
	}
	
	 public void ShowAlert(String Message)
	 {
			AlertDialog.Builder builder = new AlertDialog.Builder(uDroveExemptionList.this);
			builder.setMessage(Message).setCancelable(false).setPositiveButton("OK", null).setTitle(
					"uDrove Mobile").setIcon(R.drawable.icon_info);
			AlertDialog alert = builder.create();
			alert.show();
	 }
	
	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete " + posts);
		String result = HTTPUtil.ParseLoginJson(posts);
		System.out.println("IN uDroveExemptionList" + result);
		String spilt[] = result.split(",");
		
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			ShowAlert(spilt[1]);
		}
		else
		{   
			JSONObject jObject;
			try {
			jObject = new JSONObject(posts);
		
			JSONObject bodyObject = jObject.getJSONObject("body");
			JSONArray JArray = bodyObject.getJSONArray("exemption_ids");

	        if(ExemptionModal!=null){
	        	ExemptionIds=ExemptionModal.getIdList();
	        	ExemptionNames=ExemptionModal.getNameList();
	        }
	        
			create_master_exemption_list(ExemptionIds.length);
			create_user_exemption_list(JArray.length());
			
			for(int i=0; i < JArray.length(); i++)
			{
				switch(Integer.parseInt(JArray.getString(i)))
				{
				case 1:
				//Server responds with Status-ID 1 to indicate OIL_FIELD_EXEMPTION. 
				{
					System.out.println("In OFW Exemption listing");
					//For Showing / Hiding OFW Button
					udroveapp.setDriverType(AppConstants.OIL_FIELD_EXEMPTION);
					
					//For maintaining list of exemptions user has.
					udroveapp.setExemption(AppConstants.OIL_FIELD_EXEMPTION);
					setExemptionStatusOnUI(AppConstants.OIL_FIELD_EXEMPTION);
				}
				break;
				
				case 2:
				//Server responds with Status-ID 2 to indicate SAMPLE1_EXEMPTION.
				// Incase real Exemptions are introduced change the def'n of SAMPLE1_EXEMPTION
				{
					udroveapp.setDriverType(AppConstants.SAMPLE1_EXEMPTION);
					udroveapp.setExemption(AppConstants.SAMPLE1_EXEMPTION);
					setExemptionStatusOnUI(AppConstants.SAMPLE1_EXEMPTION);
				}
				break;
				//Server responds with Status-ID 3 to indicate SAMPLE2_EXEMPTION. 
				// Incase real Exemptions are introduced change the def'n of SAMPLE2_EXEMPTION
				case 3:
				{
					udroveapp.setDriverType(AppConstants.SAMPLE2_EXEMPTION);
					udroveapp.setExemption(AppConstants.SAMPLE2_EXEMPTION);
					setExemptionStatusOnUI(AppConstants.SAMPLE2_EXEMPTION);
				}
				break;
				
				default: 
					break;
				}
			}
		}
			catch (JSONException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}			
	
	public void setExemptionStatusOnUI(int rowIndex)
	{
		Button btn;
		
		switch(rowIndex){
		case 1:
			btn = (Button) findViewById(R.id.tbtn_exemption1);
			btn.setSelected(true);
			break;
		case 2:
			btn = (Button) findViewById(R.id.tbtn_exemption2);
			btn.setSelected(true);
			break;
		case 3:
			btn = (Button) findViewById(R.id.tbtn_exemption3);
			btn.setSelected(true);
			break;
		case 4:
			btn = (Button) findViewById(R.id.tbtn_exemption4);
			btn.setSelected(true);
			break;
		case 5:
			btn = (Button) findViewById(R.id.tbtn_exemption5);
			btn.setSelected(true);
			break;
		}
	}
	
	public void create_user_exemption_list(int no_of_exemptions){
		System.out.println("In create_user_exemption_list,no is: "+no_of_exemptions);
		udroveapp.user_exempt_list = new ArrayList<String>(no_of_exemptions);
	}

	public void create_master_exemption_list(int index)
	{
		for(int i=1; i<=index; i++){
			switch(i){
			case 1:
			{
				TableRow tr = (TableRow) findViewById(R.id.tr_exemption1);
				TextView tv = (TextView) findViewById(R.id.tv_exemption1);
				LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar1);
				tv.setText(i +". "+ExemptionNames[i-1]);
				tr.setVisibility(View.VISIBLE);
				ll.setVisibility(View.VISIBLE);
				
				break;
			}
			
			case 2:
			{
				TableRow tr = (TableRow) findViewById(R.id.tr_exemption2);
				TextView tv = (TextView) findViewById(R.id.tv_exemption2);
				LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar2);
				tv.setText(i+". "+ExemptionNames[i-1]);
				tr.setVisibility(View.VISIBLE);
				ll.setVisibility(View.VISIBLE);
				break;
			}
			
			case 3:
			{
				TableRow tr = (TableRow) findViewById(R.id.tr_exemption3);
				TextView tv = (TextView) findViewById(R.id.tv_exemption3);
				LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar3);
				tv.setText(i+". "+ExemptionNames[i-1]);
				tr.setVisibility(View.VISIBLE);
				ll.setVisibility(View.VISIBLE);
				break;
			}
			case 4:
			{
				TableRow tr = (TableRow) findViewById(R.id.tr_exemption4);
				TextView tv = (TextView) findViewById(R.id.tv_exemption4);
				LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar4);
				tv.setText(i+". "+ExemptionNames[i-1]);
				tr.setVisibility(View.VISIBLE);
				ll.setVisibility(View.VISIBLE);
				break;
			}
			case 5:
			{
				TableRow tr = (TableRow) findViewById(R.id.tr_exemption5);
				TextView tv = (TextView) findViewById(R.id.tv_exemption5);
				LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar5);
				tv.setText(i+". "+ExemptionNames[i-1]);
				tr.setVisibility(View.VISIBLE);
				ll.setVisibility(View.VISIBLE);
				break;
			}
			case 6:
			{
				TableRow tr = (TableRow) findViewById(R.id.tr_exemption6);
				TextView tv = (TextView) findViewById(R.id.tv_exemption6);
				LinearLayout ll = (LinearLayout)findViewById(R.id.linearbar6);
				tv.setText(i+". "+ExemptionNames[i-1]);
				tr.setVisibility(View.VISIBLE);
				ll.setVisibility(View.VISIBLE);
				break;
			}
			default:
			{
				break;	
			}
		  }
  	   }
	}
	
	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}
}
