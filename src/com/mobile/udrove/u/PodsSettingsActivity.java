package com.mobile.udrove.u;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.model.PODModel;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers to on,off,sleeper,and driving
 * 
 * @dated 16-Feb-2012
 */

public class PodsSettingsActivity extends uDroveParentActivity {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	private static final int CAMERA_PIC_REQUEST = 1337;
	String imageUri = null;
	TextView lbltxtheader;
	ImageView image;
	private String selectedItem;
	int intselectedItem = 0;
	uDroveApplication udroveapp;
	ArrayList<PODModel> arraylistpodmodel;
	Button saveBtn;

	String[] selectedPodTypes;
	int[] selectedPodIds;

	ArrayList<String> podsList;
	ArrayList<Integer> podsListIds;

	uDroveDataBaseAdapter dbAdapter;

	int[] podIds;
	String[] podNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.podsettings);

		udroveapp = ((uDroveApplication) getApplication());

		uDroveApplication.fromBackButton = false;

		onclickListener listener = new onclickListener();
		saveBtn = (Button) findViewById(R.id.saveButton);

		findViewById(R.id.saveButton).setOnClickListener(listener);
		findViewById(R.id.back_header_button).setOnClickListener(listener);
		findViewById(R.id.camera_header_button).setOnClickListener(listener);
		image = (ImageView) findViewById(R.id.imageview);
		arraylistpodmodel = new ArrayList<PODModel>();

		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		MetaDataModel podModal = dbAdapter.getPodTypes();

		dbAdapter.close();

		if (podModal != null) {
			podIds = podModal.getIdList();
			podNames = podModal.getNameList();
		}

		final HashMap<String, Integer> podData = new HashMap<String, Integer>();
		for (int k = 0; k < podIds.length; k++) {

			podData.put(podNames[k], podIds[k]);

		}

		selectedPodTypes = new String[uDroveApplication.arraylistpodmodel
				.size()];
		selectedPodIds = new int[uDroveApplication.arraylistpodmodel.size()];

		int i = 0;
		for (i = 0; i < uDroveApplication.arraylistpodmodel.size(); i++) {

			selectedPodTypes[i] = uDroveApplication.arraylistpodmodel.get(i)
					.getPodString();
			selectedPodIds[i] = uDroveApplication.arraylistpodmodel.get(i)
					.getPodTypeId();

			System.out.println("Selected pods ids are " + selectedPodTypes[i]);
		}

		podsList = new ArrayList<String>();
		podsListIds = new ArrayList<Integer>();

		for (i = 0; i < podNames.length; i++) {
			podsList.add(podNames[i]);
			podsListIds.add(podIds[i]);

		}

		for (int j = 0; j < selectedPodTypes.length; j++) {

			try {
				podsList.remove(selectedPodTypes[j]);
				podsListIds.remove(selectedPodIds[j]);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		Spinner spinnerpods = (Spinner) findViewById(R.id.dropdownpod);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, podsList);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerpods.setFocusable(true);
		spinnerpods.setFocusableInTouchMode(true);
		spinnerpods.setAdapter(adapter);

		spinnerpods.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				selectedItem = podsList.get(arg2);
				intselectedItem = podData.get(selectedItem);
				if (imageUri != null) {
					saveBtn.setEnabled(true);
					saveBtn.setTextColor(Color.WHITE);
				}
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			String errormsg = "";
			boolean complete = true;
			switch (v.getId()) {

			case R.id.saveButton:

				if (imageUri == null) {
					complete = false;
					errormsg += AppConstants.PHOTO + "\n";
				}
				if (intselectedItem == 0) {
					complete = false;
					errormsg += AppConstants.CATEGORY + "\n";

				}
				if (!complete) {

					UIUtils.showError(PodsSettingsActivity.this, errormsg);

				} else {
					PODModel pod = new PODModel();
					pod.setImageuri(imageUri);
					pod.setPodString(selectedItem);
					pod.setPodtype(selectedItem + ","
							+ String.valueOf(intselectedItem));
					arraylistpodmodel.add(pod);
					uDroveApplication.arraylistpodmodel.add(pod);
					finish();
				}
				break;

			case R.id.back_header_button:
				uDroveApplication.fromBackButton = true;
				finish();
				break;

			case R.id.camera_header_button:
				StartCamera();
				break;
			}

		}

	}

	int lastImageId = -1;

	private void StartCamera() {
	/*	// start camera

		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		
		 * if (uDroveApplication.galleryImageCounter <= 0) {
		 * 
		 * lastImageId = getLastImageID();
		 * 
		 * }
		 
		ContentValues values = new ContentValues();
		values.put(MediaColumns.TITLE, "New Picture");
		values.put(ImageColumns.DESCRIPTION, "From your Camera");
		imageUri = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(intent, CAMERA_PIC_REQUEST);*/
		
        System.gc();
		Intent intent = new Intent(this, CameraControlNew.class);
		startActivityForResult(intent, CAMERA_PIC_REQUEST);


	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			switch (requestCode) {

			case CAMERA_PIC_REQUEST:
			
				Bundle bdl = data.getExtras();
				imageUri = bdl.getString("filename");
				AppUtil.setImagetoView1(imageUri,image,dbAdapter);
				
				if (intselectedItem != 0) {
					saveBtn.setEnabled(true);
					saveBtn.setTextColor(Color.WHITE);
				}

				break;
			}
		}
		else if(resultCode == RESULT_CANCELED)
		{
			
			imageUri = null;
	   	}
	}

	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}