package com.mobile.udrove.u;


import com.mobile.udrove.db.uDroveDataBaseAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class Header extends RelativeLayout {
	
	public static final String TAG = Header.class.getSimpleName();

	private Button homeButton;
	private Context activitycontext;
	private Activity parentactivity;

	
	public Header(Context context) {
		super(context);
		activitycontext = context;
	}

	public Header(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Header(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void initHeader(Activity activity) {
		parentactivity = activity;
		inflateHeader();
	}

	private void inflateHeader() {
		LayoutInflater inflater = (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.common_header, this);

		onclickListener listener = new onclickListener();
		findViewById(R.id.home_header_button).setOnClickListener(listener);
		findViewById(R.id.settings_header_button).setOnClickListener(listener);

		findViewById(R.id.back_header_button).setOnClickListener(listener);
		findViewById(R.id.logout_header_button).setOnClickListener(listener);

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {

			case R.id.settings_header_button:
				if (parentactivity instanceof SettingsActivity == false) {
					openActivity.setAction(Intent.ACTION_VIEW);
					openActivity.setClassName("com.mobile.udrove.u",
							"com.mobile.udrove.u.SettingsActivity");

					parentactivity.startActivity(openActivity);
				} else {
					System.out.println("I am in Setting screen");
				}
				break;

			case R.id.logout_header_button:
				
				   System.out.println("LOGINTAG:logout button clicked ");

			   uDroveDataBaseAdapter dbAdapter=new uDroveDataBaseAdapter(parentactivity.getBaseContext());
				    dbAdapter.open();
				    dbAdapter.clearAllTabelsRecords();
				    dbAdapter.close();  
				openActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveSignInActivity");
				parentactivity.startActivity(openActivity);
				parentactivity.finish();
				break;

			case R.id.home_header_button:
			
				if (parentactivity instanceof uDroveHomeActivity == false) {
					openActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

					openActivity.setClassName("com.mobile.udrove.u",
							"com.mobile.udrove.u.uDroveHomeActivity");
					parentactivity.startActivity(openActivity);
					parentactivity.finish();
				}
				
				break;
			case R.id.back_header_button:
				parentactivity.finish();
				break;

			}
		}
	}
	

}