package com.mobile.udrove.u;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.db.uDroveDbAdapter;
import com.mobile.udrove.service.UDrovePersonalUseService;
import com.mobile.udrove.service.UDrovePersonalUseServiceListener;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the Load Management home of the uDrove Mobile
 *              apps
 * 
 * @dated 16-Feb-2012
 */

public class uDroveLoadManagementHomeActivity extends uDroveParentActivity
		implements UDrovePersonalUseServiceListener {

	/* uDrove Business Tools Home class variables */

	/**
	 * {@inheritDoc}
	 */
	Button loadbutton;
	uDroveDataBaseAdapter dbAdapter;
	uDroveDbAdapter db;
	uDroveApplication udroveapp;
	String loadId;
	TableRow tr;
	View v;
	TableLayout ll;

	onclickListener listener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.loadmanagmenthome);
		ll = (TableLayout) findViewById(R.id.dynamicbuttonslayout);
		dbAdapter = new uDroveDataBaseAdapter(this);
		udroveapp = ((uDroveApplication) getApplication());
		listener = new onclickListener();

		findViewById(R.id.currentorcreateloadButton).setOnClickListener(
				listener);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveLoadManagementHomeActivity.this);

		loadbutton = (Button) findViewById(R.id.currentorcreateloadButton);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		System.out.println("Load Management On Resume Start");
		ll.removeAllViews();
		dbAdapter.open();
		if (dbAdapter.isCurrentLoadCompleted()) {
			loadbutton.setText("Create Load");
		} else {
			loadbutton.setText("Current Load");
		}

		Cursor loadDates = dbAdapter.getLastSevenLoadDates();
		dbAdapter.close();

		if (loadDates != null && loadDates.getCount() > 0) {
			loadId = null;
			do {

				final TableLayout ll = (TableLayout) findViewById(R.id.dynamicbuttonslayout);
				Button btn;
				tr = new TableRow(this);
				tr.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.FILL_PARENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				tr.setGravity(Gravity.CENTER_HORIZONTAL);
				btn = new Button(this);
				btn.setText(loadDates.getString(0));
				loadId = loadDates.getString(0);
				btn.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				btn.setBackgroundResource(R.drawable.btn_image_color);
				btn.setTextColor(Color.rgb(255, 255, 255));
				btn.setTextSize(20);
				tr.addView(btn);
				v = new View(this);
				v.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.FILL_PARENT, 10));

				btn.setOnClickListener(listener);

				/*
				 * btn.setOnClickListener(new OnClickListener() {
				 * 
				 * public void onClick(View v) { // TODO Auto-generated method
				 * stub System.out.println("I Clicked on the Button"); Intent
				 * openActivity = new Intent();
				 * openActivity.setAction(Intent.ACTION_VIEW);
				 * openActivity.setClassName("com.mobile.udrove.u",
				 * "com.mobile.udrove.u.uDroveCompletedLoadActivity");
				 * openActivity
				 * .putExtra("load_end_date",((Button)v).getText().toString());
				 * startActivity(openActivity); } });
				 */
				ll.addView(tr);
				ll.addView(v);

			} while (loadDates.moveToNext());
			loadDates.close();

		}

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.currentorcreateloadButton:

				new UDrovePersonalUseService(
						uDroveLoadManagementHomeActivity.this,
						uDroveLoadManagementHomeActivity.this, udroveapp)
						.execute();

				/*
				 * Intent openActivity = new Intent();
				 * openActivity.setAction(Intent.ACTION_VIEW); if
				 * (loadbutton.getText().toString().equals("Create Load")) {
				 * openActivity.setClassName("com.mobile.udrove.u",
				 * "com.mobile.udrove.u.uDroveCreateLoadActivity"); } else {
				 * openActivity.setClassName("com.mobile.udrove.u",
				 * "com.mobile.udrove.u.uDroveCurrentLoadActivity"); }
				 * startActivity(openActivity);
				 */

				break;

			default:

				Intent openActivity1 = new Intent();
				openActivity1.setAction(Intent.ACTION_VIEW);
				openActivity1.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveCompletedLoadActivity");
				openActivity1.putExtra("load_end_date", ((Button) v).getText()
						.toString());
				startActivity(openActivity1);

				break;
			}

		}

	}

	public void onPersonalUseStatus(boolean result, boolean personaluseStatus,
			String message) {
		// TODO Auto-generated method stub

		if (result) {

			dbAdapter.open();
			dbAdapter.updatePersonalConveyance(String
					.valueOf(personaluseStatus));
			dbAdapter.close();

			if (personaluseStatus) {

				UIUtils.showPersonalUseFlagEnabledPromptFinish(this,
						AppConstants.TASK_TERMINATED_AS_PC_ON);

				/*
				 * navigate to homescreen
				 */

				// AppUtil.navigateToHomeScreen(this);

			} else {

				Intent openActivity = new Intent();
				openActivity.setAction(Intent.ACTION_VIEW);
				if (loadbutton.getText().toString().equals("Create Load")) {
					openActivity.setClassName("com.mobile.udrove.u",
							"com.mobile.udrove.u.uDroveCreateLoadActivity");
				} else {
					openActivity.setClassName("com.mobile.udrove.u",
							"com.mobile.udrove.u.uDroveCurrentLoadActivity");
				}
				startActivity(openActivity);

			}

		} else {

			UIUtils.showServiceError(this, message);

		}

	}

}
