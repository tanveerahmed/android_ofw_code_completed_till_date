
package com.mobile.udrove.u;

import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.UIUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class is used to Launch the uDrove Login Screen
 * @dated 16-Feb-2012
 */

public class uDroveSignInActivity extends Activity implements
		UDroveServiceListener {

	/* uDrove Signin class variables */
	
	private Button btn_signIn;
	private Button btn_requestDemo;
	uDroveApplication udroveapp;
	Context context;
	String userName;
	String password ;
	// private TextView lblResult
    
    uDroveDataBaseAdapter dbAdapter;

	/**
	 * {@inheritDoc}
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.udrovesignin);
     	udroveapp = ((uDroveApplication) getApplication());
		udroveapp.ResetAllApplicationFields();
		btn_signIn = (Button) findViewById(R.id.signInButton);
		btn_requestDemo = (Button) findViewById(R.id.requestDemoButton);
		onclickListener listener = new onclickListener();
		btn_signIn.setOnClickListener(listener);
		btn_requestDemo.setOnClickListener(listener);
		context = this;
		udroveapp.setCheckLogout(true);
		dbAdapter=new uDroveDataBaseAdapter(this);
		uDroveApplication.fromLoginOrSplash=true;
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.signInButton:
				  hideKeyBoard(btn_signIn.getWindowToken());
				 userName = ((EditText) findViewById(R.id.edtvu_userId))
						.getText().toString();
				 password = ((EditText) findViewById(R.id.edtvu_userPin))
						.getText().toString();
				validateUser(userName, password);

				break;
			case R.id.requestDemoButton:
				hideKeyBoard(btn_requestDemo.getWindowToken());
				Intent openActivity = new Intent();
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveRequestDemoActivity");
				startActivity(openActivity);

				break;
			}

		}

	}

	private void validateUser(String userName, String password) {

		String errormsg = "";
		boolean complete = true;

		if (userName == null || userName.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.USERID + "\n";
		}

		if (password == null || password.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.PIN + "\n";
		}

		if (!complete) {
			UIUtils.showError(this, errormsg);
		}

		else {
			userName = userName.trim();
			password = password.trim();

			HashMap<String, String> data;
			data = new HashMap<String, String>();
			data.put("time_stamp", HTTPUtil.TodaysDate());
			data.put("userid", userName);
			data.put("password", password);
			
			// data.put("userid", "t_driver");
			// data.put("password", "driver123");
			udroveapp.setUserid(userName);
			udroveapp.setPassword(password);
			UDroveService serv = new UDroveService(this, data, this);
			serv.execute(HTTPConstants.LOGIN_URL);

		}
	}

	private void hideKeyBoard(IBinder windowToken) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(windowToken,InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// preventing default implementation previous to
			// android.os.Build.VERSION_CODES.ECLAIR
			// finish();
		//	uDroveParentActivity.finishAll();
			
			
			Intent intent= new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
			startActivity(intent);
		
		//	finish();
			
			return true;
			
		}
		return super.onKeyDown(keyCode, event);
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete " + posts);
		String result = HTTPUtil.ParseLoginJson(posts);
		System.out.println("IN Login " + result);
	//	boolean check=false;
		String spilt[] = result.split(",");
		if (spilt[0] != null && spilt[0].equals("FAIL")) {
			ShowAlert(spilt[1]);
		}else{   
			JSONObject jObject;
			try {
			jObject = new JSONObject(posts);
		
			JSONObject headerObject = jObject.getJSONObject("header");
			JSONObject bodyObject = jObject.getJSONObject("body");
			System.out.println("sign Url" +bodyObject.getString("signature_url"));
			System.out.println("service ids " +headerObject.getString("subscription_info"));
            
			dbAdapter.open();
			dbAdapter.insertDriverDetails(userName,password,bodyObject.getString("signature_url"),headerObject.getString("subscription_info"),
					bodyObject.getString("fuel_receipt_required"),bodyObject.getString("expense_receipt_required"));
			dbAdapter.close();
	
			uDroveApplication.setSettingsImageUrl(bodyObject.getString("signature_url"));
			udroveapp.setSignUrl(bodyObject.getString("signature_url"));
			udroveapp.setServiceIds(headerObject.getString("subscription_info"));
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);
			openActivity.setClassName("com.mobile.udrove.u","com.mobile.udrove.u.uDroveHomeActivity");
			startActivity(openActivity);
			finish();
		}

	}

 public void ShowAlert(String Message)
 {
		AlertDialog.Builder builder = new AlertDialog.Builder(uDroveSignInActivity.this);
		builder.setMessage(Message).setCancelable(false).setPositiveButton("OK", null).setTitle(
				"uDrove Mobile").setIcon(R.drawable.icon_info);
		AlertDialog alert = builder.create();
		alert.show();
 }
 
 /*
 @Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(!udroveapp.getCheckLogout())
		{
			System.out.println("In resume checklogut is false....");
			android.os.Process.killProcess(android.os.Process.myPid());
			System.runFinalizersOnExit(true);
			System.exit(0);			
		}
		else
		{
			System.out.println("In resume checklogut is True....");
		}
		
	}
*/
 
 
public void onServiceComplete(String posts, String reference) {
	// TODO Auto-generated method stub
	
}

}