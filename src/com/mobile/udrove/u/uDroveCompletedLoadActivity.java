package com.mobile.udrove.u;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.db.uDroveDbAdapter;
import com.mobile.udrove.model.MetaDataModel;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the Load Management home of the uDrove Mobile
 *              apps
 * 
 * @dated 16-Feb-2012
 */

public class uDroveCompletedLoadActivity extends uDroveParentActivity {

	/* uDrove Business Tools Home class variables */

	/**
	 * {@inheritDoc}
	 */
	private String selectedOrginState, selectedDestState;

	EditText edttxtstartdate, edttxtenddate;
	private int mYear;
	private int mMonth;
	private int mDay;
	static final int DATE_DIALOG_ID = 0, DATE_DIALOG_IDS = 1;
	uDroveDbAdapter db;
	uDroveDataBaseAdapter dbAdapter;
	 int[] stateIds;
	    String[] stateNames;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.completedload);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveCompletedLoadActivity.this);

		/* DB Part */
        dbAdapter=new uDroveDataBaseAdapter(this); 
        
        dbAdapter.open();

        MetaDataModel stateModal=dbAdapter.getStatesList();
	    dbAdapter.close();
	    
	    
	    
	    if(stateModal!=null){
	          stateIds=stateModal.getIdList();
	          stateNames=stateModal.getNameList();
	           
	         }

		onclickListener listener = new onclickListener();
		findViewById(R.id.sendTripButton).setOnClickListener(listener);

		String load_end_date; // zero record
	
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {
				load_end_date = getIntent().getExtras().getString("load_end_date");
				System.out.println("Selected Id is " + load_end_date);
				
				fillLoadDetails(load_end_date);
			//	ReadLoadManagementRecord(id);
			}
		}
	}

	private void fillLoadDetails(String loadid){
		
		dbAdapter.open();
		final Cursor loadDetails=dbAdapter.getCompletedLoadDetails(loadid);
		if(loadDetails!=null){
		
			((EditText) findViewById(R.id.txtstartdatetime)).setText(loadDetails.getString(0));
			((EditText) findViewById(R.id.originstate)).setText(stateNames[Integer.parseInt(loadDetails.getString(1))]);
			((EditText) findViewById(R.id.txtcity)).setText(loadDetails.getString(2));
			((EditText) findViewById(R.id.txtodometer)).setText(loadDetails.getString(3));
			((EditText) findViewById(R.id.txtshipper)).setText(loadDetails.getString(4));
			((EditText) findViewById(R.id.txtcomm)).setText(loadDetails.getString(5));
			((EditText) findViewById(R.id.txtproship)).setText(loadDetails.getString(6));
			((EditText) findViewById(R.id.deststate)).setText(stateNames[Integer.parseInt(loadDetails.getString(7))]);
			((EditText) findViewById(R.id.txtdestcity)).setText(loadDetails.getString(8));
			((EditText) findViewById(R.id.txtenddatetime)).setText(loadDetails.getString(9));

		}
		loadDetails.close();
		dbAdapter.close(); 
		
	}
	
	
	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.sendTripButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity
						.setClassName("com.mobile.udrove.u",
								"com.mobile.udrove.u.uDroveTripPacketActivity");
				startActivity(openActivity);
				break;
			}
		}
	}
	
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		System.out.println("change duty status on finish method");
		super.finish();
	}
	
}