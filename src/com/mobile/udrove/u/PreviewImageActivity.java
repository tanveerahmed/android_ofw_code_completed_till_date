package com.mobile.udrove.u;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.util.AppUtil;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class PreviewImageActivity extends uDroveParentActivity {

	ImageView image;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.previewimage);
		uDroveDataBaseAdapter dbAdapter = new uDroveDataBaseAdapter(this);

		onclickListener listener = new onclickListener();
         
		int width = getWindowManager().getDefaultDisplay().getWidth();
		int height = getWindowManager().getDefaultDisplay().getHeight();
		
		
		image = (ImageView) findViewById(R.id.previewImageView);
		image.setOnClickListener(listener);
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {

				if (getIntent().getExtras().getString("imgageuri") != null) {
					AppUtil.setImagetoView1(getIntent().getExtras().getString("imgageuri"), image, dbAdapter,width,height,1);

				}
			}
		}
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {
			uDroveApplication.fromBackButton=true;
			finish();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// preventing default implementation previous to
			// android.os.Build.VERSION_CODES.ECLAIR
			uDroveApplication.fromBackButton=true;

			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}