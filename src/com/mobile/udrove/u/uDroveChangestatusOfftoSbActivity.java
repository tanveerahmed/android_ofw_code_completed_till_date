package com.mobile.udrove.u;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.googlecode.android.widgets.DateSlider.DateSlider;
import com.googlecode.android.widgets.DateSlider.DateTimeSlider;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.MetaDataModel;
import com.mobile.udrove.model.uDroveDriverStatus;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDrovePersonalUseService;
import com.mobile.udrove.service.UDrovePersonalUseServiceListener;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the drivers change status of off duty and
 *              Sleeper berth
 * @dated 16-Feb-2012
 */

public class uDroveChangestatusOfftoSbActivity extends uDroveParentActivity
		implements UDroveServiceListener, UDrovePersonalUseServiceListener {

	/* uDrove Change Status Home class variables */

	uDroveApplication udroveapp;
	TextView lblchangestatusofftodr;
	boolean checkstatusfrom = false;
	private String selectedStateItem;
	private int intselectedStateItem = 0;

	EditText edttxtdate, edttxtodometer;

	static final int DATETIMESELECTOR_ID = 5;
	private uDroveDriverStatus driverstatus = null;
	boolean validateodometerfield = false;
	int currentdutystatus;
	TextView tv;

	HashMap<String, String> data;

	LocationManager locationManager;
	LocationHandler mLocationHandler = new LocationHandler();
	uDroveDataBaseAdapter dbAdapter;

	int[] stateIds;
	String[] stateNames;

	/**
	 * {@inheritDoc}
	 */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.changestatusoftosb);
		tv = (TextView) findViewById(R.id.txtdutyhours);
		udroveapp = ((uDroveApplication) getApplication());
		lblchangestatusofftodr = (TextView) findViewById(R.id.txt_changestatusofftodr);
		dbAdapter = new uDroveDataBaseAdapter(this);
		dbAdapter.open();
		currentdutystatus = dbAdapter.getCurrentDutyStatus();
		MetaDataModel stateModal = dbAdapter.getStatesList();

		Cursor cur;
		try {
			cur = dbAdapter.getDriverDetails();
			if (cur != null && cur.getCount() > 0) {

				System.out
						.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"
								+ cur.getString(0) + cur.getString(1));

				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
			cur.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dbAdapter.close();

		if (stateModal != null) {
			stateIds = stateModal.getIdList();
			stateNames = stateModal.getNameList();

		}
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria crit = new Criteria();
		crit.setAccuracy(Criteria.ACCURACY_FINE);
		String bestProvider = locationManager.getBestProvider(crit, true);
		if (bestProvider != null) {
			locationManager.requestLocationUpdates(bestProvider, 2000, 3,
					mLocationHandler);
		}

		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 1, 1, mLocationHandler);

		Header header = (Header) findViewById(R.id.common_header_button);
		header.initHeader(uDroveChangestatusOfftoSbActivity.this);

		if (currentdutystatus == 1) // if the current duty status is Driving
		{
			EditText odometETV = (EditText) findViewById(R.id.txtodometer);
			odometETV.setHint("Required");
		}

		edttxtdate = (EditText) findViewById(R.id.txtdatetime);

		String changestatustitle = AppConstants.changestatustooffduty;
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {
				changestatustitle = getIntent().getExtras().getString(
						"changestatuscamefrom");

				if (changestatustitle.equals("offdutybutton")) {
					lblchangestatusofftodr
							.setText(AppConstants.changestatustooffduty);
				} else {
					checkstatusfrom = true;
					lblchangestatusofftodr
							.setText(AppConstants.changestatustosleeper);

				}
			}
		}

		onclickListener listener = new onclickListener();
		findViewById(R.id.changedutystatussaveButton).setOnClickListener(
				listener);

		// s1.setFocusable(true); s1.setFocusableInTouchMode(true);

		Spinner state = (Spinner) findViewById(R.id.statespineer);
		state.setFocusable(true);
		state.setFocusableInTouchMode(true);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, stateNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		state.setAdapter(adapter);
		state.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				selectedStateItem = stateNames[arg2];
				intselectedStateItem = stateIds[arg2];
			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}

		});

		/* DateTime Field set */

		// get the current date
		final Calendar c = Calendar.getInstance();
		updateDateDisplay();

		edttxtdate.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				showDialog(DATETIMESELECTOR_ID);
			}
		});

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.changedutystatussaveButton:
				/*
				 * String city = ((EditText)
				 * findViewById(R.id.txtcity)).getText() .toString(); String
				 * state = ((EditText) findViewById(R.id.txtstate))
				 * .getText().toString();
				 */
				String city = ((EditText) findViewById(R.id.txtcity)).getText()
						.toString();
				String datetime = ((EditText) findViewById(R.id.txtdatetime))
						.getText().toString();
				String odometer = ((EditText) findViewById(R.id.txtodometer))
						.getText().toString();
				String remarks = ((EditText) findViewById(R.id.txtremarks))
						.getText().toString();
				validatedutystatus(city, selectedStateItem, datetime, odometer,
						remarks);

				break;

			}

		}
	}

	private void validatedutystatus(String city, String state, String datetime,
			String odometeroffduty, String remarks) {

		String errormsg = "";
		boolean complete = true;

		if (city == null || city.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.CITY + "\n";
		}
		if (intselectedStateItem == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.STATE + "\n";
		}
		if (datetime == null || datetime.trim().length() == 0) {
			complete = false;
			errormsg = errormsg + AppConstants.DATETIME + "\n";
		}
		if (currentdutystatus == 1) // if the current duty status is Driving
		{
			if (odometeroffduty == null || odometeroffduty.trim().length() == 0) {
				complete = false;
				errormsg = errormsg + AppConstants.ODOMETER + "\n";
			}
		}

		if (!complete) {
			UIUtils.showError(uDroveChangestatusOfftoSbActivity.this, errormsg);
		}

		else {

			String gpslat_start = "";
			String gpslon_start = "";

			gpslat_start = String.valueOf(mLocationHandler.lastKnownLatitude);
			gpslon_start = String.valueOf(mLocationHandler.lastKnownLongitude);

			data = new HashMap<String, String>();

			if (checkstatusfrom) // came from sleeper berth
			{
				data.put("duty_status_type_id", "2"); // for sleeper berth
			} else {

				data.put("duty_status_type_id", "3"); // off duty
			}

			data.put("time_stamp", HTTPUtil.TodaysDate());
			data.put("duty_date_time", datetime);
			data.put("userid", udroveapp.getUserid());
			data.put("password", udroveapp.getPassword());
			data.put("city", city);
			data.put("state_id", String.valueOf(intselectedStateItem));
			data.put("odometer", odometeroffduty);
			data.put("remarks", remarks);
			data.put("latitude", gpslat_start);
			data.put("longitude", gpslon_start);

			// data.put("latitude","38.8951118");
			// data.put("longitude","-77.0363658");

			/*
			 * UDroveService serv = new UDroveService(this, data, this);
			 * serv.execute();
			 */

			new UDrovePersonalUseService(this, this, udroveapp).execute();

		}

	}

	private DateSlider.OnDateSetListener mDateTimeSetListener = new DateSlider.OnDateSetListener() {
		public void onDateSet(DateSlider view, Calendar selectedDate) {
			// update the dateText view with the corresponding date
			int minute = selectedDate.get(Calendar.MINUTE)
					/ DateTimeSlider.MINUTEINTERVAL
					* DateTimeSlider.MINUTEINTERVAL;

			edttxtdate.setText(String.format(
					AppConstants.DATE_SLIDER_DATE_FORMAT, selectedDate,
					selectedDate, selectedDate, selectedDate, minute));

		}
	};

	@Override
	protected Dialog onCreateDialog(int id) {

		System.out.println("On Create Dialog");
		final Calendar c = Calendar.getInstance();
		switch (id) {

		case DATETIMESELECTOR_ID:
			return new DateTimeSlider(this, mDateTimeSetListener, c);
		}
		return null;
	}

	private void updateDateDisplay() {

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());

		this.edttxtdate.setText(formattedDate3 + AppUtil.getTimeZoneString());
	}

	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub

		System.out.println("the onService Complete " + posts);
		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if (spilt[2] != null && spilt[2].equalsIgnoreCase("false")) {

			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				UIUtils.showServiceError(this, spilt[1]);
			} else {

				insertNewDutyStatusintoDB();

				Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();

				finish();

			}

		} else {

			UIUtils.showSubscriptionChangedMessage(this);
		}

	}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub

	}

	public void insertNewDutyStatusintoDB() {

		JSONObject json_req = new JSONObject();

		try {

			if (checkstatusfrom) // came from sleeper berth
			{
				json_req.put("duty_status_type_id", "2"); // for sleeper berth
			} else {

				json_req.put("duty_status_type_id", "3"); // off duty
			}
			json_req.put("duty_date_time",
					((EditText) findViewById(R.id.txtdatetime)).getText()
							.toString());
			json_req.put("duty_hours", "0");
			json_req.put("remarks", ((EditText) findViewById(R.id.txtremarks))
					.getText().toString());

			dbAdapter.open();
			dbAdapter.insertDutyStatuses(json_req, HTTPUtil.TodayDateOnly());
			dbAdapter.close();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			String avhrs = AppUtil.getUpdatedAvailableDriveTime(this, tv);
			tv.setText(avhrs);
			/*
			 * if(!avhrs.equalsIgnoreCase("00:00")){
			 * 
			 * tv.setTextColor(Color.WHITE); }
			 */
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub
		System.out.println("change duty status on finish method");
		locationManager.removeUpdates(mLocationHandler);
		super.finish();
	}

	/* PERSONAL_CONVEYANCE */

	public void onPersonalUseStatus(boolean result, boolean personaluseStatus,
			String message) {

		if (result) {

			dbAdapter.open();
			dbAdapter.updatePersonalConveyance(String
					.valueOf(personaluseStatus));
			dbAdapter.close();

			if (personaluseStatus) {
				
				UIUtils.showPersonalUseFlagEnabledPrompt(this,AppConstants.TASK_TERMINATED_AS_PC_ON);
				
				/*   
				 * navigate to homescreen
				 */
				
			//	AppUtil.navigateToHomeScreen(this);
				
				
			} else {

				UDroveService serv = new UDroveService(this, data, this);
				serv.execute(HTTPConstants.DRIVER_STATUS_URL);

			}

		} else {

			UIUtils.showServiceError(this, message);

		}

	}

}