package com.mobile.udrove.u;


import android.content.Intent;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.model.DVIRItemModel;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;



public class DvirSettingsActivity extends uDroveParentActivity {

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */

	private static final int CAMERA_PIC_REQUEST = 1337;
	String imageUri = null;
	TextView lbltxtheader;
	ImageView image;

	String changestatustitle;
	Integer checkboxid;
	uDroveDataBaseAdapter dbAdapter;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvirsettings);

		onclickListener listener = new onclickListener();
		findViewById(R.id.saveButton).setOnClickListener(listener);
		findViewById(R.id.back_header_button).setOnClickListener(listener);
		findViewById(R.id.camera_header_button).setOnClickListener(listener);
		image = (ImageView) findViewById(R.id.imageview);
		dbAdapter = new uDroveDataBaseAdapter(this);
   
		uDroveApplication.fromBackButton=false;
		lbltxtheader = (TextView) findViewById(R.id.txthead);
		changestatustitle = AppConstants.changestatustooffduty;
		if (getIntent() != null) {
			if (getIntent().getExtras() != null) {

				if (getIntent().getExtras().getString("camefrom") == null) {

					changestatustitle = getIntent().getExtras().getString("checkboxName");
					checkboxid = getIntent().getExtras().getInt("checkboxId");
					lbltxtheader.setText(changestatustitle);
					
					// CheckBox checkbox = findViewById(id)
				}
			}
		}
		
	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			String errormsg = "";
			boolean complete = true;
			switch (v.getId()) {

			case R.id.saveButton:

				String remarks = ((EditText) findViewById(R.id.txtremarks))
						.getText().toString();

				if (imageUri == null ) {
					complete = false;
					errormsg = errormsg + AppConstants.PHOTO + "\n";
				}

				if (!complete) {
					UIUtils.showError(DvirSettingsActivity.this, errormsg);
				} else {
					if (imageUri == null) {
						imageUri ="";
					}
					else {
						
					}
                      
					DVIRItemModel dvirItem = new DVIRItemModel();
					dvirItem.setImageUri(imageUri);
					dvirItem.setRemarks(remarks);
					dvirItem.setDefectId(checkboxid);
					uDroveApplication.hashmapdviritemmodel.put(checkboxid,dvirItem);
					finish();
				}

				break;

			case R.id.back_header_button:
				uDroveApplication.fromBackButton=true;
				finish();
				break;

			case R.id.camera_header_button:
				StartCamera();
				break;
			}

		}

	}
	
	
	

	private void StartCamera() {
	/*	// start camera

		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		
		 * if (uDroveApplication.galleryImageCounter <= 0) {
		 * 
		 * lastImageId = getLastImageID();
		 * 
		 * }
		 
		ContentValues values = new ContentValues();
		values.put(MediaColumns.TITLE, "New Picture");
		values.put(ImageColumns.DESCRIPTION, "From your Camera");
		imageUri = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		startActivityForResult(intent, CAMERA_PIC_REQUEST);*/
		
        System.gc();
		Intent intent = new Intent(this, CameraControlNew.class);
		startActivityForResult(intent, CAMERA_PIC_REQUEST);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {

			switch (requestCode) {

			case CAMERA_PIC_REQUEST:

			/*	if (uDroveApplication.galleryImageCounter <= 0) {

					uDroveApplication.galleryImageCounter = getLastImageID()
							- lastImageId;

				}*/
          //     imageUri=	addImagesToGallery();
				
				Bundle bdl = data.getExtras();
				imageUri = bdl.getString("filename");
				AppUtil.setImagetoView1(imageUri,image,dbAdapter);
				
				break;
			}
		}
		else if(resultCode == RESULT_CANCELED)
		{
			
			imageUri = null;
	   	}
	}
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			uDroveApplication.fromBackButton=true;
			this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	
	
	

}