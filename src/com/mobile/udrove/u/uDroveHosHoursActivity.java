package com.mobile.udrove.u;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.db.uDroveDataBaseAdapter;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the Hours of Service information includes
 *              hours of the drivers
 * 
 * @dated 16-Feb-2012
 */

public class uDroveHosHoursActivity extends uDroveParentActivity implements UDroveServiceListener{

	/* uDrove Change Status Home class variables */

	/**
	 * {@inheritDoc}
	 */
	uDroveDataBaseAdapter dbAdapter;
	String[] logDates;
	
	 JSONObject json;
     TimerTask doAsynchronousTask;
     final Handler handler = new Handler();
     Timer timer;
 	uDroveApplication udroveapp;
 	TextView ofw_head = null;
 	TextView ofw_content = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hoshours);
		
		Header header = (Header) findViewById(R.id.common_header_button);
	    header.initHeader(uDroveHosHoursActivity.this);
		udroveapp = ((uDroveApplication) getApplication());
		this.checkDriverType();
		
	    dbAdapter=new uDroveDataBaseAdapter(this);
		dbAdapter.open(); 
 		Cursor cur;
 		try {
			cur = dbAdapter.getDriverDetails();
			if(cur!=null&&cur.getCount()>0){
		     
				System.out.println("LOGINTAG:SplashScreen didAlreadyLoggedIn() cursor details"+cur.getString(0)+cur.getString(1));
				udroveapp.setUserid(cur.getString(0));
				udroveapp.setPassword(cur.getString(1));
				uDroveApplication.setSettingsImageUrl(cur.getString(2));
				udroveapp.setSignUrl(cur.getString(2));
				udroveapp.setServiceIds(cur.getString(3));
			}
		    cur.close(); 
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 dbAdapter.close(); 

	}

	public void checkDriverType()
	{
		HashMap<String, String> data;
		data = new HashMap<String, String>();
		data.put("userid", udroveapp.getUserid());
		data.put("password", udroveapp.getPassword());
		
		// data.put("userid", "t_driver");
		// data.put("password", "driver123");
		System.out.println("Posted string is:"+data.toString());
		
		UDroveService serv = new UDroveService(this, data, this);
		serv.execute(HTTPConstants.GET_USER_EXEMPTIONS);
	}
	
	public void updateLogHoursDetails(){
		
	    TextView temp;
	    try {
	    	String[] hosinfo=AppUtil.getUpdatedHosInfo(this).split(",");
			temp=(TextView)findViewById(R.id.textsixtorseventyhours);
			temp.setText(hosinfo[0]+" Hrs");
			temp=(TextView)findViewById(R.id.txtAvailDT);
			temp.setText(hosinfo[1]);
			temp=(TextView)findViewById(R.id.txtAvailOT);
			temp.setText(hosinfo[2]);
			temp=(TextView)findViewById(R.id.txtAvailTDT);
			temp.setText(hosinfo[3]);
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
		dbAdapter.open();
		logDates=dbAdapter.getLastSevenLogDates();
	//	logDates=HTTPUtil.getLastSevenDatesFromToday().split(",");
       	if(logDates!=null && logDates.length>0){
              
       		int recapIndex=0;
     	      
        	Cursor dutyStatusesCur=dbAdapter.getDutyStatusesForDate(logDates[0]);
    		// Static data for plotting
            if(dutyStatusesCur!=null&&dutyStatusesCur.getCount()>0){
    		 ArrayList<DriverStatus> dr = new ArrayList<DriverStatus>();
   	  
    	     do{
    			
    	    	String dutyStatusTypeid=dutyStatusesCur.getString(0);
				String dutyHours=dutyStatusesCur.getString(1);
				
				if(dutyStatusTypeid.equalsIgnoreCase("null")||dutyStatusTypeid.length()==0){
					
					dutyStatusTypeid="0";
				}
				
				if(dutyHours.equalsIgnoreCase("null")||dutyHours.length()==0){
					
					dutyHours="0";
				}

				dr.add(new DriverStatus(Integer.parseInt(dutyStatusTypeid), Double.parseDouble(dutyHours)));
				
    		}while(dutyStatusesCur.moveToNext());  
    	    
    	     
    	    double totOff=0.0,totOn=0.0,totSB=0.0,totDr=0.0,totOfw=0.0;

    	    /* ds is a single DriverStatus instance */
    	    /* dr is a collection of DriverStatus */
    	    
    		 DriverStatus ds;

    		for (int k = 0; k < dr.size(); k++) {

    			ds = dr.get(k);
    			
    	/*		if(k==dr.size()-1){
    				double dutyHoursForPresentDutyStatus=calculateDutyHoursForPresentStatus();
					ds.setHours(dutyHoursForPresentDutyStatus);    			}
                */
    			
    			switch(ds.getDutyStatus()){
    			case 11://oilfield wait
    				totOfw+=ds.getHours();
    				break;			
    			case 2://sleeper berth
    				totSB+=ds.getHours();
    				break;
    			case 1://driving
    				totDr+=ds.getHours();
    				break;
    			case 3: //off duty
    				totOff+=ds.getHours();
    				break;
    			case 4: //on duty
    			case 5:
    			case 6:
    			case 7:
    			case 8:
    			case 9:
    			case 10:
    				totOn+=ds.getHours();
    				break;
    			}

    		}
    		
    		temp=(TextView)findViewById(R.id.todaysON);
			temp.setText(AppUtil.timeHoursMinutesString(totOn));
			temp=(TextView)findViewById(R.id.todaysDR);
			temp.setText(AppUtil.timeHoursMinutesString(totDr));
			temp=(TextView)findViewById(R.id.todaysOF);
			temp.setText(AppUtil.timeHoursMinutesString(totOff));
			temp=(TextView)findViewById(R.id.todaysSB);
			temp.setText(AppUtil.timeHoursMinutesString(totSB));
			temp=(TextView)findViewById(R.id.todaysOFW);
			temp.setText(AppUtil.timeHoursMinutesString(totOfw));
		
        }	
       	
   		dutyStatusesCur.close();
   		
   
       	////////////////////////////////////////////////////////////////////////////////	
       	TextView dateTV;
       	TextView hoursTV;
       	for(int i=recapIndex;i<logDates.length;i++){
       	  
       	  String totOnHours=AppUtil.timeHoursMinutesString(dbAdapter.getTotalOnDutyHoursForDate(logDates[i]));
       	  System.out.println("totHours for"+logDates[i]+"  is"+totOnHours);
       	  switch(i){
       	   case 1:
       		   dateTV=(TextView)findViewById(R.id.txtdate1);
       		   hoursTV=(TextView)findViewById(R.id.txthour1);
       		   dateTV.setText(logDates[i]);
       		   hoursTV.setText(totOnHours);
       		   break;
       	   case 2:
     		   dateTV=(TextView)findViewById(R.id.txtdate2);
     		   hoursTV=(TextView)findViewById(R.id.txthour2);
     		   dateTV.setText(logDates[i]);
     		   hoursTV.setText(totOnHours);
     		   break;
       	 case 3:
     		   dateTV=(TextView)findViewById(R.id.txtdate3);
     		   hoursTV=(TextView)findViewById(R.id.txthour3);
     		   dateTV.setText(logDates[i]);
     		   hoursTV.setText(totOnHours);
     		   break;
       	 case 4:
     		   dateTV=(TextView)findViewById(R.id.txtdate4);
     		   hoursTV=(TextView)findViewById(R.id.txthour4);
     		   dateTV.setText(logDates[i]);
     		   hoursTV.setText(totOnHours);
     		   break;
       	 case 5:
     		   dateTV=(TextView)findViewById(R.id.txtdate5);
     		   hoursTV=(TextView)findViewById(R.id.txthour5);
     		   dateTV.setText(logDates[i]);
     		   hoursTV.setText(totOnHours);
     		   break;
       	 case 6:
     		   dateTV=(TextView)findViewById(R.id.txtdate6);
     		   hoursTV=(TextView)findViewById(R.id.txthour6);
     		   dateTV.setText(logDates[i]);
     		   hoursTV.setText(totOnHours);
     		   break;
       	 case 7:
     		   dateTV=(TextView)findViewById(R.id.txtdate7);
     		   hoursTV=(TextView)findViewById(R.id.txthour7);
     		   dateTV.setText(logDates[i]);
     		   hoursTV.setText(totOnHours);
     		   break;
       	   
       	  }
       	  
       	}

       	}
        dbAdapter.close();
		
	}
	
/*	private double calculateDutyHoursForPresentStatus() {
		 
		String startTime=dbAdapter.getStartTimeOfCurrentDutyStatus();
		
		if(startTime!=null&&startTime.length()>0){
			SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
			try {    
				Date dutyStartTime=df3.parse(startTime);
				Date now=df3.parse(HTTPUtil.TodaysDate());
				System.out.println("StartTime:"+dutyStartTime+"CurrentTime"+now);
				long timeDiff=now.getTime()-dutyStartTime.getTime();
				return ((float)timeDiff)/(1000*60*60);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	 */
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		scheduleTimerTask();
	}
	
	private void scheduleTimerTask(){

		doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                handler.post(new Runnable() {
                    public void run() {
                        try {
                    	System.out.println("TimerTask called at "+HTTPUtil.TodaysDate());
                    	  new GetCurrentDayLogTask().execute();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                      }
                    }
                });
            }
        };
        timer = new Timer();
		 timer.schedule(doAsynchronousTask, 0,AppConstants.TIMER_INTERVAL);//execute in every 1min
	}
	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		System.out.println("On Pause Method stopping timer at "+HTTPUtil.TodaysDate());
		timer.cancel();
		timer=null;
		super.onPause();
	}
	
	private class GetCurrentDayLogTask extends AsyncTask<String, Void, String> {
		
		ProgressDialog dialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.v(">>>>>>>>>>>>>>>>", "onPreExecute");
		    dialog=new ProgressDialog(uDroveHosHoursActivity.this);
			dialog.setMessage(HTTPConstants.CONNECTING);
			dialog.show();
		}
		
		
	     @Override
		protected String doInBackground(String... urls) {
	    	    HttpClient httpclient = new DefaultHttpClient();
				HttpParams myParams = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(myParams, 10000);
				HttpConnectionParams.setSoTimeout(myParams, 10000);

				json=new JSONObject();
				HttpPost httput = new HttpPost(HTTPConstants.GET_CURRENT_DAY_LOG);

				httput.setHeader("Accept", "application/json");
				httput.setHeader("Content-type", "application/json");

				StringEntity se = null;
				String str="";
				try {
					json.put("time_stamp",HTTPUtil.TodaysDate());
					json.put("userid",udroveapp.getUserid());
					json.put("password",udroveapp.getPassword());
					se = new StringEntity(json.toString());
					System.out.println("The SE is  request " + se.toString());
					se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
							"application/json"));
					httput.setEntity(se);
					HttpResponse response = httpclient.execute(httput);
					byte[] result = EntityUtils.toByteArray(response.getEntity());
					str = new String(result, "UTF-8");
					Log.v(">>>>>>>", "str:" + str);
					return str;
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();

				}
				return str;
	     }

	     @Override
		protected void onPostExecute(String posts) {
	    	
	    	 dialog.dismiss();
	    	 
	    	 if(posts!=null){
             //  dutyHoursTv.setText(HTTPUtil.TodaysDate());
	 			String result = HTTPUtil.ParseJson(posts);
	 			String spilt[] = result.split(",");
	 			if (spilt[0] != null && spilt[0].equals("FAIL")) {
	 				
	 				System.out.println("Current day logs got failed");
		
	 			} else {
   				System.out.println("Current day logs  got successfully");
	 			   populateCurrentDayLogToDB(posts);

	 			} 
                
	 			updateLogHoursDetails();
	    	 }
	     }
	 }

	 public void populateCurrentDayLogToDB(String posts){
			
			try {
				JSONObject jObject = new JSONObject(posts);
	            dbAdapter.open(); 
	            JSONObject bodyObject = jObject.getJSONObject("body");
	            JSONArray dutyStatusesArray = bodyObject.getJSONArray("duty_statuses");
	            if (dutyStatusesArray.length() > 0) { // Data at server side
	        	
	            SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy");
	            String todaysDate = "";
				try {
					todaysDate = df3.format(df3.parse(dutyStatusesArray.getJSONObject(0).getString("duty_date_time")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            dbAdapter.deleteCurrentDayDutyStatuses(todaysDate);
					for(int i=0;i<dutyStatusesArray.length();i++){
			    		dbAdapter.insertDutyStatuses(dutyStatusesArray.getJSONObject(i),todaysDate);
					}
				}
			
				JSONObject hosSummary=jObject.getJSONObject("summary");
		        dbAdapter.deleteAllRecords("HOS_DETAILS");
				dbAdapter.insertHosInfo(hosSummary,HTTPUtil.TodaysDate1());
			
				JSONObject logInfo=jObject.getJSONObject("log_info");
				dbAdapter.deleteCurrentDayLog(logInfo.getString("log_date"));
				dbAdapter.insertLogsForCurrentDay(logInfo.getString("paper_log_id"),logInfo.getString("log_date"));
				dbAdapter.deleteAllRecords("LOG_INFO");
				dbAdapter.insertLogInfo(logInfo);  
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				dbAdapter.close();

			} 
			dbAdapter.close();
			
		}

	 public void UpdateOFWonUI()
	 {
		 if(udroveapp.getDriverType() != AppConstants.OIL_FIELD_EXEMPTION){
			 ofw_head = (TextView) findViewById(R.id.todaysOFW);
			 ofw_content = (TextView) findViewById(R.id.txtofwhours);
			 
			 ofw_head.setVisibility(View.GONE);
			 ofw_content.setVisibility(View.GONE);
			 
		 }
	 }
	 
		public void onServiceComplete(String posts) {
			// TODO Auto-generated method stub
			System.out.println("the onService Complete " + posts);
			String result = HTTPUtil.ParseLoginJson(posts);
			System.out.println("IN ChangeDutyStatusHome" + result);
			String spilt[] = result.split(",");
			
			if (spilt[0] != null && spilt[0].equals("FAIL")) {
				ShowAlert(spilt[1]);
			}
			else
			{   
				JSONObject jObject;
				try {
				jObject = new JSONObject(posts);
			
				JSONObject bodyObject = jObject.getJSONObject("body");
	            
	/*			Database Update pending. Need to complete this once UI 
	 * 			update is done based on remote data.
	 * 
	 * 			dbAdapter.open();
				dbAdapter.insertDriverDetails(userName,password,bodyObject.getString("signature_url"),headerObject.getString("subscription_info"),
						bodyObject.getString("fuel_receipt_required"),bodyObject.getString("expense_receipt_required"));
				dbAdapter.close();
				*/
				JSONArray JArray = bodyObject.getJSONArray("exemption_ids");

				for(int i=0; i < JArray.length(); i++)
				{
					//Server responds with Status-ID 1 to indicate OIL_FIELD_EXEMPTION. 
					if(Integer.parseInt(JArray.getString(i)) == 1)
					{
						udroveapp.setDriverType(AppConstants.OIL_FIELD_EXEMPTION);
					}
				}
				
				UpdateOFWonUI();
				// Done with the UI updation. Hence now we should reset the DriverType flag.
				udroveapp.setDriverType(0);
				} 
				catch (JSONException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}

	 public void ShowAlert(String Message)
	 {
			AlertDialog.Builder builder = new AlertDialog.Builder(uDroveHosHoursActivity.this);
			builder.setMessage(Message).setCancelable(false).setPositiveButton("OK", null).setTitle(
					"uDrove Mobile").setIcon(R.drawable.icon_info);
			AlertDialog alert = builder.create();
			alert.show();
	 }
	
	
}