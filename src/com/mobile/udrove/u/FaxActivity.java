package com.mobile.udrove.u;

import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.service.HTTPConstants;
import com.mobile.udrove.service.HTTPUtil;
import com.mobile.udrove.service.UDroveService;
import com.mobile.udrove.service.UDroveServiceListener;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.UIUtils;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class FaxActivity extends uDroveParentActivity implements UDroveServiceListener {

	protected boolean _active = true;
	protected static boolean register = true;
	uDroveApplication udroveapp;
	   String docType;
	   String docIds;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (getIntent() != null) {
	
			if (getIntent().getExtras() != null) {
				
				docType=getIntent().getExtras().getString("DocType");
				docIds=getIntent().getExtras().getString("DocIds");
				
			}
		}
		if(docType.equalsIgnoreCase("DVIR")){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}else{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

			}
		setContentView(R.layout.faxbox);

		udroveapp = ((uDroveApplication) getApplication());

		findViewById(R.id.faxcanclebtn).setOnClickListener(
				new OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						finish();
					}
				});

		findViewById(R.id.faxsendbtn).setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// TODO Auto-generated method stub

				String errormsg = "";
				boolean complete = true;
				String fax = ((EditText) findViewById(R.id.edtfax)).getText()
						.toString();
				if (fax == null || fax.trim().length() == 0) {
					complete = false;
					errormsg = errormsg + AppConstants.FAX;
				}
				if(fax!=null && fax.length()>0)
				{
					if(!AppUtil.isPhoneNumberValid(fax))
					{
						UIUtils.showError(FaxActivity.this, AppConstants.INVALIDFAX);
						return;
					}
				}

				if (!complete) {
					UIUtils.showError(FaxActivity.this, errormsg);
				}

				else {
					JSONObject jsonObjSend = new JSONObject();
					try {
											
						jsonObjSend.put("time_stamp", HTTPUtil.TodaysDate());
						jsonObjSend.put("userid", udroveapp.getUserid());
						jsonObjSend.put("password", udroveapp.getPassword());
						jsonObjSend.put("doc_type",docType);
						jsonObjSend.put("doc_id",docIds);
						jsonObjSend.put("comm_type","FAX");
						jsonObjSend.put("send_to",fax);

						UDroveService serv = new UDroveService(FaxActivity.this, jsonObjSend, FaxActivity.this);
						serv.execute(HTTPConstants.SEND_DOCUMENT);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

			//		finish();
				}
			}

		});
	}
	public void onServiceComplete(String posts) {
		// TODO Auto-generated method stub
		System.out.println("the onService Complete image" + posts);
		String result = HTTPUtil.ParseJson(posts);
		String spilt[] = result.split(",");
		if(spilt[2]!=null && spilt[2].equalsIgnoreCase("false")){
			if (spilt[0] != null && spilt[0].equals("FAIL")) {
			UIUtils.showServiceError(this, spilt[1]);
		//	finish();

		} else {
			Toast.makeText(this, spilt[1], Toast.LENGTH_SHORT).show();
			finish();

		}
			
			}else{
			
	        UIUtils.showSubscriptionChangedMessage(this);			
		}


		
	}
	public void onServiceComplete(String posts, String reference) {
		// TODO Auto-generated method stub
		
	}
}