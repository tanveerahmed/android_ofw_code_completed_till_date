package com.mobile.udrove.u;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.mobile.udrove.app.uDroveApplication;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class handles the Business Tools home of the uDrove Mobile
 *              apps
 * 
 * @dated 16-Feb-2012
 */

public class uDroveBusinessToolsHomeActivity extends uDroveParentActivity{

	/* uDrove Business Tools Home class variables */

	/**
	 * {@inheritDoc}
	 */

	uDroveApplication udroveapp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.businesstoolshome);

		udroveapp = ((uDroveApplication) getApplication());
		onclickListener listener = new onclickListener();
		findViewById(R.id.expenditureButton).setOnClickListener(listener);
		findViewById(R.id.fuelButton).setOnClickListener(listener);
		findViewById(R.id.podButton).setOnClickListener(listener);
		
		Header header = (Header) findViewById(R.id.common_header_button);
	    header.initHeader(uDroveBusinessToolsHomeActivity.this);

	}

	protected class onclickListener implements OnClickListener {

		public void onClick(View v) {

			Intent openActivity = new Intent();
			openActivity.setAction(Intent.ACTION_VIEW);

			switch (v.getId()) {
			case R.id.expenditureButton:
				udroveapp.setEditsignatureexpenditure(null);
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity
						.setClassName("com.mobile.udrove.u",
								"com.mobile.udrove.u.uDroveBusinessToolsExpenditureActivity");
				startActivity(openActivity);

				break;
			case R.id.fuelButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveBusinessToolsFuelActivity");
				startActivity(openActivity);

				break;

			case R.id.podButton:
				openActivity.setAction(Intent.ACTION_VIEW);
				openActivity.setClassName("com.mobile.udrove.u",
						"com.mobile.udrove.u.uDroveBusinessToolsPodActivity");
				startActivity(openActivity);

				break;

			
			}

		}

	}

}