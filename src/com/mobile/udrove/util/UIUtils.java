package com.mobile.udrove.util;

import com.mobile.udrove.u.R;
import com.mobile.udrove.u.uDroveParentActivity;
import com.mobile.udrove.u.uDroveSplashActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;

/**
 * 
 * @author mukesh kumar @ whishworks
 * @description This class is used to handle all the Alert Dialogs in uDrove
 *              Mobile apps
 * @dated 16-Feb-2012
 */

public class UIUtils {

	public static final int ICON_HOUR_GLASS = R.drawable.hourglass;

	/**
	 * creates the dialog
	 * 
	 * @param activity
	 * @param text
	 * @param iconType
	 * @return
	 */
	public static AlertDialog createDialog(Activity activity, String text,
			int iconType) {

		return createDialog(activity, AppConstants.AlertTitle, text, iconType,
				null, null, true);
	}

	/**
	 * creates the error dialog
	 * 
	 * @param activity
	 * @param text
	 * @param bNavigateHelpWindow
	 */
	public static void showError(final Activity activity, final String text,
			boolean bNavigateHelpWindow) {
		String[] buttons = null;
		try {
			int iconType = R.drawable.help;
			if (bNavigateHelpWindow) {
				buttons = new String[] { "OK", "Help" };
			} else {
				buttons = new String[] { "OK" };
				iconType = R.drawable.ic_error;
			}
			createDialog(activity, "Mandatory Fields Missing", text, iconType,
					buttons, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							if (which == DialogInterface.BUTTON_NEGATIVE) {
								Intent openActivity = new Intent();
								openActivity.setAction(Intent.ACTION_VIEW);
								openActivity.setClassName(
										"com.mobile.udrove.u",
										"com.mobile.udrove.HelpActivity");
								activity.startActivity(openActivity);
							} else {
								if (activity instanceof uDroveSplashActivity) {
									activity.finish();
								}
							}
							return;
						}
					}, false).show();
		} catch (Throwable ex) {

		}
	}

	/**
	 * creates the error dialog
	 * 
	 * @param activity
	 * @param text
	 */
	public static void showError(final Activity activity, final String text) {
		try {
			createDialog(activity, AppConstants.AlertTitle, text,
					R.drawable.ic_error, new String[] { "OK" },
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							return;
						}
					}, false).show();
		} catch (Throwable error) {

		}
	}

	public static void showServiceError(final Activity activity,
			final String text) {
		try {
			createDialog(activity, AppConstants.SERVICETitle, text,
					R.drawable.ic_error, new String[] { "OK" },
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							return;
						}
					}, false).show();
		} catch (Throwable error) {

		}
	}

	public static void showSubscriptionChangedMessage(final Activity activity) {
		try {
			createDialog(activity, AppConstants.SERVICETitle,
					AppConstants.subscriptionChangedMessage,
					R.drawable.ic_error, new String[] { "OK" },
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();

							Intent openActivity = new Intent();
							openActivity.setAction(Intent.ACTION_VIEW);
							openActivity.setClassName("com.mobile.udrove.u",
									"com.mobile.udrove.u.uDroveSignInActivity");
							activity.startActivity(openActivity);
							return;

						}
					}, false).show();
		} catch (Throwable error) {

		}
	}

	public static void showPersonalUseFlagEnabledPrompt(
			final uDroveParentActivity activity, final String text) {
	
		try {

			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			builder.setMessage(text)
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {

									AppUtil.navigateToHomeScreen(activity);

								}
							}).create().show();

		} catch (Throwable error) {

		}
		
	}
	
	
	
	public static void showPersonalUseFlagEnabledPromptFinish(
			final uDroveParentActivity activity, final String text) {
	
		try {

			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			builder.setMessage(text)
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
                                 
									activity.finish();
								//	AppUtil.navigateToHomeScreen(activity);

								}
							}).create().show();

		} catch (Throwable error) {

		}
		
	}

	

	/**
	 * creates the error dialog
	 * 
	 * @param activity
	 * @param text
	 */
	public static void showErrorExit(final Activity activity, final String text) {
		try {
			createDialog(activity, AppConstants.SERVICETitle, text,
					R.drawable.ic_error, new String[] { "OK" },
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							activity.finish();
							return;
						}
					}, false).show();
		} catch (Throwable error) {

		}
	}

	/**
	 * creates the status dialog
	 * 
	 * @param activity
	 * @param isProgressDialog
	 * @return
	 */
	public static Dialog showStatusDialog(Activity activity,
			boolean isProgressDialog) {
		try {
			if (isProgressDialog == false) {
				Dialog dlg = createDialog(activity, AppConstants.AlertTitle,
						"Please wait...", R.drawable.hourglass, null, null,
						true);
				dlg.show();
				return dlg;
			} else {
				return ProgressDialog.show(activity, AppConstants.AlertTitle,
						"Please wait...");

			}
		} catch (Throwable error) {
			return null;
		}
	}

	public static Dialog showStatusDialog(final String text, Activity activity,
			boolean isProgressDialog) {
		try {
			if (isProgressDialog == false) {
				Dialog dlg = createDialog(activity, AppConstants.AlertTitle,
						text, R.drawable.hourglass, null, null, true);
				dlg.show();
				return dlg;
			} else {
				return ProgressDialog.show(activity, AppConstants.AlertTitle,
						text);

			}
		} catch (Throwable error) {
			return null;
		}
	}

	/**
	 * creates wait dialog and bind the dialog to activity
	 * 
	 * @param activity
	 * @return
	 */

	public static AlertDialog createDialog(Activity activity, String title,
			String message, int icon, String[] buttonTitles,
			final DialogInterface.OnClickListener listener,
			boolean isCancelableDialog) {

		AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
		alertDialog.setTitle(title);

		if (icon != -1) {
			alertDialog.setIcon(icon);
		}
		alertDialog.setMessage(message);
		if (buttonTitles != null) {
			for (int i = 0; i < buttonTitles.length; i += 1) {
				if ((buttonTitles[i] != null && buttonTitles[i].length() > 0)) {
					if (i == 0) {
						alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
								buttonTitles[i], listener);
					} else if (i == 1) {
						alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
								buttonTitles[i], listener);
					} else {
						alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,
								buttonTitles[i], listener);
					}

				}
			}
		}

		alertDialog.setCancelable(isCancelableDialog);

		return alertDialog;
	}

	public static void show(Dialog dialog) {
		try {
			dialog.show();
		} catch (Throwable error) {

		}
	}

	public static void cancel(Dialog dialog) {
		try {
			dialog.cancel();
		} catch (Throwable error) {

		}
	}

}
