package com.mobile.udrove.util;

public interface AppConstants {

	
	public static final int OIL_FIELD_EXEMPTION = 1;
	public static final int SAMPLE1_EXEMPTION = 2;
	public static final int SAMPLE2_EXEMPTION = 3;
	
	public static final int DUTY_STATUS_ID_OIL_FIELD_WAIT = 11;
	
	// mukesh
	public static final String[] master_exempt_list = {"DUMMY", "OIL FIELD", "SAM-1", "SAMPLE-2", "SAMPLE-3234", "OIL FIELD WAIT", "SALE-5"};
	public static final String LOGIN_REQUIRED_FIELDS_MESSAGE = "Please fill in all required fields.";
	public static final String AlertTitle = "Mandatory Fields Missing";
	public static final String SERVICETitle = "uDrove Mobile";
	public static final String changestatustooffduty = "Change Status to Off Duty";
	public static final String changestatustosleeper = "Change Status to Sleeper Berth";
	public static final String savemessage = "Record is Saved into the Database";
	public static final String dvirUpdateMessage = "DVIR successfully updated.";

	public static final String subscriptionChangedMessage = "Your Subscription has been changed.Please re login.";

	public static final String INTERNET_CONNECTION_ERROR = "Connection to the server not available. Please verify your internet connectivity and try again.";
	public static final String SERVER_CONNECTION_UNAVAILABLE = "Connection to the server not available. Please try again.";

	public static final String INTERNET_CONNECTION_ERROR1 = "{\"header\":{\"result\":\"FAIL\",\"message\":\"Please check your internet connection and try again\",\"subscription_changed\":\"false\",\"personal_conveyance_flag\":\"false\"}}";
	public static final String SERVER_CONNECTION_ERROR = "{\"header\":{\"result\":\"FAIL\",\"message\":\"Connection to the server not available. Please try again.\",\"subscription_changed\":\"false\",\"personal_conveyance_flag\":\"false\"}}";

	public static final String TASK_TERMINATED_AS_PC_ON = "Personal Conveyance is \"ON\". Turn OFF,to perform this action";

	
	
	/*
	 * final static String[] states = new String[] { "Select any State",
	 * "Alabama"
	 * ,"Alaska","Arizona","Arkansas","California","Colorado","Connecticut"
	 * ,"Delaware","District of Columbia","Florida",
	 * "Georgia","Hawaii","Idaho","Illinois"
	 * ,"Indiana","Iowa","Kansas","Kentucky"
	 * ,"Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota",
	 * "Mississippi"
	 * ,"Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey"
	 * ,"New Mexico","New York","North Carolina",
	 * "North Dakota","Ohio","Oklahoma"
	 * ,"Oregon","Pennsylvania","Rhode Island","South Carolina"
	 * ,"South Dakota","Tennessee","Texas",
	 * "Utah","Vermont","Virginia","Washington"
	 * ,"West Virginia","Wisconsin","Wyoming"};
	 */
	final static String[] states = new String[] {"Select any State", "Armed Forces - Americas",
			"Armed Forces - Europe/Africa/Canada", "Alaska", "Alabama",
			"Armed Forces - Pacific", "Arkansas", "American Samoa", "Arizona",
			"California", "Colorado", "Connecticut", "District of Columbia",
			"Delaware", "Florida", "Federated States of Micronesia", "Georgia",
			"Guam", "Hawaii", "Iowa", "Idaho", "Illinois", "Indiana", "Kansas",
			"Kentucky", "Louisiana", "Massachusetts", "Maryland", "Maine",
			"Marshall Islands", "Michigan", "Minnesota", "Missouri",
			"Northern Mariana Islands", "Mississippi", "Montana",
			"North Carolina", "North Dakota", "Nebraska", "New Hampshire",
			"New Jersey", "New Mexico", "Nevada", "New York", "Ohio",
			"Oklahoma", "Oregon", "Pennsylvannia", "Puerto Rico", "Palau",
			"Rhode Island", "South Carolina", "South Dakota", "Tennessee",
			"Texas", "Utah", "Virginia", "Virgin Islands", "Vermont",
			"Washington", "Wisconsin", "West Virginia", "Wyoming" };

	final static String[] podstype = new String[] { "Select Document",
			"Accessorial Receipts", "Bill of Lading", "COD Check",
			"Delivery Receipt", "Detention Fee Receipt", "In Ticket",
			"Invoice", "Loading Receipt", "Lumper Receipt", "Out Ticket",
			"Packing List", "Pallet Receipt", "Purchase Order",
			"Rate Confirmation", "Temperature Control Receipt",
			"Trailer Control Records", "Unloading Receipt",
			"Weight Scale Tickets", "Other 1", "Other 2", "Other 3" };

	public static final String USERID_STRING = "USERID";
	public static final String PIN_STRING = "PIN";

	public static final String CITY = "City is required.";
	public static final String STATE = "State is required.";
	public static final String DATETIME = "Date Time is required.";
	public static final String ODOMETER = "Odometer is required.";

	public static final String USERID = "User ID is required.";
	public static final String PIN = "Password is required.";

	public static final String POWERUNIT = "Power Unit is required.";
	public static final String TRAILER = "Trailer Id is required.";
	public static final String CODRIVER = "Co-Driver is required.";
	public static final String PROSHIPPER = "Pro-Shipper is required.";
	public static final String COMMODITY = "Commodity is required.";
	public static final String START_DATE = "Start Date is required.";
	public static final String CATEGORY = "Category is required.";
	public static final String PODDOCUMENT = "Document Type is required.";

	public static final String ORIGIN_STATE = "Origin State is required.";
	public static final String ORIGIN_CITY = "Origin City is required.";
	public static final String SHIPPER = "Shipper is required.";
	public static final String COMM = "Commodity is required.";

	public static final String DEST_STATE = "Dest State is required.";
	public static final String DEST_CITY = "Dest City is required.";
	public static final String END_DATE = "End Date is required.";
	public static final String START_END_DATE = "End Date should be greater than Start Date.";

	
	public static final String FNAME = "First Name is required.";
	public static final String CONTACT = "Contact is required.";
	public static final String EMAIL = "Email ID is required.";
	public static final String INVALIDEMAIL = "Please enter valid Email Address.";
	public static final String INVALIDPHONE = "Please enter valid Contact Number.";
	public static final String INVALIDFAX = "Please enter valid Fax Number.";

	public static final String INVALIDAMOUNT_PU = "Enter valid Power Unit Amount";
	public static final String INVALIDGALLONS_PU = "Enter valid Power Unit Gallons";
	public static final String INVALIDAMOUNT_TR = "Enter valid Trailer Unit Amount";
	public static final String INVALIDGALLONS_TR = "Enter valid Trailer Unit Gallons";

	public static final String INVALIDAMOUNT = "Enter valid Amount";
	public static final String INVALIDGALLONS = "Enter valid Gallons";

	public static final String PHOTO_PU = "Power Unit Fuel Receipt Photo is required";
	public static final String PHOTO_TR = "Trailer Unit Fuel Receipt Photo is required";
	public static final String AMOUNT_TR = "Trailer Unit Amount is required.";
	public static final String GALLONS_TR = "Trailer Unit Gallons is required.";
	public static final String AMOUNT_PU = "Power Unit Amount is required.";
	public static final String GALLONS_PU = "Power Unit Gallons is required.";
	public static final String AMOUNT = "Amount is required.";
	public static final String GALLONS = "Gallons is required.";
	public static final String CON = "Consignee is required.";
	public static final String REC = "Received By is required.";
	public static final String TODATE = "To Date is required.";
	public static final String FROMDATE = "From Date is required.";
	public static final String VENDOR = "Vendor is required.";
	public static final String REMARKS = "Remarks is required.";
	public static final String DESC = "Issue description is required.";
	public static final String DUTYROLE = "Duty Role is required.";

	public static final String DRIVER = "Driver name is required.";
	public static final String MECHANIC = "Mechanic is required.";

	public static final String DATE_FORMAT = "MM-dd-yyyy HH:mm";

	public static final String DATE_ONLY_FORMAT = "MM-dd-yyyy";

	
	public static final String DATE_SLIDER_DATE_FORMAT = "%tm-%td-%tY %tH:%02d";

	public static final String DATE_SLIDER_DATE_FORMAT_ONLY = "%tm-%td-%tY";

	public static final String EXPENSE_CATEGORY = "Please create an expense to save.";
	public static final String ErrorCHECKBOX = "Please select any of the Checkbox";
	public static final String TRIP_PACKET_NO_CHECKBOX = "Please select an option to send a trip packet";
	public static final String ErrorCHECKBOXDVIR = "Please select the DVIR's.";

	public static final String FAX = "Fax is required";
	public static final String PHOTO = "Photo is required";
	public static final String DOCPHOTO = "Document Photo is required";
	public static final long TIMER_INTERVAL=15*60000;   // time interval 
	
	public static final String PC_FLAG_TRUE = "true";
	public static final String PC_FLAG_FALSE = "false";
	

}
