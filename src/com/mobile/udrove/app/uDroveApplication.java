package com.mobile.udrove.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Application;
import android.net.Uri;
import com.mobile.udrove.model.DVIRItemModel;
import com.mobile.udrove.model.DVIRModel;
import com.mobile.udrove.model.DVIRTable;
import com.mobile.udrove.model.PODModel;
import com.mobile.udrove.util.AppConstants;

public class uDroveApplication extends Application {
	/** Called when the activity is first created. */

	private int loadstatus = 0;
	public int dateindex = 0;
	public int driver_type = 0;
	public ArrayList<String> user_exempt_list;
	
	public static int galleryImageCounter = -1;

	ArrayList<DVIRTable> listdvirtable;
	String dateArray[] = { "date", "date", "date", "date", "date", "date",
			"date" };
     private static String editsignatureexpenditure;
     private static String settingsImageUrl;
    
     private static String signUrl;
     private static String serviceIds;

     private static String userid;
     private static String password;
     public static ArrayList<Boolean> dvirInspectionItems;
   
     public static HashMap<Integer,DVIRItemModel> hashmapdviritemmodel;  
     
     private HashMap<Integer,DVIRModel> lastSevenDvirsHashMap;
     
     ArrayList<DVIRModel> arraylistdvirmodel;  
     
     public static ArrayList<PODModel> arraylistpodmodel;  
     ArrayList<String> loadtempdata;
     boolean checklogout=false;
     int createloadid=0;
     
     public static boolean updateDvirsFromServer;
     
     public static boolean isUpdateDvirsFromServer() {
		return updateDvirsFromServer;
	}

	public static void setUpdateDvirsFromServer(boolean updateDvirsFromServer) {
		uDroveApplication.updateDvirsFromServer = updateDvirsFromServer;
	}

	public static ArrayList<Uri> galleryImageUris= new ArrayList<Uri>();
     
     public static ArrayList<Uri> getGalleryImageUris() {
		return galleryImageUris;
	}

	public static void setGalleryImageUris(ArrayList<Uri> galleryImageUris) {
		uDroveApplication.galleryImageUris = galleryImageUris;
	}

	public static boolean fromBackButton=false;
	public static boolean fromLoginOrSplash=true;

      
     public static boolean isFromLoginOrSplash() {
		return fromLoginOrSplash;
	}

	public static void setFromLoginOrSplash(boolean fromLoginOrSplash) {
		uDroveApplication.fromLoginOrSplash = fromLoginOrSplash;
	}

	public void setDefaulLoadTempData()
     {
    	 loadtempdata = new ArrayList<String>();  
     }
     
     public void setLoadTempData(String id)
     {
    	 this.loadtempdata.add(id);
     }
     
     public ArrayList<String> getLoadTempData()
     {
    	 return this.loadtempdata;
     }
     
    
	
	public void updateLoadStatus(int m_loadstatus) {
		loadstatus = m_loadstatus;
	}

	public void setDVIRTableList()
	{
		listdvirtable = new ArrayList<DVIRTable>();
	}
	
	public ArrayList<DVIRTable> getDVIRTableList()
	{
		return listdvirtable;
	}
	
	public int getDriverType() {
		return driver_type;
	}

	public void setDriverType(int type) {
		this.driver_type = type;
	}
	
	public void setExemption(int exempt_type){
		
		if(exempt_type <= AppConstants.master_exempt_list.length){
		
		switch(exempt_type){
		case 1:
			this.user_exempt_list.add(AppConstants.master_exempt_list[AppConstants.OIL_FIELD_EXEMPTION]);
			break;
		case 2:
			this.user_exempt_list.add(AppConstants.master_exempt_list[AppConstants.SAMPLE1_EXEMPTION]);
			break;
		case 3:
			this.user_exempt_list.add(AppConstants.master_exempt_list[AppConstants.SAMPLE2_EXEMPTION]);
			break;
		default:			
			}
		}
	}
	
	public int getLoadStatus() {
		return loadstatus;
	}

	public void updateDateindex(int _dateindex) {
		dateindex = _dateindex;
	}

	public void LoadcompletedDate(String startdate) {
		dateArray[dateindex] = startdate;
	}

	public String[] getCompletedDate() {
		return dateArray;
	}

	
	/**
	 * @param editsignatureexpenditure the editsignatureexpenditure to set
	 */
	public void setEditsignatureexpenditure(String editsignatureexpenditure) {
		uDroveApplication.editsignatureexpenditure = editsignatureexpenditure;
	}
	
	

	/**
	 * @return the editsignatureexpenditure
	 */
	public String getEditsignatureexpenditure() {
		return editsignatureexpenditure;
}
	
	

	public static String getSettingsImageUrl() {
		return settingsImageUrl;
	}

	public static void setSettingsImageUrl(String settingsImageUrl) {
		uDroveApplication.settingsImageUrl = settingsImageUrl;
	}

	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		uDroveApplication.userid = userid;
	}

	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}

	
	
	public void setPassword(String _password) {
		uDroveApplication.password = _password;
	}

	/**
	 * @return the userid
	 */
	public String getPassword() {
		return uDroveApplication.password;
	}
	
	public static void initialiseDvirInspectionItems(){
		dvirInspectionItems = new ArrayList<Boolean>();
	}
	/**
	 * @return the dvirInspectionItems
	 */
	public static ArrayList<Boolean> getDvirInspectionItems() {
		return dvirInspectionItems;
	}

	/**
	 * @param dvirInspectionItems the dvirInspectionItems to set
	 */
	public static void setDvirInspectionItems(ArrayList<Boolean> dvirInspectionItems) {
		uDroveApplication.dvirInspectionItems = dvirInspectionItems;
	}

	public static void setFromBackButton(boolean b) {
		// TODO Auto-generated method stub
		fromBackButton = b;
	}

	public static boolean getFromBackButton() {
		// TODO Auto-generated method stub
		return fromBackButton;
	}


	
	public void setCheckLogout(boolean b) {
		// TODO Auto-generated method stub
		checklogout = b;
	}

	public boolean getCheckLogout() {
		// TODO Auto-generated method stub
		return checklogout;
	}

	/**
	 * @return the hashmapdviritemmodel
	 */
	public static HashMap<Integer, DVIRItemModel> getHashmapdviritemmodel() {
		return hashmapdviritemmodel;
	}

	/**
	 * @param hashmapdviritemmodel the hashmapdviritemmodel to set
	 */
	public static void setHashmapdviritemmodel(HashMap<Integer, DVIRItemModel> hashmapdviritemmodel){
		uDroveApplication.hashmapdviritemmodel = hashmapdviritemmodel;
	}
	
	public static void setDefaultHashmapdviritemmodel() {
		// TODO Auto-generated method stub
		hashmapdviritemmodel = new HashMap<Integer, DVIRItemModel>();
	}

	public void setDVIRArrayListModel(ArrayList<DVIRModel> _arraylistdvirmodel) {
		// TODO Auto-generated method stub
		this.arraylistdvirmodel = _arraylistdvirmodel;
	}
	


	public  ArrayList<DVIRModel> getDVIRArrayListModel()
	{
		return this.arraylistdvirmodel;
	}
	
	public void setPODArrayListModel(ArrayList<PODModel> _arraylistpodmodel) {
		// TODO Auto-generated method stub
		uDroveApplication.arraylistpodmodel = _arraylistpodmodel;
	}
	
	public void setDefaultPODArrayListModel() {
		// TODO Auto-generated method stub
	 uDroveApplication.arraylistpodmodel = new ArrayList<PODModel>();
	}

	
	public int ReturnPODListArraySize() {
		// TODO Auto-generated method stub
		
		return uDroveApplication.arraylistpodmodel.size();
	}
	public  ArrayList<PODModel> getPODArrayListModel()
	{
		return uDroveApplication.arraylistpodmodel;
	}

	public void setCreateLoadId(int _id) {
		// TODO Auto-generated method stub
		createloadid = _id;
	}
	public int getcreateLoadid()
	{
		return createloadid;
	}

	/**
	 * @return the signUrl
	 */
	public  String getSignUrl() {
		return signUrl;
	}

	/**
	 * @param signUrl the signUrl to set
	 */
	public void setSignUrl(String signUrl) {
		uDroveApplication.signUrl = signUrl;
	}

	/**
	 * @return the serviceIds
	 */
	public  String getServiceIds() {
		
		return serviceIds;
	}

	/**
	 * @param serviceIds the serviceIds to set
	 */
	public  void setServiceIds(String serviceIds) {
		
		uDroveApplication.serviceIds = serviceIds;
	}
	
	public void ResetAllApplicationFields() {

		loadstatus = 0;
		dateindex = 0;
		listdvirtable = null;
		editsignatureexpenditure = null;
		settingsImageUrl = null;
		signUrl = null;
		serviceIds = null;
		userid = null;
		password = null;
		dvirInspectionItems = null;
		hashmapdviritemmodel = null;
		arraylistdvirmodel = null;
		arraylistpodmodel = null;
		loadtempdata = null;
		checklogout = false;
		createloadid = 0;
		fromBackButton = false;
		driver_type = 0;

		}

	/**
	 * @return the lastSevenDvirsHashMap
	 */
	public HashMap<Integer,DVIRModel> getLastSevenDvirsHashMap() {
		return lastSevenDvirsHashMap;
	}

	/**
	 * @param lastSevenDvirsHashMap the lastSevenDvirsHashMap to set
	 */
	public void setLastSevenDvirsHashMap(HashMap<Integer,DVIRModel> lastSevenDvirsHashMap) {
		this.lastSevenDvirsHashMap = lastSevenDvirsHashMap;
	}
	
}