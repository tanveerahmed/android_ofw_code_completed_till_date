package com.mobile.udrove.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;

public class HTTPUtil {

	// public static final String CONTENT_DOWNLOAD_URL =
	// "http://lanetteam.co.in/TownBuddha/TownAppWebService.asmx/Classified_ClassifiedPhotosList";

	public static String ParseJson(String jsonStr) {

		try {
			JSONObject jObject = new JSONObject(jsonStr);
			JSONObject headerObject = jObject.getJSONObject("header");
			String checkresult = headerObject.getString("result");
			String attributeId = headerObject.getString("message");
		    String subscriptionChanged=headerObject.getString("subscription_changed");
			//	boolean subscriptChanged=headerObject.getBoolean("subscription_changed");
			
			if (checkresult.equals("FAIL")) // This is required
			{
				return "FAIL," + attributeId+","+subscriptionChanged;
			}
			return "PASS," + attributeId+","+subscriptionChanged;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAIL,"+HTTPConstants.SERVER_ERROR+",false";
		
		}

//		return jsonStr;
	}

	public static String ParseLoginJson(String jsonStr) {

		try {
			JSONObject jObject = new JSONObject(jsonStr);

			JSONObject headerObject = jObject.getJSONObject("header");
			String checkresult = headerObject.getString("result");
			String attributeId = headerObject.getString("message");
			
			if (checkresult.equals("FAIL")) // This is required
			{
				return "FAIL," + attributeId;
			}
			return "PASS," + attributeId;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAIL,"+HTTPConstants.SERVER_ERROR;
		
		}

//		return jsonStr;
	}

	
	public static String ParseLoadCurrentJson(String jsonStr) {

		try {
			JSONObject jObject = new JSONObject(jsonStr);

			JSONObject headerObject = jObject.getJSONObject("header");
			String checkresult = headerObject.getString("result");
			String httpStatus = headerObject.getString("http_status");
			String attributeId = headerObject.getString("message");
			String id = null;
			String trackingnum = null;

			if (checkresult.equals("FAIL")) // This is required
			{
				return "FAIL," + attributeId;
			} else {
				JSONObject bodyObject = jObject.getJSONObject("body");
				id = bodyObject.getString("id");
				trackingnum=bodyObject.getString("tracking_number");
			}
			return "PASS," + attributeId + "," + id+","+trackingnum;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonStr;
	}

	public static String ParseImageJson(String jsonStr) {

		try {
		
			JSONObject jObject = new JSONObject(jsonStr);
			JSONObject headerObject = jObject.getJSONObject("header");
			String checkresult = headerObject.getString("result");
			String httpStatus = headerObject.getString("http_status");
			String attributeId = headerObject.getString("message");
			String url = null;

			if (checkresult.equals("FAIL")) // This is required
			{
				return "FAIL," + attributeId;
			} 
			else {
				
				JSONObject bodyObject = jObject.getJSONObject("body");
				url = bodyObject.getString("url");
			}
			return "PASS," + attributeId + "," + url;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "FAIL,"+HTTPConstants.SERVER_ERROR+",false";

		}

	}

	public static String DVIRVIEWHOMEParseJson(String jsonStr) {
		String attributeId = null;
		if (!jsonStr.equals("No Response from the Server")) {
			try {
				JSONObject jObject = new JSONObject(jsonStr);

				JSONObject headerObject = jObject.getJSONObject("header");
				attributeId = headerObject.getString("result");
				if (attributeId.equals("SUCCESS")) {

					return jsonStr;

				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "FAIL";
	}

	public static String ParseTroubleTicketJson(String jsonStr) {

		if (!jsonStr.equals("No Response from the Server")) {
			try {
				JSONObject jObject = new JSONObject(jsonStr);

				JSONObject headerObject = jObject.getJSONObject("header");
				String checkResult = headerObject.getString("result");
				String attributeId = headerObject.getString("message");
			    String subscriptionChanged=headerObject.getString("subscription_changed");

				if (checkResult.equals("SUCCESS")) {

					JSONObject bodyObject = jObject.getJSONObject("body");
					String ticket_id = bodyObject.getString("ticket_id");
					return "PASS," + ticket_id + "," + subscriptionChanged;
				}
                 
				return "FAIL," + attributeId+ "," + subscriptionChanged;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jsonStr;
	}

		
	public static String ParseRequestDemoJson(String jsonStr) {

		if (!jsonStr.equals("No Response from the Server")) {
			try {
				JSONObject jObject = new JSONObject(jsonStr);

				JSONObject headerObject = jObject.getJSONObject("header");
				String checkresult = headerObject.getString("result");
	
				String attributeId = headerObject.getString("message");
				if (checkresult.equals("FAIL")) // This is required
				{
					return "FAIL," + attributeId;
				}
				return "PASS," + attributeId;
						
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "FAIL,"+HTTPConstants.SERVER_ERROR;
			}
		}
		return jsonStr;
	}

	public static String TodaysDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());
		return formattedDate3+AppUtil.getTimeZoneString();
	}

	public static String TodaysDate1() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		String formattedDate3 = df3.format(c.getTime());
		return formattedDate3+AppUtil.getTimeZoneString();
	}

	public static String TodayDateOnly() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat("MM-dd-yyyy");
		String formattedDate3 = df3.format(c.getTime());
		return formattedDate3;
	}
	
	public static String TodaysDateWithoutZone() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df3 = new SimpleDateFormat(AppConstants.DATE_FORMAT);
		String formattedDate3 = df3.format(c.getTime());
		return formattedDate3;
	}
	
	
	
	
}
