package com.mobile.udrove.service;

public class HTTPConstants {
	
//	public static final String DOMAIN_URL = "http://udroveweb.whishworks.com/api/";// for QA Testing
//	public static final String DOMAIN_URL = "http://202.65.154.167:3003/api/";   // for development testing
//	public static final String DOMAIN_URL = "http://24.116.178.119/api/";   // for UAT 
//	public static final String DOMAIN_URL = "http://eobr.udrove.com/api/";   // for PROD 
	public static final String DOMAIN_URL = "http://192.168.2.112:7777/api/";   // for testing
	
	public static final String LOGIN_URL = DOMAIN_URL+"authenticate";
	public static final String REQUESTDEMO_URL = DOMAIN_URL+"request_demo";
	public static final String TROUBLE_URL = DOMAIN_URL+"tickets";
	public static final String DRIVER_STATUS_URL = DOMAIN_URL+"change_duty_status";
	public static final String CREATE_LOG_INFO_URL = DOMAIN_URL+"create_log_info";
	public static final String UPDATE_LOG_INFO_URL = DOMAIN_URL+"update_log_info";
	public static final String EXPENSES_INFO_URL = DOMAIN_URL+"expenditures";
	public static final String FUEL_RECEIPT_URL = DOMAIN_URL+"fuel_receipts";
	public static final String POD_URL = DOMAIN_URL+"pods";
	public static final String CREATE_LOAD_URL = DOMAIN_URL+"loads";
	public static final String UPDATE_LOAD_URL = DOMAIN_URL+"update_load";
	public static final String GET_LOAD_URL = DOMAIN_URL+"loads/";
	public static final String CREATE_DVIR_URL = DOMAIN_URL+"dvir/";
	public static final String UPDATE_DVIR_URL = DOMAIN_URL+"update_dvir/";
	public static final String SEND_TRIP_PACKET = DOMAIN_URL+"send_trip_packet";
	public static final String GET_LAST_SEVEN_LOGS = DOMAIN_URL+"get_driver_logs";
	public static final String GET_CURRENT_DAY_LOG = DOMAIN_URL+"get_current_day_log";
	public static final String GET_PAPER_LOGS = DOMAIN_URL+"get_paper_logs";
	public static final String GET_CURRENT_DAY_PAPER_LOG = DOMAIN_URL+"get_current_day_paper_log";
	public static final String APPROVE_LOG = DOMAIN_URL+"approve_log";
	public static final String GET_META_DATA = DOMAIN_URL+"get_all_meta_data";
	public static final String SEND_DOCUMENT= DOMAIN_URL+"send_document";
	public static final String GET_VIEWDVIR_HOME_URL = DOMAIN_URL+"dvir/";
	public static final String IMAGE_UPLOAD = DOMAIN_URL+"upload";
	public static final String SIGN_URL= DOMAIN_URL+"update_signature";
	public static final String SEND_GPS_UPDATES=DOMAIN_URL+"save_gps_location";
	public static final String GET_ALL_EXEMPTIONS=DOMAIN_URL+"get_all_exemptions";
	public static final String GET_USER_EXEMPTIONS=DOMAIN_URL+"get_exemptions";
	
	public static final String GET_PAPER_LOG = DOMAIN_URL+"get_paper_log";
	
	public static final String SET_PERSONAL_CONVEYANCE_FLAG = DOMAIN_URL+"set_personal_conveyance_flag";
	public static final String GET_PERSONAL_CONVEYANCE_FLAG = DOMAIN_URL+"get_personal_conveyance_flag";

	public static final String CONNECTING = "Loading...";
	public static final String PREPARING = "Preparing...";

	public static final String LOGIN_ERROR = "Invalid User name or Password";
	public static final String SERVER_ERROR = "Server not responding. Please try again later";
	public static final String REQUEST_ERROR = "Request Demo Fails";
	public static final String TROUBLE_ERROR = "Trouble Ticket Fails";
	public static final String DRIVER_STATUS_ERROR = "Cannot update the status now! try after some time";
	public static final String LOG_INFO_ERROR = "Log info failed to connect";
	public static final String DVIR_VIEW_HOME_FAIL = "Failed to get the DVIR Data";
	
}
