package com.mobile.udrove.service;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.mobile.udrove.util.AppUtil;
import com.mobile.udrove.util.AppConstants;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class UDroveService extends AsyncTask<String, String, String> {

	private final ProgressDialog dialog;
	private HashMap<String, String> mData = null;// post data
	UDroveServiceListener listener = null;
	JSONObject jsonobject, expenditurejsonobject;
	int i = 0;

	String reference = null;

	private Context parentCtx;

	public UDroveService(Context ctx, JSONObject jsn,
			UDroveServiceListener _listener) {

		expenditurejsonobject = jsn;
		i = 1;
		listener = _listener;
		jsonobject = jsn;
		dialog = new ProgressDialog(ctx);
		parentCtx = ctx;

	}

	public UDroveService(Context ctx, HashMap<String, String> data,
			UDroveServiceListener _listener) {

		parentCtx = ctx;
		i = 0;
		listener = _listener;
		mData = data;
		dialog = new ProgressDialog(ctx);

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setMessage(HTTPConstants.CONNECTING);
		dialog.setCancelable(false);
		dialog.show();
	}

	@Override
	protected String doInBackground(String... params) {

		if (AppUtil.isInternetAvailable(parentCtx)) {

			byte[] result = null;
			String str = "";
			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			JSONObject json = new JSONObject(); // URL

			try {

				HttpPost httppost = new HttpPost(params[0]);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");

				if (params.length > 1) {
					reference = params[1];
				}

				if (i == 0) {
					Iterator<String> it = mData.keySet().iterator();
					while (it.hasNext()) {
						String key = it.next();

						json.put(key, mData.get(key));

					}
				} else {

					json = expenditurejsonobject;
				}

				StringEntity se = new StringEntity(json.toString());
				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httppost.setEntity(se);

				HttpResponse response = httpclient.execute(httppost);
				result = EntityUtils.toByteArray(response.getEntity());
				str = new String(result, "UTF-8");
			} catch (UnsupportedEncodingException e) {

				e.printStackTrace();
				return AppConstants.SERVER_CONNECTION_ERROR;

			} catch (Exception e) {

				e.printStackTrace();
				return AppConstants.SERVER_CONNECTION_ERROR;

			}
			return str;

		} else {

			return AppConstants.INTERNET_CONNECTION_ERROR1;

		}
	}

	@Override
	protected void onPostExecute(String posts) {

		dialog.dismiss();
		if (reference != null) {
			listener.onServiceComplete(posts, reference);
		} else {
			listener.onServiceComplete(posts);
		}

	}
}
