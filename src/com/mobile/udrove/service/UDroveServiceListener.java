package com.mobile.udrove.service;


public interface UDroveServiceListener {
	
	public void onServiceComplete(String posts);
	
	public void onServiceComplete(String posts, String reference);
}
