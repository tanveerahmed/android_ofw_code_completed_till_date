package com.mobile.udrove.service;

import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.mobile.udrove.app.uDroveApplication;
import com.mobile.udrove.util.AppConstants;
import com.mobile.udrove.util.AppUtil;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class UDrovePersonalUseService extends AsyncTask<String, Void, String> {

	ProgressDialog dialog;

	boolean setPersonalUse = false;

	private Context parentCtx;

	UDrovePersonalUseServiceListener listener = null;
	uDroveApplication udroveApp;

	public UDrovePersonalUseService(Context contex,
			UDrovePersonalUseServiceListener _listener,
			uDroveApplication _udroveApp) {

		parentCtx = contex;
		listener = _listener;
		udroveApp = _udroveApp;

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog = new ProgressDialog(parentCtx);
		dialog.setMessage(HTTPConstants.CONNECTING);
		dialog.show();
	}

	@Override
	protected String doInBackground(String... urls) {

		StringEntity se = null;
		String str = "";
		if (AppUtil.isInternetAvailable(parentCtx)) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpParams myParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(myParams, 10000);
			HttpConnectionParams.setSoTimeout(myParams, 10000);

			JSONObject json = new JSONObject();

			HttpPost httput = new HttpPost(
					HTTPConstants.GET_PERSONAL_CONVEYANCE_FLAG);

			httput.setHeader("Accept", "application/json");
			httput.setHeader("Content-type", "application/json");

			try {

				json.put("time_stamp", HTTPUtil.TodaysDate());
				json.put("userid", udroveApp.getUserid());
				json.put("password", udroveApp.getPassword());

				se = new StringEntity(json.toString());

				System.out.println("The SE is  request " + se.toString());

				se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
						"application/json"));
				httput.setEntity(se);
				HttpResponse response = httpclient.execute(httput);
				byte[] result = EntityUtils.toByteArray(response.getEntity());

				str = new String(result, "UTF-8");

				// str =
				// "{\"header\":{\"result\":\"SUCCESS\",\"message\":\"Please check your internet connection and try again\",\"subscription_changed\":\"false\"}}";

				Log.v(">>>>>>>", "str:" + str);

				return str;

			} catch (UnsupportedEncodingException e) {

				return AppConstants.SERVER_CONNECTION_ERROR;
			} catch (Exception e) {

				return AppConstants.SERVER_CONNECTION_ERROR;

			}

			

		} else {

			return AppConstants.INTERNET_CONNECTION_ERROR1;

		}

	}

	@Override
	protected void onPostExecute(String posts) {

		dialog.dismiss();

		try {
			JSONObject jObject = new JSONObject(posts);
			JSONObject headerObject = jObject.getJSONObject("header");
			String checkresult = headerObject.getString("result");
			String message = headerObject.getString("message");
			// String
			// subscriptionChanged=headerObject.getString("subscription_changed");
			String pc_flag = headerObject.getString("personal_conveyance_flag");

			boolean pc_flag_bool = false;

			if (pc_flag.equalsIgnoreCase("true")) {

				pc_flag_bool = true;

			}

			if (checkresult.equals("FAIL")) // This is required
			{
				listener.onPersonalUseStatus(false, pc_flag_bool, message);
			} else {

				listener.onPersonalUseStatus(true, pc_flag_bool, message);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			listener.onPersonalUseStatus(false, false,
					AppConstants.SERVER_CONNECTION_UNAVAILABLE);

		}

	}
}